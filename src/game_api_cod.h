#ifndef GAME_API_CACHE_ORIENTED_DESIGN_H
#define GAME_API_CACHE_ORIENTED_DESIGN_H 1

// NOTE(theGiallo): data structures and functions for cache oriented design
// ( what all other programmers call data oriented design or DOD )

#include "basic_types.h"
#include "game_api_lepre.h"
#include "game_api_utility.h"

#if COD_NO_LIMITS_H
	// NOTE(theGiallo): this is not safe, maybe is better to disable
	// bitvectors if you can't be sure.
	#define COD_CHAR_BIT ( 8 * sizeof( u8 ) )
#else
	#include <limits.h>
	#define COD_CHAR_BIT CHAR_BIT
#endif
#define COD_BITMASK(b) (1 << ((b) % COD_CHAR_BIT))
#define COD_BITSLOT(b) ((b) / COD_CHAR_BIT)
#define COD_BITSET(a, b) ((a)[COD_BITSLOT(b)] |= COD_BITMASK(b))
#define COD_BITCLEAR(a, b) ((a)[COD_BITSLOT(b)] &= ~COD_BITMASK(b))
#define COD_BITTEST(a, b) ((a)[COD_BITSLOT(b)] & COD_BITMASK(b))
#define COD_BITNSLOTS(nb) ((nb + COD_CHAR_BIT - 1) / COD_CHAR_BIT)

#define COD_POINTER_IS_64BITS_ALIGNED(p) ( 0x0LU == ( (u64)(p) & 0x7LU ) )

u64
inline
size_round_to_64bits_alignment( u64 size )
{
	u64 ret = ( size + 0x7LU ) & ( ~0x7LU );

	return ret;
}

inline
u8 *
memory_to_next_64bits_alignment( u8 * mem )
{
	u8 * ret = (u8*)( ( (u64)mem + 0x7LU ) & ( ~0x7LU ) );
	return ret;
}

struct
u32_bool
{
	u32  u32_v;
	bool bool_v;
};

struct
u64_to_u32_Hash_Table
{
	struct
	Key_Value_List_Node
	{
		u64 key;
		u32 value;
		s32 next_id;
	};
	u8  * values_table_presence_bv;
	Key_Value_List_Node * values_table;
	Key_Value_List_Node * list_nodes_storage;
	u8  * list_nodes_storage_presence_bv;
	u8  * list_nodes_storage_255_blocks_occupancy;
	u32 table_size;
	u32 list_nodes_storage_capacity;
	u32 list_nodes_storage_occupancy;
	u32 padding;
};

inline
u32
hash_table_n_n_expected_items_per_slot( u32 capacity )
{
	u32 ret;
	f32 log_capacity = logf( capacity );
	ret = (u32)ceilf( log_capacity / logf( log_capacity ) );
	return ret;
}
inline
u32
u64_to_u32_hash_table_list_nodes_storage_capacity( u32 hash_table_capacity )
{
	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( hash_table_capacity );
	u32 ret = ( expected_items_per_slot - 1 ) * ( ( hash_table_capacity + expected_items_per_slot - 1 ) / expected_items_per_slot );
	return ret;
}
inline
u64
u64_to_u32_hash_table_compute_memory_size( u32 capacity )
{
	u64 ret = 0;

	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( capacity );
	u32 list_nodes_storage_capacity = capacity * ( expected_items_per_slot - 1 );
	ret += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(capacity) );
	ret += size_round_to_64bits_alignment( capacity * sizeof( u64_to_u32_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( u64_to_u32_hash_table_list_nodes_storage_capacity( capacity ) * sizeof( u64_to_u32_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(list_nodes_storage_capacity) );
	ret += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	return ret;
}

inline
u8 *
u64_to_u32_hash_table_init( u64_to_u32_Hash_Table * ht, u8 * mem, u32 capacity )
{
	lpr_assert( COD_POINTER_IS_64BITS_ALIGNED(mem) );

	ht->values_table_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(capacity) );

	ht->values_table = (u64_to_u32_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( capacity * sizeof( u64_to_u32_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_capacity = u64_to_u32_hash_table_list_nodes_storage_capacity( capacity );

	ht->list_nodes_storage = (u64_to_u32_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( ht->list_nodes_storage_capacity * sizeof( u64_to_u32_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(ht->list_nodes_storage_capacity) );

	ht->list_nodes_storage_255_blocks_occupancy = mem;
	mem += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	ht->table_size = capacity;
	ht->list_nodes_storage_occupancy = 0;

	return mem;
}

inline
u32
u64_to_u32_hash_table_hash_key( u64 key, u32 table_size )
{
	u32 ret = 0;
	u32 * u32_keys = (u32*)&key;
#if 0
	ret = ((u32)( 0xFFFFFFFFUL & ( key ^ ( key >> 32 ) ) ) ) % table_size;
#else
	ret = ( u32_keys[0] ^ u32_keys[1] ) % table_size;
#endif

	return ret;
}

inline
void
u64_to_u32_hash_table_clear( u64_to_u32_Hash_Table * ht )
{
	ht->list_nodes_storage_occupancy = 0;
	memset( ht->values_table_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * COD_BITNSLOTS(ht->table_size) ) );
	memset( ht->list_nodes_storage_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * COD_BITNSLOTS(ht->list_nodes_storage_capacity) ) );
	memset( ht->list_nodes_storage_255_blocks_occupancy, 0x00,
	        sizeof(u8) * ( ( ht->list_nodes_storage_capacity + 254 ) / 255 ) );
}

inline
u32_bool
u64_to_u32_hash_table_retrieve( u64_to_u32_Hash_Table * ht, u64 key )
{
	u32_bool ret = {};

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( COD_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key == key )
			{
				ret.u32_v = node->value;
				ret.bool_v = true;
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

inline
u32
u64_to_u32_hash_table_retrieve_many( u64_to_u32_Hash_Table * ht, u64 key, Mem_Stack * mem_stack )
{
	u32 ret = 0;

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( COD_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u32 * ret_array = NULL;

		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key == key )
			{
				++ret;
				u32 * m = (u32*) mem_stack->push( sizeof( u32 ) );
				if ( !ret_array )
				{
					ret_array = m;
				}
				*m = node->value;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

bool
u64_to_u32_hash_table_change( u64_to_u32_Hash_Table * ht, u64 key, u32 value );

bool
u64_to_u32_hash_table_insert( u64_to_u32_Hash_Table * ht, u64 key, u32 value );

inline
u32_bool
u64_to_u32_hash_table_remove( u64_to_u32_Hash_Table * ht, u64 key )
{
	u32_bool ret = {};

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( COD_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		u64_to_u32_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key == key )
			{
				ret.u32_v  = node->value;
				ret.bool_v = true;
				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						COD_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						u64_to_u32_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}

inline
u32
u64_to_u32_hash_table_remove_many( u64_to_u32_Hash_Table * ht, u64 key )
{
	u32 ret = 0;

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( COD_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		u64_to_u32_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key == key )
			{
				++ret;
				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						COD_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						u64_to_u32_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}

inline
u32
u64_to_u32_hash_table_remove_many_and_feedback( u64_to_u32_Hash_Table * ht, u64 key, Mem_Stack * mem_stack )
{
	u32 ret = 0;

	u32 hash = u64_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( COD_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u32 * ret_array = NULL;

		u64_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		u64_to_u32_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key == key )
			{
				++ret;

				u32 * m = (u32*) mem_stack->push( sizeof( u32 ) );
				if ( !ret_array )
				{
					ret_array = m;
				}
				*m = node->value;

				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						COD_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						u64_to_u32_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

struct
B12_to_u32_Hash_Table
{
	struct
	Key_Value_List_Node
	{
		u64 key_xy;
		u32 key_z;
		u32 value;
		s32 next_id;
	};
	u8  * values_table_presence_bv;
	Key_Value_List_Node * values_table;
	Key_Value_List_Node * list_nodes_storage;
	u8  * list_nodes_storage_presence_bv;
	u8  * list_nodes_storage_255_blocks_occupancy;
	u32 table_size;
	u32 list_nodes_storage_capacity;
	u32 list_nodes_storage_occupancy;
	u32 padding;
};

union
B12
{
	struct
	{
		u64 xy;
		u32 z;
	};
	struct
	{
		s32 s32_x;
		s32 s32_y;
		s32 s32_z;
	};
	V3s32 v3s32;
	V3 v3;
	struct
	{
		u32 u32_x;
		u32 u32_y;
		u32 u32_z;
	};
	struct
	{
		u8 u8_12[12];
	};
};

inline
u32
B12_to_u32_hash_table_list_nodes_storage_capacity( u32 hash_table_capacity )
{
	// TODO(theGiallo, 2017-02-04): this is the same for the other hash table,
	// maybe use a single generic function
	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( hash_table_capacity );
	u32 ret = ( expected_items_per_slot - 1 ) * ( ( hash_table_capacity + expected_items_per_slot - 1 ) / expected_items_per_slot );
	return ret;
}
inline
u64
B12_to_u32_hash_table_compute_memory_size( u32 capacity )
{
	u64 ret = 0;

	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( capacity );
	u32 list_nodes_storage_capacity = capacity * ( expected_items_per_slot - 1 );
	ret += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(capacity) );
	ret += size_round_to_64bits_alignment( capacity * sizeof( B12_to_u32_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( B12_to_u32_hash_table_list_nodes_storage_capacity( capacity ) * sizeof( B12_to_u32_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(list_nodes_storage_capacity) );
	ret += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	return ret;
}

inline
u8 *
B12_to_u32_hash_table_init( B12_to_u32_Hash_Table * ht, u8 * mem, u32 capacity )
{
	lpr_assert( COD_POINTER_IS_64BITS_ALIGNED(mem) );

	ht->values_table_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(capacity) );

	ht->values_table = (B12_to_u32_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( capacity * sizeof( B12_to_u32_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_capacity = u64_to_u32_hash_table_list_nodes_storage_capacity( capacity );

	ht->list_nodes_storage = (B12_to_u32_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( ht->list_nodes_storage_capacity * sizeof( B12_to_u32_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(ht->list_nodes_storage_capacity) );

	ht->list_nodes_storage_255_blocks_occupancy = mem;
	mem += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	ht->table_size = capacity;
	ht->list_nodes_storage_occupancy = 0;

	return mem;
}

inline
u32
B12_to_u32_hash_table_hash_key( B12 key, u32 table_size )
{
	// TODO(theGiallo, 2017-02-04): if this generates too many collisions use u32_FNV1a
	u32 ret = 0;
	u64 seeds[2];
	seeds[0] = key.xy;
	seeds[1] = key.z;
	seeds[0] = seeds[0] ^ 29364829169487291LU;
	seeds[1] = seeds[1] ^ 9283469126382374107LU;
	ret = randu_between( seeds, 0, table_size-1 );

	return ret;
}

inline
void
B12_to_u32_hash_table_clear( B12_to_u32_Hash_Table * ht )
{
	ht->list_nodes_storage_occupancy = 0;
	memset( ht->values_table_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * COD_BITNSLOTS(ht->table_size) ) );
	memset( ht->list_nodes_storage_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * COD_BITNSLOTS(ht->list_nodes_storage_capacity) ) );
	memset( ht->list_nodes_storage_255_blocks_occupancy, 0x00,
	        sizeof(u8) * ( ( ht->list_nodes_storage_capacity + 254 ) / 255 ) );
}

inline
u32_bool
B12_to_u32_hash_table_retrieve( B12_to_u32_Hash_Table * ht, B12 key )
{
	u32_bool ret = {};

	u32 hash = B12_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( COD_BITTEST(ht->values_table_presence_bv, hash) )
	{
		B12_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key_xy == key.xy && node->key_z == key.z )
			{
				ret.u32_v = node->value;
				ret.bool_v = true;
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

bool
B12_to_u32_hash_table_change( B12_to_u32_Hash_Table * ht, B12 key, u32 value );

bool
B12_to_u32_hash_table_insert( B12_to_u32_Hash_Table * ht, B12 key, u32 value );

inline
u32_bool
B12_to_u32_hash_table_remove( B12_to_u32_Hash_Table * ht, B12 key )
{
	u32_bool ret = {};

	u32 hash = B12_to_u32_hash_table_hash_key( key, ht->table_size );

	if ( COD_BITTEST(ht->values_table_presence_bv, hash) )
	{
		B12_to_u32_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		B12_to_u32_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key_xy == key.xy && node->key_z == key.z )
			{
				ret.u32_v  = node->value;
				ret.bool_v = true;
				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						COD_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						B12_to_u32_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

struct
B12_bool
{
	B12  B12_v;
	bool bool_v;
};

struct
u64_to_B12_Hash_Table
{
	struct
	Key_Value_List_Node
	{
		u64 key;
		B12 value;
		s32 next_id;
	};
	u8  * values_table_presence_bv;
	Key_Value_List_Node * values_table;
	Key_Value_List_Node * list_nodes_storage;
	u8  * list_nodes_storage_presence_bv;
	u8  * list_nodes_storage_255_blocks_occupancy;
	u32 table_size;
	u32 list_nodes_storage_capacity;
	u32 list_nodes_storage_occupancy;
	u32 padding;
};

inline
u32
u64_to_B12_hash_table_list_nodes_storage_capacity( u32 hash_table_capacity )
{
	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( hash_table_capacity );
	u32 ret = ( expected_items_per_slot - 1 ) * ( ( hash_table_capacity + expected_items_per_slot - 1 ) / expected_items_per_slot );
	return ret;
}
inline
u64
u64_to_B12_hash_table_compute_memory_size( u32 capacity )
{
	u64 ret = 0;

	u32 expected_items_per_slot = hash_table_n_n_expected_items_per_slot( capacity );
	u32 list_nodes_storage_capacity = capacity * ( expected_items_per_slot - 1 );
	ret += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(capacity) );
	ret += size_round_to_64bits_alignment( capacity * sizeof( u64_to_B12_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( u64_to_B12_hash_table_list_nodes_storage_capacity( capacity ) * sizeof( u64_to_B12_Hash_Table::Key_Value_List_Node ) );
	ret += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(list_nodes_storage_capacity) );
	ret += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	return ret;
}

inline
u8 *
u64_to_B12_hash_table_init( u64_to_B12_Hash_Table * ht, u8 * mem, u32 capacity )
{
	lpr_assert( COD_POINTER_IS_64BITS_ALIGNED(mem) );

	ht->values_table_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(capacity) );

	ht->values_table = (u64_to_B12_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( capacity * sizeof( u64_to_B12_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_capacity = u64_to_B12_hash_table_list_nodes_storage_capacity( capacity );

	ht->list_nodes_storage = (u64_to_B12_Hash_Table::Key_Value_List_Node*)mem;
	mem += size_round_to_64bits_alignment( ht->list_nodes_storage_capacity * sizeof( u64_to_B12_Hash_Table::Key_Value_List_Node ) );

	ht->list_nodes_storage_presence_bv = mem;
	mem += size_round_to_64bits_alignment( sizeof(u8) * COD_BITNSLOTS(ht->list_nodes_storage_capacity) );

	ht->list_nodes_storage_255_blocks_occupancy = mem;
	mem += sizeof(u8) * ( ( capacity + 254 ) / 255 );

	ht->table_size = capacity;
	ht->list_nodes_storage_occupancy = 0;

	return mem;
}

inline
u32
u64_to_B12_hash_table_hash_key( u64 key, u32 table_size )
{
	u32 ret = 0;
	u32 * u32_keys = (u32*)&key;
#if 0
	ret = ((u32)( 0xFFFFFFFFUL & ( key ^ ( key >> 32 ) ) ) ) % table_size;
#else
	ret = ( u32_keys[0] ^ u32_keys[1] ) % table_size;
#endif

	return ret;
}

inline
void
u64_to_B12_hash_table_clear( u64_to_B12_Hash_Table * ht )
{
	ht->list_nodes_storage_occupancy = 0;
	memset( ht->values_table_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * COD_BITNSLOTS(ht->table_size) ) );
	memset( ht->list_nodes_storage_presence_bv, 0x00,
	        size_round_to_64bits_alignment(
	           sizeof(u8) * COD_BITNSLOTS(ht->list_nodes_storage_capacity) ) );
	memset( ht->list_nodes_storage_255_blocks_occupancy, 0x00,
	        sizeof(u8) * ( ( ht->list_nodes_storage_capacity + 254 ) / 255 ) );
}

inline
B12_bool
u64_to_B12_hash_table_retrieve( u64_to_B12_Hash_Table * ht, u64 key )
{
	B12_bool ret = {};

	u32 hash = u64_to_B12_hash_table_hash_key( key, ht->table_size );

	if ( COD_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_B12_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		for(;;)
		{
			if ( node->key == key )
			{
				ret.B12_v = node->value;
				ret.bool_v = true;
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			node = ht->list_nodes_storage + node->next_id;
		}
	}

	return ret;
}

bool
u64_to_B12_hash_table_change( u64_to_B12_Hash_Table * ht, u64 key, B12 value );

bool
u64_to_B12_hash_table_insert( u64_to_B12_Hash_Table * ht, u64 key, B12 value );

inline
B12_bool
u64_to_B12_hash_table_remove( u64_to_B12_Hash_Table * ht, u64 key )
{
	B12_bool ret = {};

	u32 hash = u64_to_B12_hash_table_hash_key( key, ht->table_size );

	if ( COD_BITTEST(ht->values_table_presence_bv, hash) )
	{
		u64_to_B12_Hash_Table::Key_Value_List_Node * node =
		   ht->values_table + hash;
		u64_to_B12_Hash_Table::Key_Value_List_Node * prev_node = NULL;
		s32 id = -1;
		for(;;)
		{
			if ( node->key == key )
			{
				ret.B12_v  = node->value;
				ret.bool_v = true;
				if ( id < 0 )
				{
					if ( node->next_id < 0 )
					{
						COD_BITCLEAR(ht->values_table_presence_bv, hash);
					} else
					{
						u32 id_to_delete = node->next_id;
						u64_to_B12_Hash_Table::Key_Value_List_Node * next_node =
						   ht->list_nodes_storage + node->next_id;
						memcpy( node, next_node, sizeof(*node) );
						COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id_to_delete );
						--ht->list_nodes_storage_255_blocks_occupancy[id_to_delete/255];
					}
				} else
				{
					COD_BITCLEAR(ht->list_nodes_storage_presence_bv, id );
					--ht->list_nodes_storage_255_blocks_occupancy[id/255];
					prev_node->next_id = node->next_id;
				}
				break;
			}
			if ( node->next_id < 0 )
			{
				break;
			}
			prev_node = node;
			id = node->next_id;
			node = ht->list_nodes_storage + id;
		}
	}

	return ret;
}

////////////////////////////////////////////////////////////////////////////////

// NOTE(theGiallo): each array is 64bit aligned, it's up to the user to make
// each data type multiple of 8B.
struct
Generic_Table_Multicolumn
{
	u64_to_u32_Hash_Table ids_location_ht;
	u8   *  in_use_bv;
	u64  *  ids;
	void ** data;
	u32  *  data_elements_sizes;
	// NOTE(theGiallo): this has to stay at the end of the memory block
	u8   *  occupancy_per_255_blocks;
	u32 data_count;
	u32 occupancy;
	u32 capacity;
	bool is_sorted;
};

inline
u64
generic_table_multicolumn_compute_memory_size(
   u32 * data_elements_sizes, u32 data_count,
   u32 capacity )
{
	u64 ret =
	   size_round_to_64bits_alignment( u64_to_u32_hash_table_compute_memory_size( capacity ) ) +
	   size_round_to_64bits_alignment( COD_BITNSLOTS(capacity) ) +
	   size_round_to_64bits_alignment( sizeof(data_elements_sizes[0]) * data_count ) +
	   size_round_to_64bits_alignment( sizeof(Generic_Table_Multicolumn::ids[0]) * capacity ) +
	   size_round_to_64bits_alignment( sizeof(Generic_Table_Multicolumn::data[0]) * data_count ) +
	   ( sizeof(u8) * ( capacity + 254 / 255 ) );

	for ( u32 i = 0; i != data_count; ++i )
	{
		ret +=
		   size_round_to_64bits_alignment( data_elements_sizes[i] * capacity );
	}

	return ret;
}

inline
u8 *
generic_table_multicolumn_init( Generic_Table_Multicolumn * t, u8 * mem,
                                u32 * data_elements_sizes, u32 data_count,
                                u32 capacity )
{
	lpr_assert( COD_POINTER_IS_64BITS_ALIGNED(mem) );

	mem = u64_to_u32_hash_table_init( &t->ids_location_ht, mem, capacity );
	mem = memory_to_next_64bits_alignment( mem );

	t->in_use_bv = mem;
	mem += size_round_to_64bits_alignment( COD_BITNSLOTS(capacity) );

	t->ids = (u64*)mem;
	mem += size_round_to_64bits_alignment( sizeof(t->ids[0]) * capacity );

	t->data = (void**)mem;
	mem +=
	   size_round_to_64bits_alignment(
	      sizeof(Generic_Table_Multicolumn::data[0]) * data_count );
	for ( u32 i = 0; i != data_count; ++i )
	{
		t->data[i] = mem;
		mem += size_round_to_64bits_alignment( data_elements_sizes[i] * capacity );
	}

	t->data_elements_sizes = (u32*)mem;
	mem +=
	   size_round_to_64bits_alignment(
	      sizeof(t->data_elements_sizes[0]) * data_count );
	for ( u32 i = 0; i != data_count; ++i )
	{
		t->data_elements_sizes[i] = data_elements_sizes[i];
	}

	t->occupancy_per_255_blocks = mem;
	mem += ( sizeof(u8) * ( capacity + 254 / 255 ) );

	t->data_count = data_count;
	t->capacity   = capacity;
	t->is_sorted  = false;
	t->occupancy  = 0;

	return mem;
}

void
generic_table_multicolumn_compact( Generic_Table_Multicolumn * t );

// NOTE(theGiallo): does not checks for duplicates
bool
generic_table_multicolumn_insert_struct( Generic_Table_Multicolumn * t, u64 id, void * element_struct );

// NOTE(theGiallo): does not checks for duplicates
// NOTE(theGiallo): data is in the same format as the out parameter 'data' of the search
bool
generic_table_multicolumn_insert_pointers( Generic_Table_Multicolumn * t, u64 id, void ** data );

void *
generic_table_multicolumn_search_single_data( Generic_Table_Multicolumn * t, u64 id, u32 data_index );

void
generic_table_multicolumn_copy_data_to_struct( Generic_Table_Multicolumn * t, u32 entry_index, void * struct_ptr );

void
generic_table_multicolumn_fill_pointers( Generic_Table_Multicolumn * t, u32 entry_index, void ** data )
{
	u32 data_count = t->data_count;
	u8 ** t_data = (u8**)t->data;
	u32 * data_elements_sizes = t->data_elements_sizes;
	for ( u32 d = 0; d != data_count; ++d )
	{
		u32 e_s = data_elements_sizes[d];
		data[d] = t_data[d] + entry_index * e_s;
	}
}

// NOTE(theGiallo): returns the internal storage position or negative
s32
generic_table_multicolumn_search( Generic_Table_Multicolumn * t, u64 id );
// NOTE(theGiallo): returns the internal storage position or negative
// data is written, must not be NULL if is the case
s32
generic_table_multicolumn_search( Generic_Table_Multicolumn * t, u64 id, void ** data );

// NOTE(theGiallo): returns number of entries found and allocates in mem_stack
// an array of u32 storing in there the indexes of such entries
u32
generic_table_multicolumn_search_many( Generic_Table_Multicolumn * t, u64 id, Mem_Stack * mem_stack );

bool
generic_table_multicolumn_remove( Generic_Table_Multicolumn * t, u64 id );

u32
generic_table_multicolumn_remove_many( Generic_Table_Multicolumn * t, u64 id, Mem_Stack * mem_stack_tmp );

u32
generic_table_multicolumn_remove_many_and_feedback( Generic_Table_Multicolumn * t, u64 id, Mem_Stack * mem_stack );

inline
void
generic_table_multicolumn_clear( Generic_Table_Multicolumn * t )
{
	u64_to_u32_hash_table_clear( &t->ids_location_ht );
	memset( t->in_use_bv, 0x00, COD_BITNSLOTS(t->capacity) );
	memset( t->occupancy_per_255_blocks, 0x00, ( t->capacity + 254 / 255 ) );
	t->occupancy = 0;
}


////////////////////////////////////////////////////////////////////////////////

struct
Generic_Table
{
	u8   * in_use_bv;
	u64  * ids;
	void * data;
	u32 occupancy;
	u32 capacity;
	u32 data_element_size;
	bool is_sorted;
};

inline
u64
generic_table_compute_memory_size( u32 data_element_size, u32 capacity )
{
	u64 ret = COD_BITNSLOTS(capacity) +
	( sizeof(Generic_Table::ids[0]) + data_element_size ) * capacity;
	return ret;
}

inline
u8 *
generic_table_init( Generic_Table * t, u8 * mem,
                    u32 data_element_size,
                    u32 capacity )
{
	lpr_assert( COD_POINTER_IS_64BITS_ALIGNED(mem) );

	t->in_use_bv = mem;
	mem += COD_BITNSLOTS(capacity);
	t->ids = (u64*)mem;
	mem += sizeof(t->ids[0]) * capacity;
	t->data = mem;
	t->capacity = capacity;
	t->data_element_size = data_element_size;
	t->is_sorted = false;
	t->occupancy = 0;

	return mem;
}

void
generic_table_compact( Generic_Table * t );

bool
generic_table_insert( Generic_Table * t, u64 id, void * element );

void *
generic_table_search( Generic_Table * t, u64 id );

bool
generic_table_remove( Generic_Table * t, u64 id );

////////////////////////////////////////////////////////////////////////////////

void
cod_test( Mem_Stack * mem );

#endif /* ifndef GAME_API_CACHE_ORIENTED_DESIGN_H */
