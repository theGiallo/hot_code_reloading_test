/*

Copyright (c) <2016> <Gianluca Alloisio>

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

   1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

   3. This notice may not be removed or altered from any source
   distribution.

*/
/*
   NOTE(theGiallo): Memory Hotel - 2016-05-23
 */
#include <stdint.h>

/* NOTE(theGiallo): example of usage:
	lpr_u8 moved[MH_BITNSLOTS( 0xFFFF )] = MH_ZERO_STRUCT;
	MH_BITSET( moved, 10 )
	lpr_assert( MH_BITTEST( moved, 10 ), "MH_BITSET is not working!" );
	MH_BITCLEAR( moved, 10 )
	lpr_assert( !MH_BITTEST( moved, 10 ), "MH_BITCLEAR is not working!" );
*/
#if MH_NO_BITVECTOR
	#define MH_BITSET(a, b) (a)[b] = 1
	#define MH_BITCLEAR(a, b) (a)[b] = 0
	#define MH_BITTEST(a, b) (a)[b]
	#define MH_BITNSLOTS(nb) (nb)
#else
#ifndef MH_CHAR_BIT
	#ifdef CHAR_BIT
		#define MH_CHAR_BIT CHAR_BIT
	#else
		#if MH_NO_LIMITS_H
			// NOTE(theGiallo): this is not safe, maybe is better to disable
			// bitvectors if you can't be sure.
			#define MH_CHAR_BIT ( 8 * sizeof( lpr_u8 ) )
		#else
			#include <limits.h>
			#define MH_CHAR_BIT CHAR_BIT
		#endif
	#endif
#endif
#endif

#define MH_BITMASK(b) (1 << ((b) % MH_CHAR_BIT))
#define MH_BITSLOT(b) ((b) / MH_CHAR_BIT)
#define MH_BITSET(a, b) ((a)[MH_BITSLOT(b)] |= MH_BITMASK(b))
#define MH_BITCLEAR(a, b) ((a)[MH_BITSLOT(b)] &= ~MH_BITMASK(b))
#define MH_BITTEST(a, b) ((a)[MH_BITSLOT(b)] & MH_BITMASK(b))
#define MH_BITNSLOTS(nb) ((nb + MH_CHAR_BIT - 1) / MH_CHAR_BIT)

#define MH_GLUE2(a,b) a##b
#define MH_GLUE2M(a,b) MH_GLUE2(a,b)
#define MH_GLUE3(a,b,c) a##b##c
#define MH_GLUE3M(a,b,c) MH_GLUE3(a,b,c)

#ifndef MH_TYPE
#error  You have to define MH_TYPE and optionally MH_SHORT_TYPE before \
including mem_hotel.h

// NOTE(theGiallo): this is to reduce YCM errors in vim
#define MH_TYPE uint8_t
#endif

#ifdef MH_STATIC
#define MH_DEF static
#else
#define MH_DEF extern
#endif

#ifndef MH_TYPE_SHORT
#define MH_TYPE_SHORT MH_TYPE
#endif

#ifndef MH_FLOORS_COUNT
#define MH_FLOORS_COUNT 256
#define MH_FULL_TYPE MH_GLUE2M(MH_TYPE,_Mem_Hotel)
#define MH_SHORT_TYPE MH_GLUE2M(MH_TYPE_SHORT,_MH)
#else
#define MH_FULL_TYPE MH_GLUE3M(MH_TYPE,_Mem_Hotel_,MH_FLOORS_COUNT)
#define MH_SHORT_TYPE MH_GLUE3M(MH_TYPE_SHORT,_MH_,MH_FLOORS_COUNT)
#endif

#define MH_ITERATOR_TYPE MH_GLUE2M(MH_SHORT_TYPE,_Iterator)

#if ! MH_BODY_ONLY

struct
MH_ITERATOR_TYPE
{
#if MH_FLOORS_COUNT < 256
	uint8_t floor;
#elif MH_FLOORS_COUNT < 65536
	uint16_t floor;
#elif MH_FLOORS_COUNT < (1UL<<32)
	uint32_t floor;
#else
	uint64_t floor;
#endif
	uint8_t room;
};

typedef
struct
MH_GLUE2M(MH_FULL_TYPE,_st)
{
	uint8_t booked_bf[MH_FLOORS_COUNT][MH_BITNSLOTS(255)];
	uint8_t floor_occupancy[MH_FLOORS_COUNT];
	uint32_t total_occupancy;
	static const uint32_t capacity = MH_FLOORS_COUNT*255;
	static const uint32_t floors_count = MH_FLOORS_COUNT;

	MH_TYPE mem[capacity];

#if !MH_NO_MEMBER_FUNCTIONS
	bool
	is_full() const;

	MH_TYPE *
	allocate();

	int32_t
	allocate_get_index();

	MH_TYPE *
	allocate_clean();

	int32_t
	allocate_clean_get_index();

	void
	deallocate( MH_TYPE * e );

	void
	deallocate_index( uint32_t index );

	MH_TYPE *
	iterate( MH_ITERATOR_TYPE * it );
#endif
} MH_FULL_TYPE;
typedef MH_FULL_TYPE MH_SHORT_TYPE;

MH_DEF
bool
is_full( const MH_FULL_TYPE * mh );

MH_DEF
MH_TYPE *
allocate( MH_FULL_TYPE * mh );

MH_DEF
int32_t
allocate_get_index( MH_FULL_TYPE * mh );

MH_DEF
MH_TYPE *
allocate_clean( MH_FULL_TYPE * mh );

MH_DEF
int32_t
allocate_clean_get_index( MH_FULL_TYPE * mh );

MH_DEF
void
deallocate( MH_FULL_TYPE * mh, MH_TYPE * e );

MH_DEF
void
deallocate_index( MH_FULL_TYPE * mh, uint32_t index );

MH_DEF
MH_TYPE *
iterate( MH_FULL_TYPE * mh, MH_ITERATOR_TYPE * it );

#endif // NOTE(theGiallo): MH_BODY_ONLY

////////////////////////////////////////////////////////////////////////////////
#if MH_EMIT_BODY

#ifndef MH_ILLEGAL_PATH
#define MH_ILLEGAL_PATH() (void)0
#endif

#ifndef mh_memset
#define mh_memset memset
#include <string.h>
#endif

#if !MH_NO_MEMBER_FUNCTIONS
bool
MH_FULL_TYPE::
is_full() const
{
	bool ret = ::is_full( this );
	return ret;
}
#endif

MH_DEF
bool
is_full( const MH_FULL_TYPE * mh )
{
	bool ret = mh->total_occupancy == mh->capacity;
	return ret;
}

#if !MH_NO_MEMBER_FUNCTIONS
MH_TYPE *
MH_FULL_TYPE::
allocate( )
{
	MH_TYPE * ret = ::allocate( this );
	return ret;
}

int32_t
MH_FULL_TYPE::
allocate_get_index( )
{
	int32_t ret = ::allocate_get_index( this );
	return ret;
}
#endif

MH_DEF
int32_t
allocate_get_index( MH_FULL_TYPE * mh )
{
	int32_t ret = -1;

	for ( int i = 0; i != MH_FLOORS_COUNT; ++i )
	{
		if ( mh->floor_occupancy[i] != 255 )
		{
			for ( int j = 0; j != 255; ++j )
			{
				if ( !MH_BITTEST(mh->booked_bf[i], j) )
				{
					MH_BITSET(mh->booked_bf[i], j);
					++mh->floor_occupancy[i];
					++mh->total_occupancy;
					ret = i * 255 + j;
					goto return_l;
				}
			}
			MH_ILLEGAL_PATH();
		}
	}
	return_l:
	return ret;
}
MH_DEF
MH_TYPE *
allocate( MH_FULL_TYPE * mh )
{
	MH_TYPE * ret = 0;

	for ( int i = 0; i != MH_FLOORS_COUNT; ++i )
	{
		if ( mh->floor_occupancy[i] != 255 )
		{
			for ( int j = 0; j != 255; ++j )
			{
				if ( !MH_BITTEST(mh->booked_bf[i], j) )
				{
					MH_BITSET(mh->booked_bf[i], j);
					++mh->floor_occupancy[i];
					++mh->total_occupancy;
					ret = mh->mem + ( i * 255 + j );
					goto return_l;
				}
			}
			MH_ILLEGAL_PATH();
		}
	}
	return_l:
	return ret;
}

#if !MH_NO_MEMBER_FUNCTIONS
MH_TYPE *
MH_FULL_TYPE::
allocate_clean( )
{
	MH_TYPE * ret = ::allocate_clean( this );
	return ret;
}

int32_t
MH_FULL_TYPE::
allocate_clean_get_index( )
{
	int32_t ret = ::allocate_clean_get_index( this );
	return ret;
}
#endif

MH_DEF
MH_TYPE *
allocate_clean( MH_FULL_TYPE * mh )
{
	MH_TYPE * ret = allocate( mh );

	if ( ret ) mh_memset( ret, 0, sizeof( MH_TYPE ) );

	return ret;
}

MH_DEF
int32_t
allocate_clean_get_index( MH_FULL_TYPE * mh )
{
	int32_t ret = allocate_get_index( mh );

	if ( ret >= 0 ) mh_memset( mh->mem + ret, 0, sizeof( MH_TYPE ) );

	return ret;
}

#if !MH_NO_MEMBER_FUNCTIONS
void
MH_FULL_TYPE::
deallocate( MH_TYPE * e )
{
	::deallocate( this, e );
}
void
MH_FULL_TYPE::
deallocate_index( uint32_t index )
{
	::deallocate_index( this, index );
}
#endif

MH_DEF
void
deallocate( MH_FULL_TYPE * mh, MH_TYPE * e )
{
	// NOTE(theGiallo): pointers are typed, so pointer arithmetic is in
	// elements, not in bytes
	int id = e - mh->mem;

	int i = id / 255;
	int j = id - i * 255;

	if ( !mh->floor_occupancy[i] )
	{
		MH_ILLEGAL_PATH();
	}

	if ( !MH_BITTEST( mh->booked_bf[i], j ) )
	{
		MH_ILLEGAL_PATH();
	}

	MH_BITCLEAR( mh->booked_bf[i], j );
	--mh->floor_occupancy[i];
	--mh->total_occupancy;
}

MH_DEF
void
deallocate_index( MH_FULL_TYPE * mh, uint32_t index )
{
	int i = index / 255;
	int j = index - i * 255;

	if ( !mh->floor_occupancy[i] )
	{
		MH_ILLEGAL_PATH();
	}

	if ( !MH_BITTEST( mh->booked_bf[i], j ) )
	{
		MH_ILLEGAL_PATH();
	}

	MH_BITCLEAR( mh->booked_bf[i], j );
	--mh->floor_occupancy[i];
	--mh->total_occupancy;
}

#if !MH_NO_MEMBER_FUNCTIONS
MH_TYPE *
MH_FULL_TYPE::
iterate( MH_ITERATOR_TYPE * it )
{
	MH_TYPE * ret = ::iterate( this, it );

	return ret;
}
#endif

MH_DEF
MH_TYPE *
iterate( MH_FULL_TYPE * mh, MH_ITERATOR_TYPE * inout_it )
{
	MH_TYPE * ret = NULL;

	MH_ITERATOR_TYPE local_it = *inout_it;
	MH_ITERATOR_TYPE * it = &local_it;
	for ( ; it->floor < mh->floors_count; it->room = 0, ++it->floor )
	{
		for ( u16 r = it->room; r < 255; ++r )
		{
			if ( MH_BITTEST( mh->booked_bf[it->floor], it->room ) )
			{
				ret = mh->mem + ( ( it->floor * 255 ) + it->room );
				it->room = r + 1;
				*inout_it = local_it;
				return ret;
			}
		}
	}

	*inout_it = local_it;
	return ret;
}

#endif /* if MH_EMIT_BODY */

#undef MH_TYPE
#undef MH_SHORT_TYPE
#undef MH_FULL_TYPE
#undef MH_TYPE_SHORT
#undef MH_BITMASK
#undef MH_BITSLOT
#undef MH_BITSET
#undef MH_BITCLEAR
#undef MH_BITTEST
#undef MH_BITNSLOTS
#undef MH_GLUE2
#undef MH_GLUE2M
#undef MH_ILLEGAL_PATH
#undef MH_FLOORS_COUNT
#undef MH_ITERATOR_TYPE
