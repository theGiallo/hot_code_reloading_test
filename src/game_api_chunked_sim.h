#ifndef GAME_API_CHUNKED_SIM_H
#define GAME_API_CHUNKED_SIM_H 1

#define TG_STATIC_ASSERT(cond) typedef char static_assert_##__LINE__[(cond)?1:-1];

#include "basic_types.h"
#include "tgmath.h"
#include "game_api_lepre.h"

/*
   NOTE(theGiallo): for now we do not check for duplicates when we add an entity
   to a chunk, so it should be up to the simulation to avoid duplicates.

   The entity can be in multiple chunks, but it's simulated only by the owner.
 */

struct
Entity
{
	u64 identifier;
	u32 type;
	void * body;
	s32 owner_chunk_x,
	    owner_chunk_y;
};

struct
Entity_List_Node
{
	Entity * entity;
	Entity_List_Node * next;
};
typedef Entity_List_Node* Entity_List;

#define MH_TYPE Entity_List_Node
#include "mem_hotel.inc.h"

bool
add_entity( Entity * entity, Entity_List * list, Entity_List_Node_MH * list_mh );
bool
remove_entity( Entity * entity, Entity_List * list, Entity_List_Node_MH * list_mh );

struct
Entity_Hash_Table
{
	Entity_List table[1024];
	Entity_List_Node_MH list_mh;
};
bool
remove_entity( Entity * entity, Entity_Hash_Table * ht );
bool
add_entity( Entity * entity, Entity_Hash_Table * ht );

typedef Entity_Hash_Table Entity_Index;

inline
bool
add_entity_to_index( Entity * entity, Entity_Index * index )
{
	bool ret = add_entity( entity, index );

	return ret;
}

enum
class
Neighbors_ID : u8
{
	NORTH_WEST,
	NORTH,
	NORTH_EAST,
	WEST,
	EAST,
	SOUTH_WEST,
	SOUTH,
	SOUTH_EAST,
	// ---
	COUNT
};
struct
Sim_Chunk
{
	Entity_Index entities;
	s32 idx_x,idx_y;
	Sim_Chunk * neighbors[(u8)Neighbors_ID::COUNT];
};

void
simulate_chunk_fixed_timestep( Sim_Chunk * sim_chunk );

void
simulate_chunk( Sim_Chunk * sim_chunk );

#define ENTITY_FLOORS_COUNT 4096

#define MH_TYPE Entity
// NOTE(theGiallo): 255 * 4096 = 1,044,480
#define MH_FLOORS_COUNT ENTITY_FLOORS_COUNT
#include "mem_hotel.inc.h"

#define MH_TYPE Entity_List_Node
// NOTE(theGiallo): 255 * 4096 = 1,044,480
#define MH_FLOORS_COUNT ENTITY_FLOORS_COUNT
#include "mem_hotel.inc.h"

struct
Entities_By_Id
{
	Entity_List table[1024*1024];
	Entity_List_Node_MH_4096 list_mh;
};

struct
Entity_Storage
{
	Entity_MH_4096 mh;
	u64 seeds[2];

	Entities_By_Id entities_by_id;
};

// NOTE(theGiallo): we should not store entities by their pointer because
// doing that we cannot serialize the pointer.
// 1 - We could serialize the pointer relative to the game_mem.
// 2 - We also could store the pointer to the header and the identifier. Then
// at load we could query for the header pointer using the identifier. After
// that could the pointer change? It could change only after a deallocation and
// a deallocation should happen only if the entity is destroyed or if it's
// serialized. In the first case it should never reappear. In the second case
// it would be reloaded at de-serialization.

Entity *
recreate_entity_header( Entity_Storage * storage, u32 type, u64 identifier );

Entity *
create_entity_header( Entity_Storage * storage );

void
destroy_entity_header( Entity_Storage * storage, Entity * entity );

Entity *
find_entity_by_identifier( Entity_Storage * storage, u64 identifier );

////////////////////////////////////////////////////////////////////////////////

// TODO(theGiallo, 2016-10-16): make the type out of a hash of something so that
// one can create a new type of entity without worrying. Make also a table to
// get the string name out of the type value.
#define ENTITY_TYPE_EXAMPLE 0x1

struct
Entity_Example
{
	float v;
};

#define MH_TYPE Entity_Example
#include "mem_hotel.inc.h"

struct
Entity_Example_Storage
{
	Entity_Example_MH mh;
};

Entity *
create_entity_example( Entity_Storage * entity_storage, Entity_Example_Storage * storage );

////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): collision detection

union
Sphere
{
	#pragma pack(push,0)
	struct
	{
		V3 center;
		f32 radius;
	};
	#pragma pack(pop)
	struct
	{
		f32 x,y,z;
		f32 _radius;
	};
};
TG_STATIC_ASSERT(sizeof(Sphere) == 16);
struct
AABB
{
	V3 center_pos;
	V3 half_dimensions;
};

// NOTE(theGiallo): the idea here is to have a Collisions storage and Entities
// to point to a Collisions in there.
// NOTE(theGiallo): simulation idea
// Entities move ( dynamic physics simulation step )
// collisions are checked
// Entities that are colliding have to move back if are relatively solid, and
//    resolve the collision. This could mean simulate an impact, fire sounds,
//    fire particles, or any other kind of effect. For this we need the Entity
//    pointer.
//    I want to try not to simulate again here, but to modify physics attributes
//    and demand the simulation to the next time-step.
enum class
Collidable_Type : u8
{
	SPHERE,
	AABB,
	OOBB,
	CONVEX_POLYGON,
};
struct
Collidable
{
	Collidable_Type type;
	Entity * owner;
};
struct
Collidable_Sphere
{
	Collidable collidable;
	Sphere sphere;
};
struct
Collidable_AABB
{
	Collidable collidable;
	AABB aabb;
};

struct
Contact
{
	V3 world_pos;
};
struct
Collision
{
	Collidable * collidable;
	// TODO(theGiallo, 2016-11-03): contacts should be stored in an external
	// storage or should we store here a max amount?
	Contact * contacts;
	u32 contacts_count;
};
struct
Collision_List_Node
{
	Collision collision;
	Collision_List_Node * next;
};

struct
Collisions
{
	Collision_List_Node * collisions_list;
};

struct
Bounding_Sphere
{
	Sphere sphere;
	Collidable * real_collidable;
	Collisions * collisions;
};
struct
Bounding_Sphere_List_Node
{
	Bounding_Sphere * bounding_sphere;
	Bounding_Sphere_List_Node * next;
};

struct
Bounding_Sphere_Multigrid_Layer
{
	// NOTE(theGiallo): please make this pointer cache-aligned
	Bounding_Sphere_List_Node ** grid;
	u32 grid_side;
};

struct
Bounding_Sphere_Multigrid
{
	Bounding_Sphere_Multigrid_Layer * layers;
	u32 layers_count;
	f32 side_meters;
};

inline
Bounding_Sphere_List_Node **
grid_element( Bounding_Sphere_Multigrid_Layer * bsml, u32 x, u32 y, u32 z )
{
	Bounding_Sphere_List_Node ** ret;

	u32 grid_side = bsml->grid_side;
	if ( grid_side > 32 )
	{
		// NOTE(theGiallo): grid is organized in 2x2x2 chunks,
		// being pointers 8bytes a chunk is 64bytes,
		// usually the size of a cache line
		// TODO(theGiallo, 2016-11-02): cache the cachable values at creation time
		const u32 chunk_side = 2;
		const u32 chunk_volume = cube( chunk_side );
		u32 chunk_x = x / chunk_side;
		u32 chunk_y = y / chunk_side;
		u32 chunk_z = z / chunk_side;
		u32 grid_chunk_side = grid_side / chunk_side;
		lpr_assert( grid_side % chunk_side == 0 );
		ret =
		bsml->grid + ( chunk_x * chunk_volume +
		               chunk_y * grid_chunk_side * chunk_volume +
		               chunk_z * squareu( grid_chunk_side ) * chunk_volume +
		               x % chunk_side +
		               y % chunk_side +
		               z % chunk_side );
	} else
	{
		ret = bsml->grid + ( x + y * grid_side + z * squareu( grid_side ) );
	}

	return ret;
}

void
insert( Bounding_Sphere_Multigrid * bsm, Bounding_Sphere * bounding_sphere );

// NOTE(theGiallo): bs is the bounding sphere previously inserted, with the old
// values. The sphere is removed from the multigrid, its values are replaced
// with the new from new_bs_value, and it is inserted again.
void
move( Bounding_Sphere_Multigrid * bsm,
      Bounding_Sphere * bs, Bounding_Sphere * new_bs_value );

void
collision_check_everything( Bounding_Sphere_Multigrid * bsm );


////////////////////////////////////////////////////////////////////////////////
// NOTE(theGiallo): cache oriented thingy
//
//                            = There is no entity =                          //
//
// An entity is only identified by it's ID. Duh. All its properties are in
// tables, indexed by such ID. If the world was made of a single giant chunk we
// would have a table of each kind, containing all that properties of every
// entity; but we have to divide the world in chunks in order to maintain a
// certain precision in physics and geometric calculations. This clustering
// comes in handy for parallelization. Parallelization requires that every
// cluster stores it's entities properties. Synchronization has to happen for
// in between data, so each cluster has to communicate to 8 neighbours in the
// 2D grid case, and 26 in the 3D grid case. This sync is needed for collisions
// testing, so for physics and area effects, e.g. AOE magic spell or arcade
// vision cone of enemy. Ray-casting could span through many clusters. AOE like
// things, or radial things, should be done in reverse order. There should be a
// list of position-radius couples to be checked, and each cluster should do the
// checking against such list. Maybe the owner chunk should perform the basic
// cluster-vs-sphere check to cull calculations and give the job only to
// intersecting clusters.
//
// # Does the chunk exist?
//
// There is no entity, there are only tables of properties of existing entities.
// A chunk is a d-cube (d={2,3}) of space, identified by 3 signed integers, from
// which we can derive its position in space. If an entity is spatial in some
// way it has an owner chunk that has the task of simulating that entity. For
// convenience we should have a table of each kind for each chunk so that we can
// process each of them in parallel and in a straight way. That would be
// convenient for spatial computations, e.g. physics simulation, but what about
// other code? If there is no dependency between data we could aggregate all
// the entities in a single table and do a streaming computation of them all.
// To parallelize the computation of every kind of aspect we should create
// T chunks, being T the number of threads, different from the physics chunks.
// * BUT * if there is really no dependency we can use a single table and split
// the data and give each thread a piece.
//  Another thing to consider is that adding components should be easy, and
// should be possible to add them in the form of DLs, so it has to be dynamic.
// In the case of storing entities entirely into chunks we need to be able to
// add tables to chunks. In this case we would need to be able to retrieve
// tables in some way. We could simply use a global system to assign an index
// into an array and assign it to some global variable (global not necessary in
// the meaning of C (static)). We would have a function that registers the need
// for a new table, stores into a global state the data necessary to initialize
// it and adds one to every existing chunk. There should be also a function to
// initialize a chunk with all the needed tables taking the init-data from that
// global state. What about removing tables? I.e. what if you disable a mod?
// Would your save game break? I think it's acceptable.
//
// Storing entities data only into the owner chunk means that if we have the
// need to retrieve the entity data given the ID we have to query for it on all
// the chunks. Storing a global Hash table that associates entities IDs to the
// owner chunk_xyz would make that work nicer. That would also mean that we
// would have to change values into this table every time an entity changes
// chunk.

#include "game_api_cod.h"

// NOTE(theGiallo): IMPORTANT don't rearrange these because they are ordered so
// that the inverse neighbor is center-symmetric
#define COD_NEIGHBORS_3D_IDS_AS_VECTORS(n,x,y,z) {x,y,z},
#define COD_NEIGHBORS_3D_IDS_AS_ENUM(n,x,y,z) n,
#define COD_NEIGHBORS_3D_IDS_AS_STRINGS(n,x,y,z) LPR_TOSTRING(n),
#define COD_NEIGHBORS_3D_IDS(ENTRY) \
	ENTRY(COD_NEIGHBOR_3D_PX_PY_0Z, +1,+1, 0) \
	ENTRY(COD_NEIGHBOR_3D_PX_MY_0Z, +1,-1, 0) \
	ENTRY(COD_NEIGHBOR_3D_PX_0Y_0Z, +1, 0, 0) \
	ENTRY(COD_NEIGHBOR_3D_PX_PY_PZ, +1,+1,+1) \
	ENTRY(COD_NEIGHBOR_3D_PX_MY_PZ, +1,-1,+1) \
	ENTRY(COD_NEIGHBOR_3D_PX_0Y_PZ, +1, 0,+1) \
	ENTRY(COD_NEIGHBOR_3D_PX_0Y_MZ, +1, 0,-1) \
	ENTRY(COD_NEIGHBOR_3D_PX_PY_MZ, +1,+1,-1) \
	ENTRY(COD_NEIGHBOR_3D_PX_MY_MZ, +1,-1,-1) \
	ENTRY(COD_NEIGHBOR_3D_0X_PY_0Z,  0,+1, 0) \
	ENTRY(COD_NEIGHBOR_3D_0X_PY_PZ,  0,+1,+1) \
	ENTRY(COD_NEIGHBOR_3D_0X_0Y_PZ,  0, 0,+1) \
	ENTRY(COD_NEIGHBOR_3D_0X_MY_PZ,  0,-1,+1) \
	ENTRY(COD_NEIGHBOR_3D_0X_PY_MZ,  0,+1,-1) \
	ENTRY(COD_NEIGHBOR_3D_0X_0Y_MZ,  0, 0,-1) \
	ENTRY(COD_NEIGHBOR_3D_0X_MY_MZ,  0,-1,-1) \
	ENTRY(COD_NEIGHBOR_3D_0X_MY_0Z,  0,-1, 0) \
	ENTRY(COD_NEIGHBOR_3D_MX_PY_PZ, -1,+1,+1) \
	ENTRY(COD_NEIGHBOR_3D_MX_MY_PZ, -1,-1,+1) \
	ENTRY(COD_NEIGHBOR_3D_MX_0Y_PZ, -1, 0,+1) \
	ENTRY(COD_NEIGHBOR_3D_MX_0Y_MZ, -1, 0,-1) \
	ENTRY(COD_NEIGHBOR_3D_MX_PY_MZ, -1,+1,-1) \
	ENTRY(COD_NEIGHBOR_3D_MX_MY_MZ, -1,-1,-1) \
	ENTRY(COD_NEIGHBOR_3D_MX_0Y_0Z, -1, 0, 0) \
	ENTRY(COD_NEIGHBOR_3D_MX_PY_0Z, -1,+1, 0) \
	ENTRY(COD_NEIGHBOR_3D_MX_MY_0Z, -1,-1, 0) \


enum
COD_Neighbor_3D_ID
{
	COD_NEIGHBORS_3D_IDS(COD_NEIGHBORS_3D_IDS_AS_ENUM)
	//---
	COD_NEIGHBOR_3D_COUNT,
};

extern const V3s32 neighbors_3d_v3s32[COD_NEIGHBOR_3D_COUNT];
extern const char * neighbors_3d_strings[COD_NEIGHBOR_3D_COUNT];

inline
COD_Neighbor_3D_ID
neighbor_3d_id_inverse( COD_Neighbor_3D_ID id )
{
	COD_Neighbor_3D_ID ret =
	   (COD_Neighbor_3D_ID)( COD_NEIGHBOR_3D_COUNT - 1 - id );
	return ret;
}

#define MAX_NUMBER_OF_TABLES_PER_CHUNK 1024
struct
COD_Chunk
{
	COD_Chunk * neighbors[(u32)COD_NEIGHBOR_3D_COUNT];
	Generic_Table_Multicolumn tables[MAX_NUMBER_OF_TABLES_PER_CHUNK];
	s32 idx_x, idx_y, idx_z;
	u32 padding;
};

#define COMPUTE_CHUNK_XYZ_OF_POS( pos, chunk_side ) \
   (make_V3s32( floor( (pos) / (chunk_side) ) ))
#define COMPUTE_CHUNK_XYZ_OF_REL_POS( pos, chunk_xyz, chunk_side ) \
   ((chunk_xyz) + make_V3s32( floor( ( (pos) + ((chunk_side) * 0.5f) ) / (chunk_side) ) ))
#define COMPUTE_CHUNK_DELTA_POS( chunk_dst, chunk_src, chunk_side ) \
   ( (V3)( (chunk_src) - (chunk_dst) ) * ((f32)(chunk_side)) )
#define CONVERT_POS_TO_CHUNK_REL( chunk_dst, chunk_src, chunk_side, pos ) \
   ( (pos) + COMPUTE_CHUNK_DELTA_POS( chunk_dst, chunk_src, chunk_side ) )

#define COD_GLUE2(a,b) a##b
#define COD_GLUE2M(a,b) COD_GLUE2(a,b)
#define COD_CHUNK_MH_FLOORS_COUNT 64
#define MH_FLOORS_COUNT COD_CHUNK_MH_FLOORS_COUNT
#define MAX_CHUNKS_COUNT ( COD_CHUNK_MH_FLOORS_COUNT * 255 )
#define MH_TYPE COD_Chunk
#include "mem_hotel.inc.h"
typedef COD_GLUE2M(COD_Chunk_MH_,COD_CHUNK_MH_FLOORS_COUNT) COD_Chunk_MH_t;

struct
COD_Generic_Table_Multicolumn_Init_Data
{
	u32 * data_elements_sizes;
	u32 data_count;
	u32 capacity;
};

#define MAX_TOTAL_ENTITIES_COUNT (1024UL*1024UL)
#define MAX_AVG_NUMBER_OF_DATA_ELEMENTS_PER_TABLE 128
struct
COD_Global_Tables_Status
{
	// NOTE(theGiallo): you have to give this pool some memory somewhere
	Mem_Pool chunks_tables_mem_pool;

	COD_Chunk_MH_t chunks_mem_hotel;

	// NOTE(theGiallo): these thing needs separate memory
	B12_to_u32_Hash_Table chunks_ht;
	u64_to_B12_Hash_Table ids_to_chunks_ht;

	COD_Generic_Table_Multicolumn_Init_Data
	tables_init_data[MAX_NUMBER_OF_TABLES_PER_CHUNK];
	const u8 * tables_names[MAX_NUMBER_OF_TABLES_PER_CHUNK];

	u32 data_elements_sizes_storage[MAX_NUMBER_OF_TABLES_PER_CHUNK * MAX_AVG_NUMBER_OF_DATA_ELEMENTS_PER_TABLE];
	u32 data_elements_sizes_storage_count;

	u32 registered_tables_count;
};

// NOTE(theGiallo): returns the id of the table in the tables array of chunks.
// Returns negative on error.
s32
register_data_table(
   COD_Global_Tables_Status * global_status,
   COD_Generic_Table_Multicolumn_Init_Data table_init_data,
   const u8 * name = NULL );

bool
initialize_chunk( COD_Global_Tables_Status * global_status,
                  COD_Chunk * chunk );

u32_bool
spawn_chunk( COD_Global_Tables_Status * global_status, V3s32 xyz );

inline
u32_bool
search_chunk( COD_Global_Tables_Status * global_status, V3s32 xyz )
{
	u32_bool ret =
	B12_to_u32_hash_table_retrieve( &global_status->chunks_ht,
	                                B12{ .v3s32 = xyz } );
	return ret;
}

////////////////////////////////////////////////////////////////////////////////

#define STRUCT_XMACRO_AS_SIZES(type,name) sizeof(type),
#define STRUCT_XMACRO_AS_STRUCT(type,name) type name;
#define STRUCT_XMACRO_AS_ENUM(type,name) name,
#define DYNAMIC_PHYSICS_DATA(ENTRY) \
	ENTRY(V3,   pos_0) \
	ENTRY(V3,   vel_0) \
	ENTRY(V3,   acc_0) \
	ENTRY(Quat, rot_0) \
	ENTRY(V3,   ang_vel_0) \
	ENTRY(V3,   ang_acc_0) \
	ENTRY(V3,   pos_1) \
	ENTRY(V3,   vel_1) \
	ENTRY(V3,   acc_1) \
	ENTRY(Quat, rot_1) \
	ENTRY(V3,   ang_vel_1) \
	ENTRY(V3,   ang_acc_1) \
	ENTRY(Mx3,  inverse_inertia_tensor) \
	ENTRY(f32,  inverse_mass) \

struct
Dynamic_Physics_Data
{
	DYNAMIC_PHYSICS_DATA(STRUCT_XMACRO_AS_STRUCT)
};
enum class
Dynamic_Physics_Data_enum
{
	DYNAMIC_PHYSICS_DATA(STRUCT_XMACRO_AS_ENUM)
	//---
	_enum_count
};

#if 0
struct
Dynamic_Physics_Data
{
	V3 pos_0, vel_0, acc_0;
	Quat rot_0;
	V3 ang_vel_0, ang_acc_0;

	V3 pos_1, vel_1, acc_1;
	Quat rot_1;
	V3 ang_vel_1, ang_acc_1;

	Mx3 inverse_inertia_tensor;
};
#endif
extern u32 dynamic_physics_data_elements_sizes[(u32)Dynamic_Physics_Data_enum::_enum_count];

// NOTE(theGiallo): here we probably need an indexing one to many: id_owner -> ids
#define COLLISION_SPHERE(ENTRY) \
	ENTRY(V3, relative_position) \
	ENTRY(f32, radius) \

struct
Collision_Sphere
{
	COLLISION_SPHERE(STRUCT_XMACRO_AS_STRUCT)
};
enum class
Collision_Sphere_enum
{
	COLLISION_SPHERE(STRUCT_XMACRO_AS_ENUM)
	//---
	_enum_count
};
extern u32 collision_sphere_elements_sizes[(u32)Collision_Sphere_enum::_enum_count];

struct
x64_fw_list
{
	union
	{
		u64 value_u64;
		s64 value_s64;
		f64 value_f64;
		void * value_vp;
		u32 values_u32[2];
		s32 values_s32[2];
		f32 values_f32[2];
		u16 values_u16[4];
		s16 values_s16[4];
		u8  values_u8 [8];
		s8  values_s8 [8];
	};
	x64_fw_list * next;
	inline
	void
	insert_head( x64_fw_list * e )
	{
		e->next = this->next;
		this->next = e;
	}
	inline
	void
	insert_tail( x64_fw_list * e )
	{
		x64_fw_list ** curr = &this->next;
		while ( *curr )
		{
			*curr = (*curr)->next;
		}
		*curr = e;
	}
};
union
x128
{
	u64 values_u64[2];
	s64 values_s64[2];
	f64 values_f64[2];
	void * values_vp[2];
	u32 values_u32[4];
	s32 values_s32[4];
	f32 values_f32[4];
	u16 values_u16[8];
	s16 values_s16[8];
	u8  values_u8 [16];
	s8  values_s8 [16];
};
struct
x128_fw_list
{
	union
	{
		u64 values_u64[2];
		s64 values_s64[2];
		f64 values_f64[2];
		void * values_vp[2];
		u32 values_u32[4];
		s32 values_s32[4];
		f32 values_f32[4];
		u16 values_u16[8];
		s16 values_s16[8];
		u8  values_u8 [16];
		s8  values_s8 [16];
	};
	x128_fw_list * next;
	inline
	void
	insert_head( x128_fw_list * e )
	{
		e->next = this->next;
		this->next = e;
	}
	inline
	void
	insert_tail( x128_fw_list * e )
	{
		x128_fw_list ** curr = &this->next;
		while ( *curr )
		{
			*curr = (*curr)->next;
		}
		*curr = e;
	}

	inline
	bool
	contains( x128 value )
	{
		for ( x128_fw_list * n = this; n; n = n->next )
		{
			if ( n->values_u64[0] == value.values_u64[0] &&
			     n->values_u64[1] == value.values_u64[1] )
			{
				return true;
			}
		}
		return false;
	}

	static
	inline
	void
	insert_head( x128_fw_list ** list, x128_fw_list * e )
	{
		if ( *list )
		{
			e->next = (*list)->next;
 			(*list)->next = e;
		} else
		{
			*list = e;
		}
	}
};

#define MH_TYPE x128_fw_list
#include "mem_hotel.inc.h"

#define COLLISIONS_LISTS_TABLE_ENTRY(ENTRY) \
	ENTRY(x128_fw_list*, spheres) \
	ENTRY(x128_fw_list*, planes )

struct
Collisions_Lists_Table_Entry
{
	COLLISIONS_LISTS_TABLE_ENTRY(STRUCT_XMACRO_AS_STRUCT)
};
enum
Collisions_Lists_Table_Entry_enum
{
	COLLISIONS_LISTS_TABLE_ENTRY(STRUCT_XMACRO_AS_ENUM)
	//---
	_enum_count
};
extern u32 collisions_lists_table_entry_elements_sizes[(u32)Collisions_Lists_Table_Entry_enum::_enum_count];

#endif
