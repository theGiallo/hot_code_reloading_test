#ifndef TGUI_DEF
#define TGUI_DEF extern
#endif

#ifndef TGUI_H
#define TGUI_H 1

#include "basic_types.h"
#include "tgmath.h"
#include "game_api.h"
#include "game_api_utility.h"
#include "lepre.h"

enum class
tgui_Orientation
{
	WE = 0x0,
	NS = 0x1,
	SN = 0x2,
	EW = 0x3,
};

struct
tgui_AARect
{
	V2 min, max;
};

struct
tgui_Contextual_Data
{
	struct
	tgui_Button
	{
		V2 max_size;
		V2 min_size;
		V2 size;
		V2 start;
		V2 padding;
		V2 margin;
		bool selected;
		bool unclickable;
		Lpr_Txt_HAlign text_h_align;
		Lpr_Txt_VAlign text_v_align;
		Lpr_Rgba_u8 bg_color_clickable;
		Lpr_Rgba_u8 bg_color_selected;
		Lpr_Rgba_u8 bg_color_unclickable;
		Lpr_Rgba_u8 bg_color_hover;
		Lpr_Rgba_u8 text_color_clickable;
		Lpr_Rgba_u8 text_color_selected;
		Lpr_Rgba_u8 text_color_unclickable;
		Lpr_Rgba_u8 text_color_hover;
	} button;
	struct
	tgui_Text
	{
		V2 max_size;
		V2 min_size;
		V2 size;
		V2 start;
		V2 padding;
		V2 margin;
		Lpr_Txt_HAlign h_align;
		Lpr_Txt_VAlign v_align;
		Lpr_Txt_Wrap   wrap;
		Lpr_Rgba_u8 bg_color;
		Lpr_Rgba_u8 text_color;
	} text;
	struct
	tgui_Sliding_Bar
	{
		V2 size;
		V2 start;
		V2 border;
		V2 margin;
		f32 min_value;
		f32 max_value;
		f32 text_value_width;
		Lpr_Rgba_u8 bar_color;
		Lpr_Rgba_u8 bar_bg_color;
		Lpr_Rgba_u8 bg_color;
		Lpr_Rgba_u8 bg_color_hover;
		Lpr_Rgba_u8 text_color;
		const char * text_value_format;
		tgui_Orientation label_orientation;
	} sliding_bar;
	struct
	tgui_Check_Box
	{
		V2 size;
		V2 start;
		V2 padding;
		V2 margin;
		const char * default_symbol;
		Lpr_Rgba_u8 check_area_color_unchecked;
		Lpr_Rgba_u8 check_area_color_unchecked_hover;
		Lpr_Rgba_u8 check_area_color_checked;
		Lpr_Rgba_u8 check_area_color_checked_hover;
		Lpr_Rgba_u8 border_color_checked;
		Lpr_Rgba_u8 border_color_checked_hover;
		Lpr_Rgba_u8 border_color_unchecked;
		Lpr_Rgba_u8 border_color_unchecked_hover;
		Lpr_Rgba_u8 tick_color;
		Lpr_Rgba_u8 tick_color_hover;
	} check_box;
	struct
	tgui_Line
	{
		bool in_use;
		V2 start;
		// TODO(theGiallo, 2017-01-03): use max length to auto carry line
		f32 max_length;
		f32 curr_length;
		f32 height;
	} line;
};

struct
tgui_Container
{
	tgui_Contextual_Data contextual_data;
	tgui_AARect area;
	tgui_AARect active_area;
	tgui_AARect active_area_plus_scrollbars;
	tgui_AARect area_plus_scrollbars;
	V2 min_size;
	V2 max_size;
	V2 scrolling;
	tgui_AARect extent;
	f32 scrollbar_size;
	u32 fixed_point_type;
	bool with_dynamic_size;
	u8 scrolling_directions;
	u8 scrolling_dynamic_directions;
	u8 stencil_id;
	u8 prev_stencil_id;
};

struct
tgui_Context
{
	// NOTE(theGiallo): this is tied to the lepre max stencils count
	// The first container has no stencil and doesn't have to be started.
	#define TGUI_MAX_CONTAINERS_DEPTH 255
	tgui_Container containers_stack[TGUI_MAX_CONTAINERS_DEPTH];
	s32 containers_curr_id;

	// ---

	struct
	{
		Lpr_Draw_Data * draw_data;
		u32 batch_id;
		Lpr_Multi_Font * multi_font;
		u32 font_id;
		u32 font_size_id;
		u16 bg_z;
		u8  stencil_id;
	} lepre;

	Inputs * inputs;
	s32 window_id;
};

struct
tgui_Button_Events
{
	u32 hover                :  1;
	u32 clicked_left         :  1;
	u32 clicked_right        :  1;
	u32 clicked_wheel        :  1;
	u32 scrolled_up_wheel    :  4;
	u32 scrolled_down_wheel  :  4;
	u32 scrolled_left_wheel  :  4;
	u32 scrolled_right_wheel :  4;
	u32 left_pressed         :  1;
	u32 right_pressed        :  1;
	u32 wheel_pressed        :  1;
	u32 left_down            :  1;
	u32 right_down           :  1;
	u32 wheel_down           :  1;
	u32 left_released        :  1;
	u32 right_released       :  1;
	u32 wheel_released       :  1;
	u32 _padding             :  3;
};

enum
tgui_Fixed_Point_Type
{
	TGUI_BOTTOM_LEFT,
	TGUI_TOP_LEFT,
	TGUI_BOTTOM_RIGHT,
	TGUI_TOP_RIGHT,
	//---
	TGUI_FIXED_POINT_TYPES_COUNT_
};

struct
tgui_Scrolling_Data
{
	tgui_AARect old_extent;
	V2 scrolling;
	V2 mouse_pos;
	bool mouse_now_scrolling_arr[2];
	u8 could_scroll_directions;
};

enum
tgui_Sort_By
{
	TGUI_SORT_BY_NAME,
	TGUI_SORT_BY_SIZE,
	TGUI_SORT_BY_CREATION_DATE,
	TGUI_SORT_BY_MODIFICATION_DATE,
	//---
	TGUI_SORT_BY__COUNT
};
struct
tgui_File_Select_Data
{
// NOTE(theGiallo): these 2 means 16MB for a dir content
#define TGUI_MAX_FILES_IN_DIRECTORY 65536
#define TGUI_MAX_FILE_NAME_BYTES 256
#define TGUI_MAX_FILE_PATH_BYTES 1024
#define TGUI_MAX_FILE_EXTENSION_BYTES 5
#define TGUI_MAX_EXTENSIONS_PER_FILTER 128
	u64  dir_file_sizes       [TGUI_MAX_FILES_IN_DIRECTORY];
	u8   dir_file_names       [TGUI_MAX_FILES_IN_DIRECTORY][TGUI_MAX_FILE_NAME_BYTES];
	u8 * dir_file_names_sorted[TGUI_MAX_FILES_IN_DIRECTORY];
	u8 * tmp_file_names_for_sorting[TGUI_MAX_FILES_IN_DIRECTORY];
	// NOTE(theGiallo): used for the current directory path and for the selected
	// file full path
	u8 file_path[TGUI_MAX_FILE_PATH_BYTES];
	// NOTE(theGiallo): with the point
	u8 filter_extension[TGUI_MAX_EXTENSIONS_PER_FILTER][TGUI_MAX_FILE_EXTENSION_BYTES];
	tgui_Scrolling_Data scrolling_data;
	tgui_Scrolling_Data top_menu_scrolling_data;
	s64 sys_error;
	s64 selected_file_id;
	tgui_Sort_By sort_by;
	Sort_Order   sort_order;
	u32 file_path_null_idx;
	u32  dir_path_null_idx;
	u32 filter_extensions_count;
	u32 total_files_count;
	// NOTE(theGiallo): dirs are stored before files in dir_files_names
	u32 dirs_count;
	bool dir_changed;
	bool has_to_be_sorted;
	// NOTE(theGiallo): aborted means no selection happened, but it was cancelled.
	bool aborted;
	// NOTE(theGiallo): used to reset the data at the first call after closure
	bool active;
	bool error_path_too_long;
	bool select_directory;
	// NOTE(theGiallo): this is inverted to default to 0
	// = don't select files
	bool dont_select_nondirectory;
	bool show_hidden_files;
	bool filter_extensions_boolean;
	bool display_as_columns;
};

tgui_AARect
inline
tgui_aa_rect_intersection( Lpr_AA_Rect r0, Lpr_AA_Rect r1 )
{
	tgui_AARect ret;

	V2 r0_bl = make_V2( r0.pos ) - make_V2( r0.size ) * 0.5f;
	V2 r1_bl = make_V2( r1.pos ) - make_V2( r1.size ) * 0.5f;
	V2 r0_tr = make_V2( r0.pos ) + make_V2( r0.size ) * 0.5f;
	V2 r1_tr = make_V2( r1.pos ) + make_V2( r1.size ) * 0.5f;

	ret.min = max( r0_bl, r1_bl );
	ret.max = min( r0_tr, r1_tr );

	return ret;
}
tgui_AARect
inline
tgui_aa_rect_intersection( tgui_AARect r0, tgui_AARect r1 )
{
	tgui_AARect ret;

	ret.min = max( r0.min, r1.min );
	ret.max = min( r0.max, r1.max );

	return ret;
}

tgui_AARect
inline
tgui_make_AARect( Lpr_AA_Rect r )
{
	tgui_AARect ret;

	ret.min = make_V2( r.pos ) - make_V2( r.size ) * 0.5f;
	ret.max = make_V2( r.pos ) + make_V2( r.size ) * 0.5f;

	return ret;
}

bool
inline
tgui_AARect_is_not_void( tgui_AARect r )
{
	bool ret = r.min.x < r.max.x &&
	           r.min.y < r.max.y;
	return ret;
}
// NOTE(theGiallo): context has to be initialized to 0 outside and then lepre
// data has to be set before calling this
void
inline
tgui_Context_Init( tgui_Context * context )
{
	context->containers_stack[0].stencil_id = LPR_STENCIL_NONE;
	Lpr_Batch_Data * batch =
	   context->lepre.draw_data->batches + context->lepre.batch_id;
	context->containers_stack[0].area =
	context->containers_stack[0].area_plus_scrollbars =
	context->containers_stack[0].active_area =
	context->containers_stack[0].active_area_plus_scrollbars =
	{
	   make_V2(batch->camera_pos) - hadamard( make_V2(batch->camera_world_span), make_V2(batch->vanishing_point) ),
	   make_V2(batch->camera_pos) + hadamard( make_V2(batch->camera_world_span), V2{1.0,1.0} - make_V2(batch->vanishing_point) ),
	};

	static bool single = true;
	if ( single )
	{
		single = false;
		lpr_log_dbg( "sizeof tgui_Context is %lu ( %lukB )", sizeof(tgui_Context), sizeof(tgui_Context)/1000LU );
	}
}

#define TGUI_SCROLLING_H 0x1
#define TGUI_SCROLLING_V 0x2
bool
TGUI_DEF
tgui_container_begin( tgui_Context * context, Lpr_AA_Rect area,
                      f32 scrollbar_size, bool clipped = true );

bool
TGUI_DEF
tgui_container_begin( tgui_Context * context, tgui_Scrolling_Data * scrolling_data,
                      V2 fixed_point, tgui_Fixed_Point_Type fixed_point_type,
                      V2 min_size, V2 max_size,
                      f32 scrollbar_size, bool clipped );

tgui_AARect
TGUI_DEF
tgui_container_end( tgui_Context * context,
                    tgui_Scrolling_Data * scrolling_data );

void
TGUI_DEF
tgui_container_dynamic_scrolling_directions(
   tgui_Context * context,
   u32 scrolling_directions,
   tgui_Scrolling_Data * scrolling_data );

void
TGUI_DEF
tgui_container_permanent_scrolling_directions( tgui_Context * context,
                                               u32 scrolling_directions );

void
TGUI_DEF
tgui_container_set_scrolling( tgui_Context * context, V2 scrolling );

void
TGUI_DEF
tgui_container_scrolling_h( tgui_Context * context,
                            tgui_Scrolling_Data * scrolling_data );

void
TGUI_DEF
tgui_container_scrolling_v( tgui_Context * context,
                            tgui_Scrolling_Data * scrolling_data );

void
TGUI_DEF
tgui_container_background( tgui_Context * context, Lpr_Rgba_u8 bg_color );

tgui_Button_Events
TGUI_DEF
tgui_button( tgui_Context * context, const char * text, u32 text_bytes_size );

void
inline
TGUI_DEF
tgui_button_start_point( tgui_Context * context, V2 start )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	start += container->area.min - container->scrolling;

	container->contextual_data.button.start = start;
}
void
inline
TGUI_DEF
tgui_button_margin( tgui_Context * context, V2 margin )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.margin = margin;
}
void
inline
TGUI_DEF
tgui_button_padding( tgui_Context * context, V2 padding )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.padding = padding;
}
void
inline
TGUI_DEF
tgui_button_size( tgui_Context * context, V2 size )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.size = size;
}
void
inline
TGUI_DEF
tgui_button_min_size( tgui_Context * context, V2 min_size )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.min_size = min_size;
}
void
inline
TGUI_DEF
tgui_button_max_size( tgui_Context * context, V2 max_size )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.max_size = max_size;
}
void
inline
TGUI_DEF
tgui_button_text_color_clickable( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.text_color_clickable = color;
}
void
inline
TGUI_DEF
tgui_button_text_color_unclickable( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.text_color_unclickable = color;
}
void
inline
TGUI_DEF
tgui_button_text_color_selected( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.text_color_selected = color;
}
void
inline
TGUI_DEF
tgui_button_text_color_hover( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.text_color_hover = color;
}
void
inline
TGUI_DEF
tgui_button_bg_color_clickable( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.bg_color_clickable = color;
}
void
inline
TGUI_DEF
tgui_button_bg_color_unclickable( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.bg_color_unclickable = color;
}
void
inline
TGUI_DEF
tgui_button_bg_color_selected( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.bg_color_selected = color;
}
void
inline
TGUI_DEF
tgui_button_bg_color_hover( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.bg_color_hover = color;
}
void
inline
TGUI_DEF
tgui_button_text_h_align( tgui_Context * context, Lpr_Txt_HAlign align )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.text_h_align = align;
}
void
inline
TGUI_DEF
tgui_button_text_v_align( tgui_Context * context, Lpr_Txt_VAlign align )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.text_v_align = align;
}
void
inline
TGUI_DEF
tgui_button_selected( tgui_Context * context, bool selected )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.selected = selected;
}
void
inline
TGUI_DEF
tgui_button_unclickable( tgui_Context * context, bool unclickable )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.button.unclickable = unclickable;
}

void
inline
TGUI_DEF
tgui_text_start_point( tgui_Context * context, V2 start )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	start += container->area.min - container->scrolling;

	container->contextual_data.text.start = start;
}
void
inline
TGUI_DEF
tgui_text_margin( tgui_Context * context, V2 margin )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.text.margin = margin;
}
void
inline
TGUI_DEF
tgui_text_padding( tgui_Context * context, V2 padding )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.text.padding = padding;
}
void
inline
TGUI_DEF
tgui_text_size( tgui_Context * context, V2 size )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.text.size = size;
}
void
inline
TGUI_DEF
tgui_text_min_size( tgui_Context * context, V2 min_size )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.text.min_size = min_size;
}
void
inline
TGUI_DEF
tgui_text_max_size( tgui_Context * context, V2 max_size )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.text.max_size = max_size;
}
void
inline
TGUI_DEF
tgui_text_text_color( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.text.text_color = color;
}
void
inline
TGUI_DEF
tgui_text_bg_color( tgui_Context * context, Lpr_Rgba_u8 color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.text.bg_color = color;
}
void
inline
TGUI_DEF
tgui_text_h_align( tgui_Context * context, Lpr_Txt_HAlign align )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.text.h_align = align;
}
void
inline
TGUI_DEF
tgui_text_v_align( tgui_Context * context, Lpr_Txt_VAlign align )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.text.v_align = align;
}

void
inline
TGUI_DEF
tgui_sliding_bar_size( tgui_Context * context, V2 size )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.size = size;
}

void
inline
TGUI_DEF
tgui_sliding_bar_start( tgui_Context * context, V2 start )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	start += container->area.min - container->scrolling;

	container->contextual_data.sliding_bar.start = start;
}

void
inline
TGUI_DEF
tgui_sliding_bar_border( tgui_Context * context, V2 border )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.border = border;
}

void
inline
TGUI_DEF
tgui_sliding_bar_margin( tgui_Context * context, V2 margin )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.margin = margin;
}

void
inline
TGUI_DEF
tgui_sliding_bar_min_value( tgui_Context * context, f32 min_value )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.min_value = min_value;
}

void
inline
TGUI_DEF
tgui_sliding_bar_max_value( tgui_Context * context, f32 max_value )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.max_value = max_value;
}

void
inline
TGUI_DEF
tgui_sliding_bar_text_value_width( tgui_Context * context, f32 text_value_width )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.text_value_width = text_value_width;
}

void
inline
TGUI_DEF
tgui_sliding_bar_bar_color( tgui_Context * context, Lpr_Rgba_u8 bar_color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.bar_color = bar_color;
}

void
inline
TGUI_DEF
tgui_sliding_bar_bar_bg_color( tgui_Context * context, Lpr_Rgba_u8 bar_bg_color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.bar_bg_color = bar_bg_color;
}

void
inline
TGUI_DEF
tgui_sliding_bar_bg_color( tgui_Context * context, Lpr_Rgba_u8 bg_color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.bg_color = bg_color;
}

void
inline
TGUI_DEF
tgui_sliding_bar_bg_color_hover( tgui_Context * context, Lpr_Rgba_u8 bg_color_hover )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.bg_color_hover = bg_color_hover;
}

void
inline
TGUI_DEF
tgui_sliding_bar_text_color( tgui_Context * context, Lpr_Rgba_u8 text_color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.text_color = text_color;
}

void
inline
TGUI_DEF
tgui_sliding_bar_text_value_format( tgui_Context * context, const char * text_value_format )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.text_value_format = text_value_format;
}

void
inline
TGUI_DEF
tgui_sliding_bar_label_orientation( tgui_Context * context, tgui_Orientation label_orientation )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.sliding_bar.label_orientation = label_orientation;
}

void
inline
TGUI_DEF
tgui_check_box_size( tgui_Context * context, V2 size )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.size = size;
}

void
inline
TGUI_DEF
tgui_check_box_start( tgui_Context * context, V2 start )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	start += container->area.min - container->scrolling;

	container->contextual_data.check_box.start = start;
}

void
inline
TGUI_DEF
tgui_check_box_padding( tgui_Context * context, V2 padding )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.padding = padding;
}

void
inline
TGUI_DEF
tgui_check_box_margin( tgui_Context * context, V2 margin )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.margin = margin;
}

void
inline
TGUI_DEF
tgui_check_box_check_area_color_all(
   tgui_Context * context,
   Lpr_Rgba_u8 check_area_color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.check_area_color_checked =
	container->contextual_data.check_box.check_area_color_checked_hover =
	container->contextual_data.check_box.check_area_color_unchecked =
	container->contextual_data.check_box.check_area_color_unchecked_hover =
	   check_area_color;
}

void
inline
TGUI_DEF
tgui_check_box_check_area_color_all_hover(
   tgui_Context * context,
   Lpr_Rgba_u8 check_area_color_hover )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.check_area_color_checked_hover =
	container->contextual_data.check_box.check_area_color_unchecked_hover =
	   check_area_color_hover;
}

void
inline
TGUI_DEF
tgui_check_box_check_area_color_checked_all(
   tgui_Context * context,
   Lpr_Rgba_u8 check_area_color_checked )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.check_area_color_checked =
	container->contextual_data.check_box.check_area_color_checked_hover =
	   check_area_color_checked;
}

void
inline
TGUI_DEF
tgui_check_box_check_area_color_unchecked_all(
   tgui_Context * context,
   Lpr_Rgba_u8 check_area_color_unchecked )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.check_area_color_unchecked =
	container->contextual_data.check_box.check_area_color_unchecked_hover =
	   check_area_color_unchecked;
}

void
inline
TGUI_DEF
tgui_check_box_check_area_color_unchecked(
   tgui_Context * context,
   Lpr_Rgba_u8 check_area_color_unchecked )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.check_area_color_unchecked = check_area_color_unchecked;
}

void
inline
TGUI_DEF
tgui_check_box_check_area_color_unchecked_hover(
   tgui_Context * context,
   Lpr_Rgba_u8 check_area_color_unchecked_hover )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.check_area_color_unchecked_hover =
	   check_area_color_unchecked_hover;
}

void
inline
TGUI_DEF
tgui_check_box_check_area_color_checked( tgui_Context * context,
                                         Lpr_Rgba_u8 check_area_color_checked )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.check_area_color_checked = check_area_color_checked;
}

void
inline
TGUI_DEF
tgui_check_box_check_area_color_checked_hover(
   tgui_Context * context,
   Lpr_Rgba_u8 check_area_color_checked_hover )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.check_area_color_checked_hover =
	   check_area_color_checked_hover;
}

void
inline
TGUI_DEF
tgui_check_box_border_color_unchecked_all( tgui_Context * context,
                                           Lpr_Rgba_u8 border_color_unchecked )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.border_color_unchecked =
	container->contextual_data.check_box.border_color_unchecked_hover =
	   border_color_unchecked;
}

void
inline
TGUI_DEF
tgui_check_box_border_color_checked_all( tgui_Context * context,
                                         Lpr_Rgba_u8 border_color_checked )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.border_color_checked =
	container->contextual_data.check_box.border_color_checked_hover =
	   border_color_checked;
}

void
inline
TGUI_DEF
tgui_check_box_border_color_all( tgui_Context * context,
                                 Lpr_Rgba_u8 border_color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.border_color_checked =
	container->contextual_data.check_box.border_color_checked_hover =
	container->contextual_data.check_box.border_color_unchecked =
	container->contextual_data.check_box.border_color_unchecked_hover =
	   border_color;
}

void
inline
TGUI_DEF
tgui_check_box_border_color_all_hover( tgui_Context * context,
                                        Lpr_Rgba_u8 border_color_hover )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.border_color_checked_hover =
	container->contextual_data.check_box.border_color_unchecked_hover =
	   border_color_hover;
}

void
inline
TGUI_DEF
tgui_check_box_border_color_checked( tgui_Context * context,
                                     Lpr_Rgba_u8 border_color_checked )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.border_color_checked = border_color_checked;
}

void
inline
TGUI_DEF
tgui_check_box_border_color_checked_hover(
   tgui_Context * context,
   Lpr_Rgba_u8 border_color_checked_hover )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.border_color_checked_hover = border_color_checked_hover;
}

void
inline
TGUI_DEF
tgui_check_box_border_color_unchecked( tgui_Context * context,
                                       Lpr_Rgba_u8 border_color_unchecked )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.border_color_unchecked = border_color_unchecked;
}

void
inline
TGUI_DEF
tgui_check_box_border_color_unchecked_hover(
   tgui_Context * context,
   Lpr_Rgba_u8 border_color_unchecked_hover )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.border_color_unchecked_hover =
	   border_color_unchecked_hover;
}

void
inline
TGUI_DEF
tgui_check_box_tick_color_all( tgui_Context * context, Lpr_Rgba_u8 tick_color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.tick_color =
	container->contextual_data.check_box.tick_color_hover =
	   tick_color;
}

void
inline
TGUI_DEF
tgui_check_box_tick_color( tgui_Context * context, Lpr_Rgba_u8 tick_color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.tick_color = tick_color;
}

void
inline
TGUI_DEF
tgui_check_box_tick_color_hover( tgui_Context * context,
                                 Lpr_Rgba_u8 tick_color_hover )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.check_box.tick_color_hover = tick_color_hover;
}

void
inline
TGUI_DEF
tgui_line_start( tgui_Context * context, V2 start, f32 max_length, f32 height )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	start += container->area.min - container->scrolling;

	container->contextual_data.line.in_use      = true;
	container->contextual_data.line.start       = start;
	container->contextual_data.line.max_length  = max_length;
	container->contextual_data.line.height      = height;
	container->contextual_data.line.curr_length = 0.0f;
}
void
inline
TGUI_DEF
tgui_line_carry( tgui_Context * context )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.line.start.y -= container->contextual_data.line.height;
	container->contextual_data.line.curr_length = 0.0f;
}
void
inline
TGUI_DEF
tgui_line_stop( tgui_Context * context )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->contextual_data.line.in_use = false;
}

f32
TGUI_DEF
tgui_sliding_bar( tgui_Context * context, f32 value, const char * label = NULL );

bool
TGUI_DEF
tgui_check_box( tgui_Context * context, bool value,
                const char * symbol = NULL );

void
TGUI_DEF
tgui_file_select( tgui_Context * context,
                  tgui_File_Select_Data * file_select_data );

bool
inline
TGUI_DEF
tgui_file_select_change_dir( tgui_File_Select_Data * file_select_data,
                             const u8 * start_dir )
{
	int i = 0;
	for ( i = 0; i == 0 || start_dir[i-1] != 0; ++i )
	{
		if ( i == TGUI_MAX_FILE_PATH_BYTES )
		{
			return false;
		}
		file_select_data->file_path[i] = start_dir[i];
	}
	if ( start_dir[i-2] != '/' )
	{
		if ( i == TGUI_MAX_FILE_PATH_BYTES )
		{
			return false;
		}
		file_select_data->file_path[i-1] = '/';
		file_select_data->file_path[i  ] = 0;
		++i;
	}
	file_select_data->file_path_null_idx = i-1;
	file_select_data-> dir_path_null_idx = i-1;
	file_select_data->dir_changed = true;
	return true;
}

bool
inline
TGUI_DEF
tgui_file_select_start_dir( tgui_File_Select_Data * file_select_data,
                            const u8 * start_dir )
{
	bool ret = true;
	if ( !file_select_data->active )
	{
		ret = tgui_file_select_change_dir( file_select_data, start_dir );
	}

	return ret;
}

bool
inline
TGUI_DEF
tgui_file_select_is_active( tgui_File_Select_Data * file_select_data )
{
	return file_select_data->active;
}

bool
inline
TGUI_DEF
tgui_file_select_aborted( tgui_File_Select_Data * file_select_data )
{
	return file_select_data->aborted;
}

inline
TGUI_DEF
const u8 *
tgui_file_select_file_path( tgui_File_Select_Data * file_select_data )
{
	return file_select_data->file_path;
}

void
inline
TGUI_DEF
tgui_file_select_dirs_only( tgui_File_Select_Data * file_select_data )
{
	file_select_data->select_directory = true;
	file_select_data->dont_select_nondirectory = true;
}

void
inline
TGUI_DEF
tgui_file_select_nondirs_only( tgui_File_Select_Data * file_select_data )
{
	file_select_data->select_directory = false;
	file_select_data->dont_select_nondirectory = false;
}

void
inline
TGUI_DEF
tgui_file_select_both_files_and_dirs( tgui_File_Select_Data * file_select_data )
{
	file_select_data->select_directory = true;
	file_select_data->dont_select_nondirectory = false;
}

// NOTE(theGiallo): filters are a white-list or a black-list?
void
inline
TGUI_DEF
tgui_file_select_extension_filters_boolean( tgui_File_Select_Data * file_select_data,
                                            bool boolean )
{
	file_select_data->filter_extensions_boolean = boolean;
}

bool
inline
TGUI_DEF
tgui_file_select_add_extension_filter( tgui_File_Select_Data * file_select_data,
                                       const u8 * extension )
{
	bool ret = false;
	if ( file_select_data->filter_extensions_count >= TGUI_MAX_EXTENSIONS_PER_FILTER )
	{
		return ret;
	}
	u32 bytes_count = str_bytes_to_null( (char*)extension );
	if ( bytes_count > TGUI_MAX_FILE_EXTENSION_BYTES )
	{
		return ret;
	}
	u32 extensions_count = file_select_data->filter_extensions_count;
	for ( s32 i = 0; i != (s32)extensions_count; ++i )
	{
		if ( string_min( (char*)file_select_data->filter_extension[i],
		                 (char*)extension ) == 0 )
		{
			ret = true;
			return ret;
		}
	}
	memcpy( file_select_data->filter_extension[extensions_count],
	        extension, bytes_count );
	++file_select_data->filter_extensions_count;
	ret = true;
	return ret;
}

void
inline
TGUI_DEF
tgui_file_select_clear_extension_filters( tgui_File_Select_Data * file_select_data )
{
	file_select_data->filter_extensions_count = 0;
}

bool
inline
TGUI_DEF
tgui_file_select_got_an_error( tgui_File_Select_Data * file_select_data )
{
	bool ret = file_select_data->sys_error < 0 ||
	           file_select_data->error_path_too_long;
	return ret;
}

s64
inline
TGUI_DEF
tgui_file_select_sys_error( tgui_File_Select_Data * file_select_data )
{
	s64 ret = file_select_data->sys_error;
	return ret;
}

bool
inline
TGUI_DEF
tgui_file_select_error_path_too_long( tgui_File_Select_Data * file_select_data )
{
	bool ret = file_select_data->error_path_too_long;
	return ret;
}

#endif /* ifndef TGUI_H */

#if TGUI_IMPLEMENTATION
#include "game_api_render.h"

void
static
tgui_button_manage_input_( tgui_Context * context, tgui_AARect area, tgui_Button_Events * out_events );

void
TGUI_DEF
tgui_container_compute_active_area_( tgui_Context * context )
{
	lpr_assert( context->containers_curr_id );

	tgui_Container * c =
	   context->containers_stack + context->containers_curr_id;

	u32 prev_container_id = context->containers_curr_id - 1;
	tgui_Container * prev_container =
	   context->containers_stack + prev_container_id;

	tgui_AARect prev_area = prev_container->area;

	c->active_area = tgui_aa_rect_intersection( c->area, prev_area );
	c->active_area_plus_scrollbars =
	   tgui_aa_rect_intersection( c->area_plus_scrollbars, prev_area );
}

void
TGUI_DEF
tgui_container_compute_stencil_( tgui_Context * context, bool do_clear )
{
	tgui_Container * c =
	   context->containers_stack + context->containers_curr_id;

	if ( c->stencil_id != LPR_STENCIL_NONE )
	{
		if ( do_clear )
		{
			lpr_stencil_clear(
			   context->lepre.draw_data, context->lepre.batch_id,
				   c->stencil_id, false );
		}
		lpr_stencil_draw_aa_rect(
		   context->lepre.draw_data, context->lepre.batch_id,
		   c->stencil_id,
		   lpr_AA_Rect_from_minmax( c->area_plus_scrollbars.min, c->area_plus_scrollbars.max ),
		   LPR_STENCIL_DRAW_MODE_SET_ONE,
		   c->prev_stencil_id );
	}
}

bool
TGUI_DEF
tgui_container_begin_( tgui_Context * context, Lpr_AA_Rect area,
                       tgui_Scrolling_Data * scrolling_data,
                       bool with_dynamic_size,
                       V2 fixed_point, tgui_Fixed_Point_Type fixed_point_type,
                       V2 min_size, V2 max_size,
                       f32 scrollbar_size, bool clipped )
{
	bool ret = false;
	bool area_is_null = make_V2(area.pos) == V2{} && make_V2(area.size) == V2{};
	lpr_assert( !area_is_null || f32{} == scrollbar_size || with_dynamic_size );

	if ( context->containers_curr_id != TGUI_MAX_CONTAINERS_DEPTH - 1 )
	{
		++context->containers_curr_id;
		tgui_Container * c =
		   context->containers_stack + context->containers_curr_id;

		lpr_assert( context->containers_curr_id );
		u32 prev_container_id = context->containers_curr_id - 1;
		tgui_Container * prev_container =
		   context->containers_stack + prev_container_id;

		*c = {};
		c->extent.min = V2{FLT_MAX,FLT_MAX};
		c->extent.max = V2{-FLT_MAX,-FLT_MAX};
		memcpy( &c->contextual_data, &prev_container->contextual_data,
		        sizeof(tgui_Contextual_Data) );

		tgui_AARect prev_area = prev_container->area;
		u8 old_stencil = prev_container->stencil_id;
		for ( s32 id = prev_container_id-1;
		      id > 0 && old_stencil == LPR_STENCIL_NONE;
		      --id )
		{
			old_stencil = context->containers_stack[id].stencil_id;
		}
		V2 origin = prev_area.min;
		origin += prev_container->scrolling;

		if ( with_dynamic_size )
		{
			c->min_size = min_size;
			c->max_size = max_size;
			c->with_dynamic_size = true;

			c->area.max = scrolling_data->old_extent.max -
			              scrolling_data->old_extent.min;
			c->area.max = clamp( c->area.max, min_size, max_size );
			V2 size = c->area.max;
			c->area.min += fixed_point;
			c->area.max += fixed_point;
			c->fixed_point_type = fixed_point_type;
			switch ( fixed_point_type )
			{
				case TGUI_BOTTOM_LEFT:
					break;
				case TGUI_TOP_LEFT:
					c->area.min.y -= size.h;
					c->area.max.y -= size.h;
					break;
				case TGUI_BOTTOM_RIGHT:
					c->area.min.x -= size.w;
					c->area.max.x -= size.w;
					break;
				case TGUI_TOP_RIGHT:
					c->area.min.x -= size.w;
					c->area.max.x -= size.w;
					c->area.min.y -= size.h;
					c->area.max.y -= size.h;
					break;
				default:
					break;
			}
			c->area.min += origin;
			c->area.max += origin;
			c->area_plus_scrollbars = c->area;

			c->scrollbar_size = scrollbar_size;

			tgui_container_compute_active_area_( context );
		} else
		{
			if ( area_is_null )
			{
				c->area = prev_container->area;
				c->area_plus_scrollbars = prev_container->area_plus_scrollbars;
				c->active_area = prev_container->active_area;
				c->active_area_plus_scrollbars =
				   prev_container->active_area_plus_scrollbars;
			} else
			{
				c->area      = tgui_make_AARect( area );
				c->area.min += origin;
				c->area.max += origin;
				c->area_plus_scrollbars = c->area;

				c->scrollbar_size = scrollbar_size;

				tgui_container_compute_active_area_( context );
			}
		}

		c->stencil_id = LPR_STENCIL_NONE;
		c->prev_stencil_id = old_stencil;
		if ( clipped && tgui_AARect_is_not_void( c->active_area ) )
		{
			c->stencil_id =
			lpr_new_stencil( context->lepre.draw_data, context->lepre.batch_id );
			tgui_container_compute_stencil_( context, false );
		} else
		{
			ret = true;
		}
	}

	return ret;
}

bool
TGUI_DEF
tgui_container_begin( tgui_Context * context, Lpr_AA_Rect area,
                      f32 scrollbar_size, bool clipped )
{
	bool ret = false;

	ret =
	tgui_container_begin_( context, area,
	                       NULL, false, {}, {}, {}, {},
	                       scrollbar_size, clipped );

	return ret;
}

bool
TGUI_DEF
tgui_container_begin( tgui_Context * context, tgui_Scrolling_Data * scrolling_data,
                      V2 fixed_point, tgui_Fixed_Point_Type fixed_point_type,
                      V2 min_size, V2 max_size,
                      f32 scrollbar_size, bool clipped )
{
	bool ret = false;

	ret =
	tgui_container_begin_( context, {}, scrolling_data, true, fixed_point,
	                       fixed_point_type, min_size, max_size,
	                       scrollbar_size, clipped );

	return ret;
}

tgui_AARect
TGUI_DEF
tgui_container_end( tgui_Context * context,
                    tgui_Scrolling_Data * scrolling_data )
{
	tgui_AARect ret = {};
	tgui_Container * c =
	   context->containers_stack + context->containers_curr_id;

	if ( context->containers_curr_id )
	{
		if ( scrolling_data )
		{
			tgui_container_scrolling_h( context, scrolling_data );
			tgui_container_scrolling_v( context, scrolling_data );
			scrolling_data->old_extent = c->extent;
		}

		--context->containers_curr_id;

		tgui_Container * c_c =
		   context->containers_stack + context->containers_curr_id;
		c_c->extent.min = min( c->area_plus_scrollbars.min, c_c->extent.min );
		c_c->extent.max = max( c->area_plus_scrollbars.max, c_c->extent.max );
	}

	ret = c->area_plus_scrollbars;

	return ret;
}

void
TGUI_DEF
tgui_container_dynamic_scrolling_directions(
   tgui_Context * context,
   u32 scrolling_directions,
   tgui_Scrolling_Data * scrolling_data )
{
	lpr_assert( ( scrolling_directions & 0xFFFFFFFC ) == 0);

	tgui_Container * c =
	   context->containers_stack + context->containers_curr_id;

	c->scrolling_dynamic_directions = scrolling_directions;

	u32 now_could_scroll_directions = 0;
	V2 old_extent_size =
	   scrolling_data->old_extent.max -
	   scrolling_data->old_extent.min;
	if ( old_extent_size.w > c->area.max.x - c->area.min.x )
	{
		now_could_scroll_directions |= TGUI_SCROLLING_H;
	}
	if ( old_extent_size.h > c->area.max.y - c->area.min.y )
	{
		now_could_scroll_directions |= TGUI_SCROLLING_V;
	}

	u32 tmp_scrolling_directions;
	u32 tmp_now_could_scroll_directions = now_could_scroll_directions;
	do
	{
		tmp_scrolling_directions = c->scrolling_directions;

		tmp_scrolling_directions |=
		   tmp_now_could_scroll_directions & scrolling_directions;

		if ( tmp_scrolling_directions & TGUI_SCROLLING_H )
		{
			c->area.min.y = c->area_plus_scrollbars.min.y + c->scrollbar_size;
			if ( old_extent_size.h > c->area.max.y - c->area.min.y &&
			     !( tmp_scrolling_directions & TGUI_SCROLLING_V ) )
			{
				tmp_now_could_scroll_directions |= TGUI_SCROLLING_V;
				continue;
			}
		} else
		{
			c->area.min.y = c->area_plus_scrollbars.min.y;
		}
		if ( tmp_scrolling_directions & TGUI_SCROLLING_V )
		{
			c->area.max.x = c->area_plus_scrollbars.max.x - c->scrollbar_size;
			if ( old_extent_size.w > c->area.max.x - c->area.min.x &&
			     !( tmp_scrolling_directions & TGUI_SCROLLING_H ) )
			{
				tmp_now_could_scroll_directions |= TGUI_SCROLLING_H;
				continue;
			}
		} else
		{
			c->area.max.x = c->area_plus_scrollbars.max.x;
		}

		if ( c->with_dynamic_size )
		{
			if ( tmp_scrolling_directions & TGUI_SCROLLING_H &&
			     c->area_plus_scrollbars.max.y - c->area_plus_scrollbars.min.y + c->scrollbar_size <= c->max_size.h )
			{
				switch ( c->fixed_point_type )
				{
					case TGUI_BOTTOM_LEFT:
					case TGUI_BOTTOM_RIGHT:
						c->area                .max.y += c->scrollbar_size;
						c->area_plus_scrollbars.max.y += c->scrollbar_size;
						break;
					case TGUI_TOP_LEFT:
					case TGUI_TOP_RIGHT:
						c->area                .min.y -= c->scrollbar_size;
						c->area_plus_scrollbars.min.y -= c->scrollbar_size;
						break;
					default:
						break;
				}
				tgui_container_compute_active_area_( context );
				tgui_container_compute_stencil_( context, true );
				tmp_now_could_scroll_directions = now_could_scroll_directions;
				continue;
			}
			if ( tmp_scrolling_directions & TGUI_SCROLLING_V &&
			     c->area_plus_scrollbars.max.x - c->area_plus_scrollbars.min.x + c->scrollbar_size <= c->max_size.w )
			{
				switch ( c->fixed_point_type )
				{
					case TGUI_BOTTOM_LEFT:
					case TGUI_TOP_LEFT:
						c->area                .min.x -= c->scrollbar_size;
						c->area_plus_scrollbars.min.x -= c->scrollbar_size;
						break;
					case TGUI_TOP_RIGHT:
					case TGUI_BOTTOM_RIGHT:
						c->area                .max.x += c->scrollbar_size;
						c->area_plus_scrollbars.max.x += c->scrollbar_size;
						break;
					default:
						break;
				}
				tgui_container_compute_active_area_( context );
				tgui_container_compute_stencil_( context, true );
				tmp_now_could_scroll_directions = now_could_scroll_directions;
				continue;
			}
		}

		break;
	} while ( true );
	c->scrolling_directions = tmp_scrolling_directions;

	tgui_container_compute_active_area_( context );
}


void
TGUI_DEF
tgui_container_permanent_scrolling_directions( tgui_Context * context, u32 scrolling_directions )
{
	lpr_assert( ( scrolling_directions & 0xFFFFFFFC ) == 0);

	tgui_Container * c =
	   context->containers_stack + context->containers_curr_id;

	if ( scrolling_directions & TGUI_SCROLLING_H )
	{
		c->area.min.y = c->area_plus_scrollbars.min.y + c->scrollbar_size;
	} else
	{
		c->area.min.y = c->area_plus_scrollbars.min.y;
	}
	if ( scrolling_directions & TGUI_SCROLLING_V )
	{
		c->area.max.x = c->area_plus_scrollbars.max.x - c->scrollbar_size;
	} else
	{
		c->area.max.x = c->area_plus_scrollbars.max.x;
	}
	c->scrolling_directions |= scrolling_directions;
	c->scrolling_dynamic_directions &= ~scrolling_directions;
	tgui_container_compute_active_area_( context );
}


void
TGUI_DEF
tgui_container_set_scrolling( tgui_Context * context, V2 scrolling )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	container->scrolling = scrolling;
}

void
TGUI_DEF
tgui_container_scrolling_generic_( tgui_Context * context,
                                   tgui_Scrolling_Data * scrolling_data,
                                   u32 dimension_index )
{
	lpr_assert( dimension_index < 2 );

	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	tgui_Contextual_Data * ctx_data = &container->contextual_data;

	f32 bar_height = container->scrollbar_size;
	f32 area_length = container->area.max.arr[dimension_index] -
	                  container->area.min.arr[dimension_index];
	f32 mx = container->extent.max.arr[dimension_index];
	f32 mn = container->extent.min.arr[dimension_index];
	f32 l = 0.0f;
	f32 pos_l = 0.0f;
	f32 scrolling_rails_width = area_length - bar_height * 2.0f;
	f32 max_scrolling = 0.0f;
	f32 min_scrolling = 0.0f;
	if ( mx > mn )
	{
		l = mx - mn;
		pos_l =
		   container->extent.max.arr[dimension_index] +
		   container->scrolling.arr[dimension_index] -
		   container->area.min.arr[dimension_index];
		min_scrolling =
		   container->extent.min.arr[dimension_index] +
		   container->scrolling.arr[dimension_index] -
		   container->area.min.arr[dimension_index];
		max_scrolling =
		   pos_l -
		   area_length;
	}

	bool scrolling_disabled = ( l <= area_length );
	if ( scrolling_disabled )
	{
		scrolling_data->could_scroll_directions &=
		   ~( dimension_index ? TGUI_SCROLLING_V : TGUI_SCROLLING_H );
	} else
	{
		scrolling_data->could_scroll_directions |=
		   dimension_index ? TGUI_SCROLLING_V : TGUI_SCROLLING_H;
	}
	if ( ! ( ( container->scrolling_directions |
	           container->scrolling_dynamic_directions ) &
	         ( dimension_index ? TGUI_SCROLLING_V : TGUI_SCROLLING_H ) ) )
	{
		return;
	}
	bool scrolling_interface_on =
	   container->scrolling_directions &
	   ( dimension_index ? TGUI_SCROLLING_V : TGUI_SCROLLING_H );

	if ( scrolling_disabled )
	{
		if ( dimension_index )
		{
			container->scrolling.arr[dimension_index] = max_scrolling;
			min_scrolling = max_scrolling;
		} else
		{
			container->scrolling.arr[dimension_index] = min_scrolling;
			max_scrolling = min_scrolling;
		}
	}

	if ( scrolling_interface_on )
	{
		#if 0
		{
			u8 str[1024] = {};
			u32 written =
			snprintf( (char*)str, sizeof(str),
			          "%u\n"
			          "min scrolling: %f\n"
			          "max scrolling: %f\n"
			          "    scrolling: %f\n"
			          "min extent   : %f\n"
			          "max extent   : %f\n"
			          "pos_l        : %f\n",
			          dimension_index,
			          min_scrolling,
			          max_scrolling,
			          container->scrolling.arr[dimension_index],
			          container->extent.min.arr[dimension_index],
			          container->extent.max.arr[dimension_index],
			          pos_l );
			V2 scr = container->scrolling;
			container->scrolling = V2{};
			u32 container_id = context->containers_curr_id;
			context->containers_curr_id = 0;
			tgui_line_stop( context );
			tgui_button_text_h_align( context, LPR_TXT_HALIGN_LEFT );
			tgui_button_unclickable( context, true );
			tgui_button_start_point(
			   context,
			   V2{300.0f + 300.0f * (f32)dimension_index,
			      context->lepre.draw_data->window_size.h - 150.0f} );
			tgui_button_size( context, {} );
			tgui_button_max_size( context, {1000.0f,1000.0f} );
			tgui_button( context, (char*)str, written );
			tgui_button_unclickable( context, false );
			context->containers_curr_id = container_id;
			container->scrolling = scr;
		}
		#endif

		tgui_Contextual_Data::tgui_Button orig_button = ctx_data->button;
		bool line_in_use = ctx_data->line.in_use;
		ctx_data->line.in_use = false;
		V2 scr = container->scrolling;
		container->scrolling = V2{};
		ctx_data->button.start =
		   dimension_index ?
		   container->area_plus_scrollbars.max - V2{bar_height,bar_height} :
		   container->area_plus_scrollbars.min;
		ctx_data->button.size         = V2{bar_height,bar_height};
		ctx_data->button.margin       = V2{};
		ctx_data->button.text_h_align = LPR_TXT_HALIGN_CENTERED;
		ctx_data->button.text_v_align = LPR_TXT_VALIGN_MIDDLE;
		ctx_data->button.unclickable  = false;

		container->scrolling = scr;

		tgui_AARect area = container->area;
		container->area = container->area_plus_scrollbars;
		tgui_AARect active_area = container->active_area;
		container->active_area = container->active_area_plus_scrollbars;
		context->lepre.bg_z += 2;
		tgui_AARect extent = container->extent;

		f32 bd = area_length * 0.25f;
		const char * str = dimension_index?u8"▲":u8"◀";
		bool old_unclickable = ctx_data->button.unclickable;
		ctx_data->button.unclickable = scrolling_disabled;
		if ( tgui_button( context, str, str_bytes_to_null(str) ).clicked_left &&
		     !ctx_data->button.unclickable )
		{
			container->scrolling.arr[dimension_index] += dimension_index ? bd : -bd;
		}
		container->extent = extent;

		ctx_data->button.start.arr[dimension_index] =
		   dimension_index ?
		   area.min.y :
		   area.max.x - bar_height;

		str = dimension_index?u8"▼":u8"▶";
		if ( tgui_button( context, str, str_bytes_to_null(str) ).clicked_left &&
		     !ctx_data->button.unclickable )
		{
			container->scrolling.arr[dimension_index] += dimension_index ? -bd : bd;
		}
		ctx_data->button.unclickable = old_unclickable;
		container->extent = extent;

		container->scrolling.arr[dimension_index] =
		   lpr_clamp( container->scrolling.arr[dimension_index],
		              min_scrolling, max_scrolling );

		if ( !scrolling_disabled )
		{
			ctx_data->button.size.arr[dimension_index]  =
			   scrolling_rails_width * ( area_length / l );
			ctx_data->button.start.arr[dimension_index] =
			   dimension_index ?
			   area.max.y - bar_height - ctx_data->button.size.h -
			   scrolling_rails_width *
			   ( ( max_scrolling -
			       container->scrolling.arr[dimension_index] ) / l ) :
			   area.min.arr[dimension_index] + bar_height +
			   scrolling_rails_width *
			   ( ( - min_scrolling +
			       container->scrolling.arr[dimension_index] ) / l );

			tgui_Button_Events e = tgui_button( context, "", 1 );
			container->extent = extent;

			if ( e.left_pressed && !e.left_released )
			{
				scrolling_data->mouse_now_scrolling_arr[dimension_index] = true;
				scrolling_data->mouse_pos = context->inputs->mouse_pos;
			} else
			if ( context->inputs->mouse_button_left_released )
			{
				scrolling_data->mouse_now_scrolling_arr[dimension_index] = false;
			}

			#if 0
			if ( !e.hover )
			{
				scrolling_data->mouse_now_scrolling_arr[dimension_index] = false;
			}
			#endif
			if ( scrolling_data->mouse_now_scrolling_arr[dimension_index] )
			{
				if ( context->inputs->mouse_button_left_down )
				{
					f32 mouse_mov =
					   context->inputs->mouse_pos.arr[dimension_index] -
					   scrolling_data->mouse_pos.arr[dimension_index];
					container->scrolling.arr[dimension_index] +=
					   l * ( mouse_mov / scrolling_rails_width );
					scrolling_data->mouse_pos = context->inputs->mouse_pos;
				} else
				{
					scrolling_data->mouse_now_scrolling_arr[dimension_index] = false;
				}
			}

			e = {};
			tgui_AARect area_plus_scrollbars = container->area_plus_scrollbars;
			tgui_AARect scrolling_area;
			if ( dimension_index )
			{
				scrolling_area = {V2{area.max.x,area_plus_scrollbars.min.y},area_plus_scrollbars.max};
			} else
			{
				scrolling_area = {area_plus_scrollbars.min,V2{area_plus_scrollbars.max.x,area.min.y}};
			}
			tgui_button_manage_input_( context, scrolling_area, &e );
			s32 w = (s32)e.scrolled_down_wheel - (s32)e.scrolled_up_wheel;
			if ( w )
			{
				if ( dimension_index == 1 )
				{
					w = -w;
				}
				container->scrolling.arr[dimension_index] +=
				   l * ( area_length * 0.015625f * w / scrolling_rails_width );
				scrolling_data->mouse_pos = context->inputs->mouse_pos;
			}
		}

		context->lepre.bg_z -= 2;
		container->area = area;
		container->active_area = active_area;

		ctx_data->button = orig_button;
		ctx_data->line.in_use = line_in_use;
	}

	scrolling_data->scrolling.arr[dimension_index] =
	container->scrolling.arr[dimension_index] =
	   lpr_clamp( container->scrolling.arr[dimension_index],
	              min_scrolling, max_scrolling );
}
void
TGUI_DEF
tgui_container_scrolling_h( tgui_Context * context,
                            tgui_Scrolling_Data * scrolling_data )
{
	tgui_container_scrolling_generic_( context, scrolling_data, 0 );
}

void
TGUI_DEF
tgui_container_scrolling_v( tgui_Context * context,
                            tgui_Scrolling_Data * scrolling_data )
{
	tgui_container_scrolling_generic_( context, scrolling_data, 1 );
}

void
TGUI_DEF
tgui_container_background( tgui_Context * context, Lpr_Rgba_u8 bg_color )
{
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	Lpr_AA_Rect rect =
	lpr_AA_Rect_from_minmax( container->area_plus_scrollbars.min,
	                         container->area_plus_scrollbars.max );

	u16 id =
	lpr_add_colored_rect( context->lepre.draw_data,
	                      context->lepre.batch_id,
	                      rect.pos, rect.size, bg_color, 0.0f );
	lpr_set_instance_stencil_id( context->lepre.draw_data,
	                             context->lepre.batch_id,
	                             id, container->stencil_id );
	lpr_set_instance_z( context->lepre.draw_data,
	                    context->lepre.batch_id,
	                    id, context->lepre.bg_z );
}

void
static
tgui_button_manage_input_( tgui_Context * context, tgui_AARect area, tgui_Button_Events * out_events )
{
	if ( context->inputs->mouse_window_id != context->window_id )
	{
		return;
	}

	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;

	if ( container )
	{
		area =
		   tgui_aa_rect_intersection( area, container->active_area );
	}

	if ( point_in_aabb( context->inputs->mouse_pos, area.min, area.max ) )
	{
		if ( context->inputs->mouse_clicked_left )
		{
			out_events->clicked_left = 1;
		}
		if ( context->inputs->mouse_clicked_right )
		{
			out_events->clicked_right = 1;
		}
		if ( context->inputs->mouse_clicked_wheel )
		{
			out_events->clicked_wheel = 1;
		}
		if ( context->inputs->mouse_wheel_y > 0 )
		{
			out_events->scrolled_up_wheel =
			   lpr_min( context->inputs->mouse_wheel_y, 15 );
		}
		if ( context->inputs->mouse_wheel_y < 0 )
		{
			out_events->scrolled_down_wheel =
			   lpr_min( -context->inputs->mouse_wheel_y, 15 );
		}
		if ( context->inputs->mouse_wheel_x > 0 )
		{
			out_events->scrolled_right_wheel =
			   lpr_min( context->inputs->mouse_wheel_x, 15 );
		}
		if ( context->inputs->mouse_wheel_x < 0 )
		{
			out_events->scrolled_left_wheel =
			   lpr_min( -context->inputs->mouse_wheel_x, 15 );
		}
		if ( context->inputs->mouse_button_left_pressed )
		{
			out_events->left_pressed = 1;
		}
		if ( context->inputs->mouse_button_right_pressed )
		{
			out_events->right_pressed = 1;
		}
		if ( context->inputs->mouse_button_wheel_pressed )
		{
			out_events->wheel_pressed = 1;
		}
		if ( context->inputs->mouse_button_left_down )
		{
			out_events->left_down = 1;
		}
		if ( context->inputs->mouse_button_right_down )
		{
			out_events->right_down = 1;
		}
		if ( context->inputs->mouse_button_wheel_down )
		{
			out_events->wheel_down = 1;
		}
		if ( context->inputs->mouse_button_left_released )
		{
			out_events->left_released = 1;
		}
		if ( context->inputs->mouse_button_right_released )
		{
			out_events->right_released = 1;
		}
		if ( context->inputs->mouse_button_wheel_released )
		{
			out_events->wheel_released = 1;
		}
		out_events->hover = 1;
	}
}

tgui_Button_Events
TGUI_DEF
tgui_button( tgui_Context * context, const char * text, u32 text_bytes_size=0 )
{
	tgui_Button_Events ret = {};

	u8 stencil_id = LPR_STENCIL_NONE;
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	stencil_id = container->stencil_id;
	tgui_Contextual_Data * ctx_data = &container->contextual_data;

	if ( !text_bytes_size )
	{
		text_bytes_size = str_bytes_to_null( text );
	}

	lpr_s32 glyph_id;
	Lpr_AA_Rect aa_bb;
	Lpr_V2 ending_pt;

	Lpr_AA_Rect rect;
	V2 min_v, max_v;
	bool tight_x = ctx_data->button.size.w <= 0;
	bool tight_y = ctx_data->button.size.h <= 0;

	if ( ctx_data->line.in_use &&
	     ctx_data->line.curr_length &&
	     ( ctx_data->line.curr_length +
	          ( tight_x ? ctx_data->button.min_size.w : ctx_data->button.size.w )
	          + 2.0f * ctx_data->button.margin.w
	       > ctx_data->line.max_length ) )
	{
		tgui_line_carry( context );
	}

	V2 start;
	if ( ctx_data->line.in_use )
	{
		start.x = ctx_data->line.start.x + ctx_data->line.curr_length;
		start.y = ctx_data->line.start.y;
	} else
	{
		start.x = ctx_data->button.start.x;
		start.y = ctx_data->button.start.y;
	}
	start += ctx_data->button.margin;
	min_v = start;

	if ( !tight_x )
	{
		max_v.x = min_v.x + ctx_data->button.size.w;
	} else
	{
		max_v.x = min_v.x + ctx_data->button.max_size.w;
	}
	if ( !tight_y )
	{
		max_v.y = min_v.y + ctx_data->button.size.h;
	} else
	{
		max_v.y = min_v.y + ctx_data->button.max_size.h;
	}

	rect.size = max_v - min_v;
	rect.size.w = rect.size.w - 2.0f * ctx_data->button.padding.w;
	rect.size.h = rect.size.h - 2.0f * ctx_data->button.padding.h;
	rect.pos = ( max_v + min_v ) * 0.5f;

	{
		tgui_AARect mouse_rect = {min_v, max_v};
		tgui_button_manage_input_( context, mouse_rect, &ret );
	}

	Lpr_Rgba_u8 bg_color, text_color;
	if ( ctx_data->button.selected )
	{
		bg_color   = ctx_data->button.  bg_color_selected;
		text_color = ctx_data->button.text_color_selected;
	} else
	if ( ctx_data->button.unclickable )
	{
		bg_color   = ctx_data->button.  bg_color_unclickable;
		text_color = ctx_data->button.text_color_unclickable;
	} else
	if ( ret.hover )
	{
		bg_color   = ctx_data->button.  bg_color_hover;
		text_color = ctx_data->button.text_color_hover;
	} else
	{
		bg_color   = ctx_data->button.  bg_color_clickable;
		text_color = ctx_data->button.text_color_clickable;
	}

	Lpr_Sprites_Span span = {};
	bool b_res =
	lpr_add_multiline_text_in_rect_aligned(
	   context->lepre.draw_data, context->lepre.batch_id,
	   context->lepre.multi_font,
	   context->lepre.font_id, context->lepre.font_size_id,
	   rect, text, text_bytes_size,
	   ctx_data->button.text_h_align,
	   ctx_data->button.text_v_align,
	   text_color,
	   true, context->lepre.bg_z + context->containers_curr_id * 2 + 1, 1.0f,
	   false, false, stencil_id, &glyph_id, &aa_bb, &ending_pt, &span );
	if ( !b_res )
	{
		lpr_log_err( "lpr_add_multiline_text_in_rect_aligned failed" );
	}


	aa_bb.size.w += 2.0f * ctx_data->button.padding.x;
	aa_bb.size.h += 2.0f * ctx_data->button.padding.y;
	V2 displacement = {};
	if ( tight_x )
	{
		if ( aa_bb.size.w < ctx_data->button.min_size.w )
		{
			switch ( ctx_data->button.text_h_align )
			{
				case LPR_TXT_HALIGN_CENTERED:
					displacement.w =
					   min_v.x + 0.5f * ctx_data->button.min_size.x - aa_bb.pos.x;
					break;
				case LPR_TXT_HALIGN_RIGHT:
					displacement.w =
					   min_v.x + ctx_data->button.min_size.w - ( aa_bb.pos.x + 0.5f * aa_bb.size.x );
					break;
				case LPR_TXT_HALIGN_LEFT:
					break;
			}
			rect.size.w = ctx_data->button.min_size.w;
			rect.pos.x = min_v.x + rect.size.w * 0.5f;
		} else
		if ( aa_bb.size.w < ctx_data->button.max_size.w )
		{
			if ( ctx_data->button.text_h_align == LPR_TXT_HALIGN_RIGHT )
			{
				displacement.w = aa_bb.size.w - ctx_data->button.max_size.w;
			} else
			if ( ctx_data->button.text_h_align == LPR_TXT_HALIGN_CENTERED )
			{
				displacement.w = 0.5f * ( aa_bb.size.w - ctx_data->button.max_size.w );
			}
			rect.size.w = aa_bb.size.w;
			rect.pos.x = aa_bb.pos.x + displacement.x;
		} else
		{
			rect.size.w = ctx_data->button.max_size.w;
			rect.pos.x = min_v.x + 0.5f * ctx_data->button.max_size.x;
		}
	} else
	{
		rect.size.w += 2.0f * ctx_data->button.padding.x;
	}

	if ( tight_y )
	{
		if ( aa_bb.size.h < ctx_data->button.min_size.h )
		{
			switch ( ctx_data->button.text_v_align )
			{
				case LPR_TXT_VALIGN_MIDDLE:
					displacement.h =
					   min_v.y + 0.5f * ctx_data->button.min_size.h - aa_bb.pos.y;
					break;
				case LPR_TXT_VALIGN_TOP:
					displacement.h =
					   min_v.y + ctx_data->button.min_size.h - ( aa_bb.pos.y + 0.5f * aa_bb.size.h );
					break;
				case LPR_TXT_VALIGN_BOTTOM:
					break;
			}
			rect.size.h = ctx_data->button.min_size.h;
			rect.pos.y = min_v.y + rect.size.h * 0.5f;
		}  else
		if ( aa_bb.size.h < ctx_data->button.max_size.h )
		{
			if ( ctx_data->button.text_v_align == LPR_TXT_VALIGN_TOP )
			{
				displacement.h = aa_bb.size.h - ctx_data->button.max_size.h;
			} else
			if ( ctx_data->button.text_v_align == LPR_TXT_VALIGN_MIDDLE )
			{
				displacement.h = 0.5f * ( aa_bb.size.h - ctx_data->button.max_size.h );
			}
			rect.size.h = aa_bb.size.h;
			rect.pos.y = aa_bb.pos.y + displacement.y;
		} else
		{
			rect.size.h = ctx_data->button.max_size.h;
			rect.pos.y = min_v.y + 0.5f * ctx_data->button.max_size.y;
		}
	} else
	{
		rect.size.h += 2.0f * ctx_data->button.padding.y;
	}

	bool changed_line = false;
	if ( ctx_data->line.in_use &&
	     ctx_data->line.curr_length &&
	     ctx_data->line.start.x + ctx_data->line.max_length <
	     rect.pos.x + rect.size.w * 0.5f + ctx_data->button.margin.w )
	{
		V2 old_line_start;
		old_line_start.x = ctx_data->line.start.x + ctx_data->line.curr_length;
		old_line_start.y = ctx_data->line.start.y;
		tgui_line_carry( context );
		V2 d = ctx_data->line.start - old_line_start;
		rect.pos.x   += d.x;
		rect.pos.y   += d.y;
		displacement += d;
		changed_line = true;
	}

	if ( tight_x || tight_y || changed_line )
	{
		ret = {};
		min_v = make_V2( rect.pos ) - make_V2( rect.size ) * 0.5f;
		max_v = min_v + make_V2( rect.size );
		tgui_AARect mouse_rect = {min_v, max_v};
		tgui_button_manage_input_( context, mouse_rect, &ret );

		Lpr_Rgba_u8 new_text_color;
		if ( ctx_data->button.selected )
		{
			bg_color       = ctx_data->button.  bg_color_selected;
			new_text_color = ctx_data->button.text_color_selected;
		} else
		if ( ctx_data->button.unclickable )
		{
			bg_color       = ctx_data->button.  bg_color_unclickable;
			new_text_color = ctx_data->button.text_color_unclickable;
		} else
		if ( ret.hover )
		{
			bg_color       = ctx_data->button.  bg_color_hover;
			new_text_color = ctx_data->button.text_color_hover;
		} else
		{
			bg_color       = ctx_data->button.  bg_color_clickable;
			new_text_color = ctx_data->button.text_color_clickable;
		}
		if ( new_text_color.r != text_color.r ||
		     new_text_color.g != text_color.g ||
		     new_text_color.b != text_color.b ||
		     new_text_color.a != text_color.a )
		{
			lpr_change_color_and_move_sprites(
			   context->lepre.draw_data, context->lepre.batch_id,
			   span, new_text_color, displacement );
		} else
		{
			lpr_assert(
			lpr_move_sprites( context->lepre.draw_data, context->lepre.batch_id,
			                  span, displacement ) );
		}
	}

#if 1
	// TODO(theGiallo, 2017-01-10): Should do this also before calling the multiline text
	tgui_AARect visible_area =
	tgui_aa_rect_intersection( container->active_area_plus_scrollbars, tgui_make_AARect( rect ) );
	if ( visible_area.min.x >= visible_area.max.x ||
	     visible_area.min.y >= visible_area.max.y )
	{
		context->lepre.draw_data->batches[context->lepre.batch_id].quads_count =
		   span.first;
	} else
#endif
	{
		u32 id = lpr_add_colored_rect(
		   context->lepre.draw_data, context->lepre.batch_id,
		   rect.pos, rect.size, bg_color );
		lpr_set_instance_z( context->lepre.draw_data, context->lepre.batch_id,
		                    id, context->lepre.bg_z + context->containers_curr_id * 2 );
		lpr_set_instance_stencil_id( context->lepre.draw_data, context->lepre.batch_id,
		                             id, stencil_id );
	}

	container->extent.max = max( container->extent.max, make_V2(rect.pos) + make_V2(rect.size) * 0.5f + ctx_data->button.margin );
	container->extent.min = min( container->extent.min, make_V2(rect.pos) - make_V2(rect.size) * 0.5f - ctx_data->button.margin );

	if ( ctx_data->line.in_use )
	{
		ctx_data->line.curr_length =
		   rect.pos.x + rect.size.w * 0.5f + ctx_data->button.margin.w -
		   ctx_data->line.start.x;
	}

	return ret;
}

void
TGUI_DEF
tgui_text( tgui_Context * context, const char * text, u32 text_bytes_size=0 )
{
	u8 stencil_id = LPR_STENCIL_NONE;
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	stencil_id = container->stencil_id;
	tgui_Contextual_Data * ctx_data = &container->contextual_data;

	if ( !text_bytes_size )
	{
		text_bytes_size = str_bytes_to_null( text );
	}

	lpr_s32 glyph_id;
	Lpr_V2 ending_pt;

	Lpr_Rot_Rect rect = {};
	V2 min_v, max_v;

	V2 start;
	if ( ctx_data->line.in_use )
	{
		start.x = ctx_data->line.start.x + ctx_data->line.curr_length;
		start.y = ctx_data->line.start.y;
	} else
	{
		start.x = ctx_data->text.start.x;
		start.y = ctx_data->text.start.y;
	}
	start += ctx_data->text.margin;
	min_v = start;

	bool tight_x = ctx_data->text.size.w <= 0;
	bool tight_y = ctx_data->text.size.h <= 0;
	if ( !tight_x )
	{
		max_v.x = min_v.x + ctx_data->text.size.w;
	} else
	{
		if ( ctx_data->text.max_size.w < 0.0 )
		{
			max_v.x = min_v.x + ctx_data->text.min_size.w;
		} else
		{
			max_v.x = min_v.x + ctx_data->text.max_size.w;
		}
	}
	if ( !tight_y )
	{
		max_v.y = min_v.y + ctx_data->text.size.h;
	} else
	{
		if ( ctx_data->text.max_size.h < 0.0 )
		{
			max_v.y = min_v.y + ctx_data->text.min_size.h;
		} else
		{
			max_v.y = min_v.y + ctx_data->text.max_size.h;
		}
	}

	rect.size = max_v - min_v;
	rect.size.w = rect.size.w - 2.0f * ctx_data->text.padding.w;
	rect.size.h = rect.size.h - 2.0f * ctx_data->text.padding.h;
	rect.pos = ( max_v + min_v ) * 0.5f;

	Lpr_Rgba_u8 bg_color, text_color;
	bg_color   = ctx_data->text.  bg_color;
	text_color = ctx_data->text.text_color;

	Lpr_Rot_Rect bb = {};

	Lpr_Sprites_Span span = {};
	bool b_res =
	lpr_add_multiline_text_in_rot_rect_aligned_wrap_clipped(
	   context->lepre.draw_data, context->lepre.batch_id,
	   context->lepre.multi_font,
	   context->lepre.font_id, context->lepre.font_size_id,
	   rect, text, text_bytes_size,
	   ctx_data->text.h_align,
	   ctx_data->text.v_align,
	   ctx_data->text.wrap,
	   text_color,
	   lpr_AA_Rect_from_minmax( container->active_area_plus_scrollbars.min, container->active_area_plus_scrollbars.max ),
	   true, context->lepre.bg_z + context->containers_curr_id * 2 + 1, 1.0f,
	   false, false, true, stencil_id, &glyph_id, &bb, &ending_pt, &span );
	if ( !b_res )
	{
		lpr_log_err( "lpr_add_multiline_text_in_rect_aligned failed" );
	}


	bb.size.w += 2.0f * ctx_data->text.padding.x;
	bb.size.h += 2.0f * ctx_data->text.padding.y;
	V2 displacement = {};
	if ( tight_x )
	{
		if ( bb.size.w < ctx_data->text.min_size.w )
		{
			switch ( ctx_data->text.h_align )
			{
				case LPR_TXT_HALIGN_CENTERED:
					displacement.w =
					   min_v.x + 0.5f * ctx_data->text.min_size.x - bb.pos.x;
					break;
				case LPR_TXT_HALIGN_RIGHT:
					displacement.w =
					   min_v.x + ctx_data->text.min_size.w - ( bb.pos.x + 0.5f * bb.size.x );
					break;
				case LPR_TXT_HALIGN_LEFT:
					break;
			}
			rect.size.w = ctx_data->text.min_size.w;
			rect.pos.x = min_v.x + rect.size.w * 0.5f;
		} else
		if ( bb.size.w < ctx_data->text.max_size.w )
		{
			if ( ctx_data->text.h_align == LPR_TXT_HALIGN_RIGHT )
			{
				displacement.w = bb.size.w - ctx_data->text.max_size.w;
			} else
			if ( ctx_data->text.h_align == LPR_TXT_HALIGN_CENTERED )
			{
				displacement.w = 0.5f * ( bb.size.w - ctx_data->text.max_size.w );
			}
			rect.size.w = bb.size.w;
			rect.pos.x = bb.pos.x + displacement.x;
		} else
		if ( ctx_data->text.max_size.w < 0.0 )
		{
			rect.size.w = bb.size.w;
			rect.pos.x = bb.pos.x;
		} else
		if ( bb.size.w < ctx_data->text.max_size.w )
		{
			rect.size.w = ctx_data->text.max_size.w;
			rect.pos.x = min_v.x + 0.5f * ctx_data->text.max_size.x;
		}
	} else
	{
		rect.size.w += 2.0f * ctx_data->text.padding.x;
	}

	if ( tight_y )
	{
		if ( bb.size.h < ctx_data->text.min_size.h )
		{
			switch ( ctx_data->text.v_align )
			{
				case LPR_TXT_VALIGN_MIDDLE:
					displacement.h =
					   min_v.y + 0.5f * ctx_data->text.min_size.h - bb.pos.y;
					break;
				case LPR_TXT_VALIGN_TOP:
					displacement.h =
					   min_v.y + ctx_data->text.min_size.h - ( bb.pos.y + 0.5f * bb.size.h );
					break;
				case LPR_TXT_VALIGN_BOTTOM:
					break;
			}
			rect.size.h = ctx_data->text.min_size.h;
			rect.pos.y = min_v.y + rect.size.h * 0.5f;
		} else
		if ( bb.size.h < ctx_data->text.max_size.h )
		{
			if ( ctx_data->text.v_align == LPR_TXT_VALIGN_TOP )
			{
				displacement.h = bb.size.h - ctx_data->text.max_size.h;
			} else
			if ( ctx_data->text.v_align == LPR_TXT_VALIGN_MIDDLE )
			{
				displacement.h = 0.5f * ( bb.size.h - ctx_data->text.max_size.h );
			}
			rect.size.h = bb.size.h;
			rect.pos.y = bb.pos.y + displacement.y;
		} else
		if ( ctx_data->text.max_size.h < 0.0f )
		{
			rect.size.h = bb.size.h;
			rect.pos.y = bb.pos.y;
		} else
		{
			rect.size.h = ctx_data->text.max_size.h;
			rect.pos.y = min_v.y + 0.5f * ctx_data->text.max_size.y;
		}
	} else
	{
		rect.size.h += 2.0f * ctx_data->text.padding.y;
	}
	if ( displacement.x || displacement.y )
	{
		lpr_assert(
		   lpr_move_sprites( context->lepre.draw_data, context->lepre.batch_id,
		                     span, displacement ) );
	}

	u32 id = lpr_add_colored_rect(
	   context->lepre.draw_data, context->lepre.batch_id,
	   rect.pos, rect.size, bg_color );
	lpr_set_instance_z( context->lepre.draw_data, context->lepre.batch_id,
	                    id, context->lepre.bg_z + context->containers_curr_id * 2 );
	lpr_set_instance_stencil_id( context->lepre.draw_data, context->lepre.batch_id,
	                             id, stencil_id );

	container->extent.max = max( container->extent.max, make_V2(rect.pos) + make_V2(rect.size) * 0.5f + ctx_data->button.margin );
	container->extent.min = min( container->extent.min, make_V2(rect.pos) - make_V2(rect.size) * 0.5f - ctx_data->button.margin );

	if ( ctx_data->line.in_use )
	{
		ctx_data->line.curr_length =
		   rect.pos.x + rect.size.w * 0.5f + ctx_data->text.margin.w -
		   ctx_data->line.start.x;
	}
}

f32
TGUI_DEF
tgui_sliding_bar( tgui_Context * context, f32 value, const char * label )
{
	f32 ret = value;

	u8 stencil_id = LPR_STENCIL_NONE;
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	stencil_id = container->stencil_id;
	tgui_Contextual_Data * ctx_data = &container->contextual_data;

	V2 start = ctx_data->sliding_bar.start;
	if ( ctx_data->line.in_use )
	{
		start = ctx_data->line.start;
		start.x += ctx_data->line.curr_length;
	}
	start += ctx_data->sliding_bar.margin;

	V2 bg_size = ctx_data->sliding_bar.border * 2.0f + ctx_data->sliding_bar.size;
	f32 whole_end_x =
	   start.x +
	   lpr_max( bg_size.w, ctx_data->sliding_bar.text_value_width ) +
	   ctx_data->sliding_bar.margin.x;
	if ( bg_size.w < ctx_data->sliding_bar.text_value_width )
	{
		start.x += 0.5f * ( ctx_data->sliding_bar.text_value_width - bg_size.w );
	}
	V2 size = bg_size;
	V2 pos = start + size * 0.5f;

	Lpr_Rgba_u8 bg_color     = ctx_data->sliding_bar.bg_color;
	Lpr_Rgba_u8 bar_bg_color = ctx_data->sliding_bar.bar_bg_color;
	Lpr_Rgba_u8 bar_color    = ctx_data->sliding_bar.bar_color;

	V2 mouse_pos = context->inputs->mouse_pos;
	s32 wheel = 0;
	tgui_AARect mouse_rect = {start, start + size};
	if ( container )
	{
		mouse_rect =
		   tgui_aa_rect_intersection( mouse_rect, container->active_area );
	}
	if ( point_in_aabb( mouse_pos, mouse_rect.min, mouse_rect.max ) )
	{
		bg_color = ctx_data->sliding_bar.bg_color_hover;
		wheel = context->inputs->mouse_wheel_y;
	}

	u32 id = lpr_add_colored_rect(
	   context->lepre.draw_data, context->lepre.batch_id,
	   pos, size, bg_color );
	lpr_set_instance_z( context->lepre.draw_data, context->lepre.batch_id,
	                    id, context->lepre.bg_z + context->containers_curr_id * 2 );
	lpr_set_instance_stencil_id( context->lepre.draw_data, context->lepre.batch_id,
	                             id, stencil_id );

	container->extent.max = max( container->extent.max, pos + size * 0.5f + ctx_data->sliding_bar.margin );
	container->extent.min = min( container->extent.min, pos - size * 0.5f - ctx_data->sliding_bar.margin );

	size = ctx_data->sliding_bar.size;
	pos = start + ctx_data->sliding_bar.border + size * 0.5f;
	id = lpr_add_colored_rect(
	   context->lepre.draw_data, context->lepre.batch_id,
	   pos, size, bar_bg_color );
	lpr_set_instance_z( context->lepre.draw_data, context->lepre.batch_id,
	                    id, context->lepre.bg_z + context->containers_curr_id * 2 );
	lpr_set_instance_stencil_id( context->lepre.draw_data, context->lepre.batch_id,
	                             id, stencil_id );

	container->extent.max = max( container->extent.max, pos + size * 0.5f + ctx_data->sliding_bar.margin );
	container->extent.min = min( container->extent.min, pos - size * 0.5f - ctx_data->sliding_bar.margin );

	f32 value_span = ctx_data->sliding_bar.max_value - ctx_data->sliding_bar.min_value;
	V2 min_v = start + ctx_data->sliding_bar.border;
	V2 max_v = min_v + size;
	bool sliding = false;
	mouse_rect = {min_v,max_v};
	if ( container )
	{
		mouse_rect =
		   tgui_aa_rect_intersection( mouse_rect, container->active_area );
	}
	if ( point_in_aabb( mouse_pos, mouse_rect.min, mouse_rect.max ) )
	{
		if ( context->inputs->mouse_button_left_down )
		{
			ret = value =
			   ctx_data->sliding_bar.min_value +
			   value_span * ( ( mouse_pos.y - min_v.y ) / size.h );
			sliding = true;
		}
	}
	if ( !sliding && wheel )
	{
		ret = value =
		lpr_clamp(
		   value +
		   (f32)wheel *
		   ( value_span / ctx_data->sliding_bar.size.h ),
		   ctx_data->sliding_bar.min_value,
		   ctx_data->sliding_bar.max_value );
	}

	size.y *=
	   ( value - ctx_data->sliding_bar.min_value ) / value_span;
	pos = start + ctx_data->sliding_bar.border + size * 0.5f;
	id = lpr_add_colored_rect(
	   context->lepre.draw_data, context->lepre.batch_id,
	   pos, size, bar_color );
	lpr_set_instance_z( context->lepre.draw_data, context->lepre.batch_id,
	                    id, context->lepre.bg_z + context->containers_curr_id * 2 );
	lpr_set_instance_stencil_id( context->lepre.draw_data, context->lepre.batch_id,
	                             id, stencil_id );

	container->extent.max = max( container->extent.max, pos + size * 0.5f + ctx_data->sliding_bar.margin );
	container->extent.min = min( container->extent.min, pos - size * 0.5f - ctx_data->sliding_bar.margin );

	const char * fmt = ctx_data->sliding_bar.text_value_format;
	if ( !fmt )
	{
		fmt = "%0f";
	}
	char str[1024] = {};
	s32 first_free = 0;
	first_free =
	snprintf( str, sizeof( str ) - first_free, fmt, value );

	Lpr_AA_Rect rect, aa_bb;
	rect.size.w = ctx_data->sliding_bar.text_value_width;
	rect.size.h = ctx_data->sliding_bar.text_value_width;
	rect.pos.x = start.w + bg_size.w * 0.5f;
	rect.pos.y = start.h - rect.size.h * 0.5f;
	bool b_res =
	lpr_add_multiline_text_in_rect_aligned(
	   context->lepre.draw_data, context->lepre.batch_id,
	   context->lepre.multi_font,
	   context->lepre.font_id, context->lepre.font_size_id,
	   rect, str, first_free,
	   LPR_TXT_HALIGN_CENTERED,
	   LPR_TXT_VALIGN_TOP,
	   ctx_data->sliding_bar.text_color,
	   true, context->lepre.bg_z + context->containers_curr_id * 2 + 1, 1.0f,
	   false, false, stencil_id, NULL, &aa_bb );
	if ( !b_res )
	{
		lpr_log_err( "lpr_add_multiline_text_in_rect_aligned failed" );
	}

	container->extent.max = max( container->extent.max, make_V2(aa_bb.pos) + make_V2(aa_bb.size) * 0.5f + ctx_data->sliding_bar.margin );
	container->extent.min = min( container->extent.min, make_V2(aa_bb.pos) - make_V2(aa_bb.size) * 0.5f - ctx_data->sliding_bar.margin );

	if ( label )
	{
		switch ( ctx_data->sliding_bar.label_orientation )
		{
			case tgui_Orientation::WE:
				rect.size.w = ctx_data->sliding_bar.text_value_width;
				rect.size.h = ctx_data->sliding_bar.text_value_width;
				rect.pos.y -= aa_bb.size.h;
				b_res =
				lpr_add_multiline_text_in_rect_aligned(
				   context->lepre.draw_data, context->lepre.batch_id,
				   context->lepre.multi_font,
				   context->lepre.font_id, context->lepre.font_size_id,
				   rect, label, str_bytes_to_null( label ),
				   LPR_TXT_HALIGN_CENTERED,
				   LPR_TXT_VALIGN_TOP,
				   ctx_data->sliding_bar.text_color,
				   true, context->lepre.bg_z + context->containers_curr_id * 2 + 1, 1.0f,
				   false, false, stencil_id, NULL, &aa_bb );
				if ( !b_res )
				{
					lpr_log_err( "lpr_add_multiline_text_in_rect_aligned failed" );
				}

				container->extent.max = max( container->extent.max, make_V2(aa_bb.pos) + make_V2(aa_bb.size) * 0.5f + ctx_data->sliding_bar.margin );
				container->extent.min = min( container->extent.min, make_V2(aa_bb.pos) - make_V2(aa_bb.size) * 0.5f - ctx_data->sliding_bar.margin );
				break;
			case tgui_Orientation::NS:
			case tgui_Orientation::SN:
			case tgui_Orientation::EW:
			{
				Lpr_Txt_HAlign h_align;
				Lpr_Txt_VAlign v_align;
				Lpr_Rot_Rect rot_rect;
				rot_rect.pos = rect.pos;
				rot_rect.size.w = rect.size.h;
				rot_rect.size.h = rot_rect.size.w;
				rot_rect.pos.y = start.y - aa_bb.size.h - rot_rect.size.w * 0.5f;
				switch ( ctx_data->sliding_bar.label_orientation )
				{
					case tgui_Orientation::NS:
						rot_rect.rot_deg = -90.0f;
						h_align = LPR_TXT_HALIGN_LEFT;
						v_align = LPR_TXT_VALIGN_MIDDLE;
						break;
					case tgui_Orientation::SN:
						rot_rect.rot_deg = 90.0f;
						h_align = LPR_TXT_HALIGN_RIGHT;
						v_align = LPR_TXT_VALIGN_MIDDLE;
						break;
					case tgui_Orientation::EW:
						rot_rect.rot_deg = 180.0f;
						h_align = LPR_TXT_HALIGN_CENTERED;
						v_align = LPR_TXT_VALIGN_BOTTOM;
						break;
				}
				Lpr_Rot_Rect rot_bb = {};
				b_res =
				lpr_add_multiline_text_in_rot_rect_aligned(
				   context->lepre.draw_data, context->lepre.batch_id,
				   context->lepre.multi_font,
				   context->lepre.font_id, context->lepre.font_size_id,
				   rot_rect, label, str_bytes_to_null( label ),
				   h_align,
				   v_align,
				   ctx_data->sliding_bar.text_color,
				   true, context->lepre.bg_z + context->containers_curr_id * 2 + 1, 1.0f,
				   false, false, false, stencil_id, NULL, &rot_bb );
				if ( !b_res )
				{
					lpr_log_err( "lpr_add_multiline_text_in_rect_aligned failed" );
				}
				V2 max_ex = {};
				V2 min_ex = {};
				switch ( ctx_data->sliding_bar.label_orientation )
				{
					case tgui_Orientation::EW:
						max_ex.x = rot_bb.pos.x + rot_bb.size.w * 0.5f;
						max_ex.y = rot_bb.pos.y + rot_bb.size.h * 0.5f;
						min_ex.x = rot_bb.pos.x - rot_bb.size.w * 0.5f;
						min_ex.y = rot_bb.pos.y - rot_bb.size.h * 0.5f;
						break;
					case tgui_Orientation::NS:
					case tgui_Orientation::SN:
						max_ex.x = rot_bb.pos.x + rot_bb.size.h * 0.5f;
						max_ex.y = rot_bb.pos.y + rot_bb.size.w * 0.5f;
						min_ex.x = rot_bb.pos.x - rot_bb.size.h * 0.5f;
						min_ex.y = rot_bb.pos.y - rot_bb.size.w * 0.5f;
						break;
				}

				container->extent.max = max( container->extent.max, max_ex + ctx_data->sliding_bar.margin );
				container->extent.min = min( container->extent.min, min_ex - ctx_data->sliding_bar.margin );
			}
				break;
			default:
				LPR_ILLEGAL_PATH();
				break;
		}
	}

	if ( ctx_data->line.in_use )
	{
		ctx_data->line.curr_length = whole_end_x - ctx_data->line.start.x;
	}

	return ret;
}

bool
TGUI_DEF
tgui_check_box( tgui_Context * context, bool value, const char * symbol )
{
	bool ret = value;

	u8 stencil_id = LPR_STENCIL_NONE;
	tgui_Container * container =
	   context->containers_stack + context->containers_curr_id;
	stencil_id = container->stencil_id;
	tgui_Contextual_Data * ctx_data = &container->contextual_data;

	V2 start = ctx_data->check_box.start;
	if ( ctx_data->line.in_use )
	{
		start = ctx_data->line.start;
		start.x += ctx_data->line.curr_length;
	}
	start += ctx_data->check_box.margin;

	V2 bg_size = ctx_data->check_box.size;

	Lpr_Rgba_u8 check_area_color;
	Lpr_Rgba_u8 border_color;
	Lpr_Rgba_u8 tick_color;

	bool hover = false;
	V2 mouse_pos = context->inputs->mouse_pos;
	tgui_AARect mouse_rect = {start, start + bg_size};
	if ( container )
	{
		mouse_rect =
		   tgui_aa_rect_intersection( mouse_rect, container->active_area );
	}
	if ( point_in_aabb( mouse_pos, mouse_rect.min, mouse_rect.max ) )
	{
		hover = true;
		if ( context->inputs->mouse_clicked_left )
		{
			ret = value = !value;
		}
	}
	if ( value && hover )
	{
		check_area_color = ctx_data->check_box.check_area_color_checked_hover;
		border_color = ctx_data->check_box.border_color_checked_hover;
		tick_color = ctx_data->check_box.tick_color_hover;
	} else
	if ( value && !hover )
	{
		check_area_color = ctx_data->check_box.check_area_color_checked;
		border_color = ctx_data->check_box.border_color_checked;
		tick_color = ctx_data->check_box.tick_color;
	} else
	if ( !value && hover )
	{
		check_area_color = ctx_data->check_box.check_area_color_unchecked_hover;
		border_color = ctx_data->check_box.border_color_unchecked_hover;
		tick_color = ctx_data->check_box.tick_color_hover;
	} else
	if ( !value && !hover )
	{
		check_area_color = ctx_data->check_box.check_area_color_unchecked;
		border_color = ctx_data->check_box.border_color_unchecked;
		tick_color = ctx_data->check_box.tick_color;
	}

	V2 pos = start + bg_size * 0.5f;
	u32
	id = lpr_add_colored_rect(
	   context->lepre.draw_data, context->lepre.batch_id,
	   pos, bg_size, border_color );
	lpr_set_instance_z( context->lepre.draw_data, context->lepre.batch_id,
	                    id, context->lepre.bg_z + context->containers_curr_id * 2 );
	lpr_set_instance_stencil_id( context->lepre.draw_data, context->lepre.batch_id,
	                             id, stencil_id );
	id = lpr_add_colored_rect(
	   context->lepre.draw_data, context->lepre.batch_id,
	   pos, bg_size - 2.0f * ctx_data->check_box.padding, check_area_color );
	lpr_set_instance_z( context->lepre.draw_data, context->lepre.batch_id,
	                    id, context->lepre.bg_z + context->containers_curr_id * 2 + 1 );
	lpr_set_instance_stencil_id( context->lepre.draw_data, context->lepre.batch_id,
	                             id, stencil_id );

	if ( value )
	{
		if ( !symbol )
		{
			symbol = ctx_data->check_box.default_symbol;
			if ( !symbol )
			{
				symbol = u8"X";
			}
		}
		Lpr_AA_Rect rect;
		rect.pos = pos;
		rect.size = bg_size;
		bool b_res =
		lpr_add_multiline_text_in_rect_aligned(
		   context->lepre.draw_data, context->lepre.batch_id,
		   context->lepre.multi_font,
		   context->lepre.font_id, context->lepre.font_size_id,
		   rect, symbol, str_bytes_to_null( symbol ),
		   LPR_TXT_HALIGN_CENTERED,
		   LPR_TXT_VALIGN_MIDDLE,
		   tick_color,
		   true, context->lepre.bg_z + context->containers_curr_id * 2 + 2, 1.0f,
		   false, false, stencil_id );
		if ( !b_res )
		{
			lpr_log_err( "lpr_add_multiline_text_in_rect_aligned failed" );
		}
	}

	container->extent.max = max( container->extent.max, start + bg_size + ctx_data->check_box.margin );
	container->extent.min = min( container->extent.min, start - ctx_data->check_box.margin );

	if ( ctx_data->line.in_use )
	{
		ctx_data->line.curr_length =
		   start.x + bg_size.w + ctx_data->check_box.margin.w -
		   ctx_data->line.start.x;
	}

	return ret;
}

void
TGUI_DEF
tgui_file_select( tgui_Context * context,
                  tgui_File_Select_Data * fsd )
{
	if ( !fsd->active )
	{
		// NOTE(theGiallo): restoring last dir
		// ( fsd->file_path[fsd->file_path_null_idx-1] != '/' )
		if ( fsd->file_path_null_idx != fsd->dir_path_null_idx )
		{
			fsd->file_path[fsd->dir_path_null_idx] = 0;
			fsd->file_path_null_idx = fsd->dir_path_null_idx;
			fsd->dir_changed = true;
			lpr_log_dbg( "restoring last dir '%s'", fsd->file_path );
		}
		fsd->selected_file_id = -1;
		fsd->aborted = false;
		fsd->sys_error = 0;
		fsd->active = true;

		fsd->scrolling_data = {};
	}

	// NOTE(theGiallo): false loop (to be able to break)
	while ( fsd->dir_changed )
	{
		fsd->dir_changed = false;
		lpr_log_dbg( "entering dir '%s'", fsd->file_path );
		fsd->selected_file_id = -1;
		{
			const u32 mem_buff_bytes = 1024*1024;
			u8 mem_buff[mem_buff_bytes] = {};
			s64 res =
			global_sys_api->list_directory_fast(
			   (char*)fsd->file_path, mem_buff, mem_buff_bytes, 0 );
			if ( SYS_API_RET_IS_ERROR( res ) )
			{
				lpr_log_err( "error listing a directory ('%s'): %s\n%s",
				             fsd->file_path,
				             global_sys_api->get_error_name( res ),
				             global_sys_api->get_error_description( res ) );
				fsd->sys_error = res;
				// TODO(theGiallo, 2017-01-05): if was entered by the user go back
				break;
			}
			fsd->total_files_count = fsd->dirs_count = 0;
			for ( u32 it = 0; it != 2; ++it )
			{
				for ( Sys_Listed_Directory * d =
				         (Sys_Listed_Directory *)( mem_buff + sizeof(u32) );
				      d;
				      d = d->next )
				{
					u32 res =
					str_copy( fsd->file_path + fsd->file_path_null_idx,
					          TGUI_MAX_FILE_PATH_BYTES - fsd->file_path_null_idx,
					          d->name );
					if ( !res )
					{
						lpr_log_err( "file path longer than max (%d B)\n"
						             "  path: '%s'\n"
						             "  file: '%s'",
						             TGUI_MAX_FILE_PATH_BYTES,
						             fsd->file_path,
						             d->name );
						continue;
					}
					res = global_sys_api->file_is_directory( (char*)fsd->file_path );
					if ( SYS_API_RET_IS_ERROR( res ) )
					{
						lpr_log_err( "error listing a directory ('%s'): %s\n%s",
						             fsd->file_path,
						             global_sys_api->get_error_name( res ),
						             global_sys_api->get_error_description( res ) );
						continue;
					}
					if ( ( res && !it ) || ( !res && it ) )
					{
						res =
						   str_copy( fsd->dir_file_names[fsd->total_files_count],
						             TGUI_MAX_FILE_NAME_BYTES,
						             d->name );
						if ( !res )
						{
							lpr_log_err( "file name longer than max (%d B) '%s'",
							             TGUI_MAX_FILE_NAME_BYTES,
							             d->name );
							continue;
						}
						if ( !it )
						{
							++fsd->dirs_count;
						}
						++fsd->total_files_count;
					}
				}
			}
		}
		for ( s32 i = 0; i!= fsd->total_files_count; ++i )
		{
			u8 * file_name = fsd->dir_file_names[i];
			u32 u32_res =
			str_copy( fsd->file_path + fsd->file_path_null_idx,
			          TGUI_MAX_FILE_PATH_BYTES - fsd->file_path_null_idx,
			          file_name );
			if ( !u32_res )
			{
				lpr_log_err( "file path longer than max (%d B)\n"
				             "  path: '%s'\n"
				             "  file: '%s'",
				             TGUI_MAX_FILE_PATH_BYTES,
				             fsd->file_path,
				             file_name );
				continue;
			}
			s64 res = global_sys_api->get_file_size( (const char*)fsd->file_path );
			if ( SYS_API_RET_IS_ERROR( res ) )
			{
				lpr_log_err( "error getting file size of '%s': %s\n%s",
				             fsd->file_path,
				             global_sys_api->get_error_name( res ),
				             global_sys_api->get_error_description( res ) );
			}
			fsd->dir_file_sizes[i] = res;
		}
		for ( u32 i = 0; i!=fsd->total_files_count; ++i )
		{
			fsd->dir_file_names_sorted[i] = fsd->dir_file_names[i];
		}

		fsd->file_path[fsd->dir_path_null_idx] = 0;
		fsd->has_to_be_sorted = true;
		break;
	}
	if ( fsd->has_to_be_sorted )
	{
		fsd->has_to_be_sorted = false;
		u32 ids    [TGUI_MAX_FILES_IN_DIRECTORY];
		u32 ids_tmp[TGUI_MAX_FILES_IN_DIRECTORY];
		u32 count = fsd->dirs_count;
		for ( u32 i = 0; i!=count; ++i )
		{
			ids    [i] = i;
			ids_tmp[i] = i;
		}
		if ( fsd->sort_by == TGUI_SORT_BY_NAME )
		{
			sort_lexicographically_ids_only(
			   (char const**)fsd->dir_file_names_sorted,
			   count,
			   fsd->sort_order,
			   ids, ids_tmp );
		} else
		if ( fsd->sort_by == TGUI_SORT_BY_SIZE )
		{
			radix_sort_mc_64_32( fsd->dir_file_sizes, ids, ids_tmp, count,
			                     fsd->sort_order );
		}
		reorder( ids, fsd->dir_file_sizes, count,
		         sizeof(fsd->dir_file_sizes[0]) );
		reorder( ids, fsd->dir_file_names_sorted, count,
		         sizeof(fsd->dir_file_names_sorted[0]) );

		count = fsd->total_files_count - fsd->dirs_count;
		for ( u32 i = 0; i!=count; ++i )
		{
			ids    [i] = i;
			ids_tmp[i] = i;
		}
		if ( fsd->sort_by == TGUI_SORT_BY_NAME )
		{
			sort_lexicographically_ids_only(
			   (char const**)fsd->dir_file_names_sorted + fsd->dirs_count,
			   count,
			   fsd->sort_order,
			   ids, ids_tmp );
		} else
		if ( fsd->sort_by == TGUI_SORT_BY_SIZE )
		{
			radix_sort_mc_64_32( fsd->dir_file_sizes + fsd->dirs_count,
			                     ids, ids_tmp, count,
			                     fsd->sort_order );
		}
		reorder( ids, fsd->dir_file_sizes + fsd->dirs_count,
		         count,
		         sizeof(fsd->dir_file_sizes[0]) );
		reorder( ids, fsd->dir_file_names_sorted + fsd->dirs_count, count,
		         sizeof(fsd->dir_file_names_sorted[0]) );
	}

	tgui_Container * container =
	   context->containers_stack + ( context->containers_curr_id );
	tgui_AARect c_area = container->active_area;
#if SELECTOR_OWN_CONTAINER
	tgui_container_begin( context, {{0.0,0.0},{0.0,0.0}}, {}, true );
#endif


	// NOTE(theGiallo): bottom line buttons

	f32 bottom_lines_total_height = 0;
#define BUTTONS_DOWN 0
#if BUTTONS_DOWN
	bottom_lines_total_height = 50.0f;
	tgui_button_start_point( context, {} );
	tgui_line_start( context, V2{0.0,10.0}, c_area.max.x - c_area.min.x - 20.0, 24.0 );
	bool can_select =
	   ( fsd->select_directory && fsd->selected_file_id < 0 ) ||
	   ( !fsd->dont_select_nondirectory &&
	        fsd->selected_file_id >= fsd->dirs_count );
	tgui_button_unclickable( context, !can_select );
	if ( tgui_button( context, "select" ).clicked_left && can_select )
	{
		fsd->active = false;
		if ( fsd->selected_file_id >= 0 )
		{
			u32 res =
			str_copy( fsd->file_path + fsd->dir_path_null_idx,
			          TGUI_MAX_FILE_NAME_BYTES - fsd->dir_path_null_idx,
			          fsd->dir_file_names_sorted[fsd->selected_file_id] );
			if ( !res )
			{
				lpr_log_err( "file + path make a path longer than the max (%u)\n"
				             "  path: '%s'\n"
				             "  file: '%s'",
				             TGUI_MAX_FILE_PATH_BYTES,
				             fsd->file_path,
				             fsd->dir_file_names_sorted[fsd->selected_file_id] );
				fsd->error_path_too_long = true;
			}
			fsd->file_path_null_idx += res;
		}
	}
	tgui_button_unclickable( context, false );
	if ( tgui_button( context, "cancel" ).clicked_left )
	{
		fsd->active = false;
		fsd->aborted = true;
	}
	tgui_line_stop( context );
#endif

	f32 top_lines_height = 32.0f;
	// NOTE(theGiallo): top buttons container
	tgui_container_begin( context, &fsd->top_menu_scrolling_data,
	                      V2{ 0.0f, c_area.max.y - c_area.min.y },
	                      TGUI_TOP_LEFT,
	                      V2{ c_area.max.w - c_area.min.w,
	                          top_lines_height },
	                      V2{ c_area.max.w - c_area.min.w,
	                          top_lines_height * 2.0f + 24.0f },
	                      24.0f, true );
	tgui_container_dynamic_scrolling_directions(
	   context, TGUI_SCROLLING_H | TGUI_SCROLLING_V,
	   &fsd->top_menu_scrolling_data );
	tgui_container_set_scrolling( context, fsd->top_menu_scrolling_data.scrolling );
	tgui_container_background( context, lpr_C_colors_u8.white );

	// NOTE(theGiallo): top line buttons and labels
	f32 top_lines_total_height = top_lines_height;

	tgui_button_start_point( context, {} );
	tgui_line_start( context,
	                 V2{0.0f, 0.0f},
	                 FLT_MAX,
	                 top_lines_height );
	tgui_button_margin( context,  V2{2.0f,2.0f} );
	tgui_button_min_size( context,  V2{28.0f,28.0f} );
	tgui_button_max_size( context,  V2{1000.0f,28.0f} );

	tgui_button_unclickable( context, false );
	if ( tgui_button( context, "↻" ).clicked_left )
	{
		fsd->dir_changed = true;
	}
	tgui_button_unclickable( context, fsd->dir_path_null_idx < 3 );
	if ( tgui_button( context, "↩" ).clicked_left )
	{
		for ( s64 i = (s64)fsd->dir_path_null_idx - 2;
		      i >= 0;
		      --i )
		{
			if ( fsd->file_path[i] == '/' )
			{
				fsd->file_path_null_idx =
				fsd->dir_path_null_idx = i + 1;
				fsd->file_path[fsd->dir_path_null_idx] = 0;
				lpr_log_dbg( "%s", fsd->file_path );
				fsd->dir_changed = true;
				break;
			}
		}
	}
	tgui_button_unclickable( context, true );
	tgui_button( context, (char*)fsd->file_path, fsd->dir_path_null_idx );
	tgui_button_unclickable( context, true );
	if ( fsd->selected_file_id >= fsd->dirs_count )
	{
		tgui_button( context, (char*)fsd->dir_file_names[fsd->selected_file_id] );
	}
	tgui_line_carry( context );
	top_lines_total_height += top_lines_height;

#if !BUTTONS_DOWN
	bool can_select =
	   ( fsd->select_directory && fsd->selected_file_id < 0 ) ||
	   ( !fsd->dont_select_nondirectory &&
	        fsd->selected_file_id >= fsd->dirs_count );
	tgui_button_unclickable( context, !can_select );
	if ( tgui_button( context, "select" ).clicked_left && can_select )
	{
		fsd->active = false;
		if ( fsd->selected_file_id >= 0 )
		{
			u32 res =
			str_copy( fsd->file_path + fsd->dir_path_null_idx,
			          TGUI_MAX_FILE_NAME_BYTES - fsd->dir_path_null_idx,
			          fsd->dir_file_names[fsd->selected_file_id] );
			if ( !res )
			{
				lpr_log_err( "file + path make a path longer than the max (%u)\n"
				             "  path: '%s'\n"
				             "  file: '%s'",
				             TGUI_MAX_FILE_PATH_BYTES,
				             fsd->file_path,
				             fsd->dir_file_names[fsd->selected_file_id] );
				fsd->error_path_too_long = true;
			}
			fsd->file_path_null_idx += res;
		}
	}
	tgui_button_unclickable( context, false );
	if ( tgui_button( context, "cancel" ).clicked_left )
	{
		fsd->active = false;
		fsd->aborted = true;
	}
#endif

	tgui_button_unclickable( context, false );
	if ( tgui_button( context,
	                  fsd->sort_order==SORT_ASCENDANTLY?"sort ▲":"sort ▼" ).clicked_left )
	{
		fsd->sort_order =
		   fsd->sort_order == SORT_ASCENDANTLY ?
		   SORT_DESCENDANTLY : SORT_ASCENDANTLY;
		fsd->has_to_be_sorted = true;
	}
	{
		bool clickable = fsd->sort_by != TGUI_SORT_BY_NAME;
		tgui_button_selected( context, !clickable );
		if ( tgui_button( context, "by name" ).clicked_left && clickable )
		{
			fsd->sort_by = TGUI_SORT_BY_NAME;
			fsd->has_to_be_sorted = true;
		}
		clickable = fsd->sort_by != TGUI_SORT_BY_SIZE;
		tgui_button_selected( context, !clickable );
		if ( tgui_button( context, "by size" ).clicked_left && clickable )
		{
			fsd->sort_by = TGUI_SORT_BY_SIZE;
			fsd->has_to_be_sorted = true;
		}
		tgui_button_selected( context, false );
	}
	// tgui_button_unclickable( context, false );
	if ( tgui_button( context,
	                  fsd->show_hidden_files ?
	                  "○ ◌ showing hidden" :
	                  "○ - hiding  hidden" ).clicked_left )
	{
		fsd->show_hidden_files = !fsd->show_hidden_files;
	}
	tgui_button_text_h_align( context, LPR_TXT_HALIGN_LEFT );
	if ( tgui_button( context,
	                  fsd->display_as_columns ?
	                  "▬▬  \n▬" :
	                  "▬▬ ▬\n▬ ▬" ).clicked_left )
	{
		fsd->display_as_columns = !fsd->display_as_columns;
	}
	tgui_button_text_h_align( context, LPR_TXT_HALIGN_CENTERED );
	tgui_button_unclickable( context, true );
	{
		char str[128];
		s32 len =
		snprintf( str, sizeof(str), "%ld %s",
		          fsd->total_files_count, "total files count" );
		tgui_button( context, str, len );
	}
	tgui_line_stop( context );

	// NOTE(theGiallo): top menu container
	tgui_AARect top_menu_area =
	tgui_container_end( context, &fsd->top_menu_scrolling_data );


	// NOTE(theGiallo): directory content

	tgui_AARect files_area =
	   { V2{0.0f,0.0f},//bottom_lines_total_height},
	     V2{c_area.max.x-c_area.min.x,top_menu_area.min.y-c_area.min.y} };
	Lpr_AA_Rect files_area_lpr =
	   lpr_AA_Rect_from_minmax( files_area.min, files_area.max );

	tgui_container_begin(
	   context,
	   files_area_lpr,
	   24.0, true );
	tgui_container_dynamic_scrolling_directions(
	   context,
	   TGUI_SCROLLING_H | TGUI_SCROLLING_V,
	   &fsd->scrolling_data );
	tgui_container_background( context, lpr_C_colors_u8.white );

	tgui_Container * files_container = context->containers_stack + context->containers_curr_id;
	tgui_container_set_scrolling( context, fsd->scrolling_data.scrolling );
	bool changed_dir = false;
	tgui_button_size( context, {} );
	tgui_button_margin( context, {2.0f,2.0f} );
	tgui_button_min_size( context,  V2{24.0f,24.0f} );
	tgui_button_max_size( context,  V2{1000.0f,1000.0f} );
	tgui_button_text_h_align( context, LPR_TXT_HALIGN_LEFT );
	tgui_line_start( context, {0.0f,276.0f},
	                 fsd->display_as_columns ?
	                    10000.0f :
	                    files_area.max.x - files_area.min.x - 20.0f -
	                    (f32)files_container->scrollbar_size,
	                 28.0f);
	tgui_button_unclickable( context, false );
	Lpr_Rgba_u8 original_bg_color_unclickable = files_container->contextual_data.button.bg_color_unclickable;
	Lpr_Rgba_u8 original_bg_color_clickable   = files_container->contextual_data.button.bg_color_clickable;
	Lpr_Rgba_u8 original_bg_color_hover       = files_container->contextual_data.button.bg_color_hover;
	files_container->contextual_data.button.bg_color_unclickable = palette_pastel[palette_pastel_granny_smith];
	files_container->contextual_data.button.bg_color_unclickable.r *= 0.6f;
	files_container->contextual_data.button.bg_color_unclickable.g *= 0.6f;
	files_container->contextual_data.button.bg_color_unclickable.b *= 0.6f;
	files_container->contextual_data.button.bg_color_clickable = palette_pastel[palette_pastel_granny_smith];
	files_container->contextual_data.button.bg_color_clickable.r *= 0.9f;
	files_container->contextual_data.button.bg_color_clickable.g *= 0.9f;
	files_container->contextual_data.button.bg_color_clickable.b *= 0.9f;
	files_container->contextual_data.button.bg_color_hover = palette_pastel[palette_pastel_pancho];
	u64 filedirs_visuzlized = 0;
	s64 new_dir_id = -1;
	for ( s64 i = 0; i!=fsd->total_files_count; ++i )
	{
		if ( i == fsd->dirs_count )
		{
			files_container->contextual_data.button.bg_color_unclickable =
			   original_bg_color_unclickable;
			files_container->contextual_data.button.bg_color_clickable =
			   original_bg_color_clickable;
			files_container->contextual_data.button.bg_color_hover =
			   original_bg_color_hover;
			if ( filedirs_visuzlized )
			{
				tgui_line_carry( context );
			}
		}

		// NOTE(theGiallo): extension filters
		bool filter_matches = true;
		for ( s32 j = 0; j!=fsd->filter_extensions_count; ++j )
		{
			u32 e_l = str_bytes_to_null_or_end( (char*)fsd->filter_extension[j],
			                                    TGUI_MAX_FILE_EXTENSION_BYTES );
			u32 f_l = str_bytes_to_null( (char*)fsd->dir_file_names_sorted[i] );
			for ( s32 k = 0; k != e_l && filter_matches; ++k )
			{
				filter_matches = filter_matches &&
				   ( fsd->filter_extension     [j][e_l-1-k] ==
				     fsd->dir_file_names_sorted[i][f_l-1-k] );
			}
			if ( filter_matches )
			{
				break;
			}
		}
		bool hidden_permission =
		   ( fsd->show_hidden_files || fsd->dir_file_names_sorted[i][0] != '.' );
		bool filter_permission =
		   ( i < fsd->dirs_count ||
		     filter_matches == fsd->filter_extensions_boolean );
		if ( hidden_permission && filter_permission )
		{
			++filedirs_visuzlized;
			if ( fsd->display_as_columns )
			{
				char str[1280];
				snprintf( str, sizeof(str), "%10lu B", fsd->dir_file_sizes[i] );
				tgui_button_unclickable( context, true );
				tgui_button_selected( context, false );
				tgui_button( context, str );
			}

			tgui_button_unclickable( context, false );
			tgui_button_selected( context, fsd->selected_file_id == i );
			if ( tgui_button( context, (char*)fsd->dir_file_names_sorted[i] ).clicked_left )
			{
				if ( fsd->selected_file_id == i )
				{
					fsd->selected_file_id = -1;
				} else
				{
					if ( i < fsd->dirs_count )
					{
						fsd->dir_changed = true;
						new_dir_id = i;
					} else {
						fsd->selected_file_id = i;
					}
				}
			}
			if ( fsd->display_as_columns )
			{
				tgui_line_carry( context );
			}
		}
	}
	files_container->contextual_data.button.bg_color_unclickable =
	   original_bg_color_unclickable;
	files_container->contextual_data.button.bg_color_clickable =
	   original_bg_color_clickable;
	files_container->contextual_data.button.bg_color_hover =
	   original_bg_color_hover;
	tgui_line_stop( context );
	tgui_container_end( context, &fsd->scrolling_data );

#if SELECTOR_OWN_CONTAINER
	tgui_container_end( context, NULL );
#endif

	if ( fsd->dir_changed && new_dir_id >= 0 )
	{
		u32 res =
		str_copy( fsd->file_path + fsd->dir_path_null_idx,
		          TGUI_MAX_FILE_PATH_BYTES - fsd->dir_path_null_idx,
		          fsd->dir_file_names_sorted[new_dir_id] );
		if ( !res )
		{
			lpr_log_err( "file path longer than max (%d B)\n"
			             "  path: '%s'\n"
			             "  file: '%s'",
			             TGUI_MAX_FILE_PATH_BYTES,
			             fsd->file_path,
			             fsd->dir_file_names_sorted[new_dir_id] );
			fsd->error_path_too_long = true;
		} else
		{
			fsd->dir_path_null_idx += res - 1;
			fsd->file_path_null_idx = fsd->dir_path_null_idx;
			if ( !( TGUI_MAX_FILE_PATH_BYTES - fsd->file_path_null_idx ) )
			{
				lpr_log_err( "new dir path is longer than maximum ( %u B ): %s/",
				             TGUI_MAX_FILE_PATH_BYTES, fsd->file_path );
				fsd->error_path_too_long = true;
			} else
			{
				fsd->file_path[fsd->dir_path_null_idx] = '/';
				++fsd->dir_path_null_idx;
				fsd->file_path_null_idx = fsd->dir_path_null_idx;
				fsd->file_path[fsd->dir_path_null_idx] = 0;
			}
		}
	}
}

#endif
