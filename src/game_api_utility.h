#ifndef GAME_API_UTILITY_H
#define GAME_API_UTILITY_H 1

#include "basic_types.h"
#include "game_api_lepre.h"

#define ARRAY_COUNT( a ) (sizeof(a) / sizeof((a)[0]))
#define SWAP_T(t,a,b) do{ t _tmp_for_swap_ = (a); (a) = (b); (b) = _tmp_for_swap_; } while(0)

// NOTE(theGiallo): the first time pass *internal_bit = 0
internal
u8
rand_bit( u64 seeds[2], u64 * internal_data,
          u8 * internal_bit_id );

internal
f32
randd_between( u64 seeds[2], f32 left, f32 right );

// NOTE(theGiallo): random u32 in [min, max] interval
internal
u32
randu_between( u64 seeds[2], u32 min, u32 max );

// NOTE(theGiallo): random u32 in [0, count-1] interval
internal
u32
randu_with_distribution( u64 seeds[2], f32 * probability_distribution, u32 count );

internal
f32
randd_01( u64 seeds[2] );

struct
Marsaglia_Gauss_Data
{
	f64 v1, v2, s, u1, u2;
	s32 phase;
	u64 seeds[2];
};
internal
f64
randd_gauss_01( Marsaglia_Gauss_Data * g_data );

inline
internal
f64
randd_gauss( Marsaglia_Gauss_Data * g_data, f64 mean, f64 sd )
{
	f64 ret = randd_gauss_01( g_data ) * sd + mean;

	return ret;
}

inline
internal
f64
randd_gauss_clamped( Marsaglia_Gauss_Data * g_data, f64 mean, f64 sd,
                     f64 min, f64 max )
{
	f64 ret;
	do
	{
		ret = randd_gauss( g_data, mean, sd );
	} while ( ret < min || ret > max );

	return ret;
}

internal
u32
u32_FNV1a( const char * str );

internal
u32
u32_FNV1a( const void * str, u32 bytes_size );

internal
inline
f32
f32_from_u32_after_radixsort( u32 uf )
{
	f32 ret;
	u32 mask = ( ( uf >> 31 ) - 1 ) | 0x80000000;
	uf = uf ^ mask;
	ret = *(f32*)&uf;

	return ret;
}
enum
Sort_Order : u8
{
	SORT_ASCENDANTLY,
	SORT_DESCENDANTLY
};

internal
void
radix_sort_f32( f32 * keys_arr, u32 keys_arr_size, u16 * ids,
                Sort_Order sort_order,
                bool restore_keys_values = false );

internal
void
sort_lpr_batch_f32( Lpr_Draw_Data * draw_data, u32 batch_id,
                    f32 * keys_arr, bool restore_keys_values = false );

internal
void
radix_sort_mc_32_keys_only( u32 * keys, u32 * tmp, u32 count, Sort_Order sort_order = SORT_ASCENDANTLY );

internal
void
radix_sort_mc_32_16( const u32 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order = SORT_ASCENDANTLY );

internal
void
radix_sort_mc_64_16( const u64 * keys, u16 * ids, u16 * tmp, u32 count, Sort_Order sort_order = SORT_ASCENDANTLY );

internal
void
radix_sort_mc_64_32( const u64 * keys, u32 * ids, u32 * tmp, u32 count, Sort_Order sort_order );

// NOTE(theGiallo): -1: string0 <  string1
//                   0: string0 == string1
//                   1: string0 >  string1
// NULL is the least possible
internal
s32
string_min( const char * string0, const char * string1 );

internal
void
sort_lexicographically_insertion( const char ** strings, u32 count,
                                  u32 * data = NULL, Sort_Order sort_order = SORT_ASCENDANTLY );

internal
void
sort_lexicographically_insertion_ids_only( const char *const* strings, u32 count,
                                           u32 * ids, Sort_Order sort_order );

internal
void
sort_lexicographically_insertion_ids_only_16( const char *const* strings, u32 count,
                                              u16 * ids, Sort_Order sort_order );

internal
bool
sort_lexicographically_radix_insertion( char const** strings, u32 count,
                                        char const** tmp,
                                        Sort_Order sort_order = SORT_ASCENDANTLY,
                                        u32 * data     = NULL,
                                        u32 * data_tmp = NULL,
                                        u32 * lengths_strings = NULL,
                                        u32 * lengths_tmp     = NULL,
                                        u32 char_idx = 0 );

internal
bool
sort_lexicographically_radix_insertion_ids_only( char const*const* strings, u32 count,
                                                 Sort_Order sort_order,
                                                 u32 * ids,
                                                 u32 * ids_tmp = NULL,
                                                 u32 const * lengths_strings = NULL,
                                                 u32 char_idx = 0 );

internal
bool
sort_lexicographically_radix_insertion_ids_only_16( char const*const* strings, u32 count,
                                                    Sort_Order sort_order,
                                                    u16 * ids,
                                                    u16 * ids_tmp = NULL,
                                                    u32 const * lengths_strings = NULL,
                                                    u32 char_idx = 0 );

// NOTE(theGiallo): Stable sort of UTF-8 strings.
// NOTE(theGiallo): if you provide lengths* you have to fill them.
// If not provided they will be allocated on the stack (<- IMPORTANT!)
// and their values computed (aka string traversal).
// NOTE(theGiallo): the strings, data and lengths array will be sorted after
// the call. *tmp content can be discarded.
// NOTE(theGiallo): if you won't provide tmp, it will be allocated on the stack.
internal
void
sort_lexicographically( char const** strings, u32 count,
                        char const** tmp = NULL,
                        Sort_Order sort_order = SORT_ASCENDANTLY,
                        u32 * data     = NULL,
                        u32 * data_tmp = NULL,
                        u32 * lengths_strings = NULL,
                        u32 * lengths_tmp     = NULL );

internal
void
sort_lexicographically_ids_only( char const*const* strings, u32 count,
                                 Sort_Order sort_order,
                                 u32 * ids,
                                 u32 * ids_tmp = NULL,
                                 u32 * lengths_strings = NULL );

internal
void
sort_lexicographically_ids_only_16( char const*const* strings, u32 count,
                                    Sort_Order sort_order,
                                    u16 * ids,
                                    u16 * ids_tmp = NULL,
                                    u32 * lengths_strings = NULL );

// NOTE(theGiallo): -1: string0 <  string1
//                   0: string0 == string1
//                   1: string0 >  string1
// NULL is the least possible
s32
string_min( const char * string0, const char * string1 );

inline
u32
str_bytes_to_char_or_end( const char * string, u32 bytes_length, u8 c )
{
	u32 ret = 0;

	if ( string )
	{
		for ( ; ret != bytes_length && string[ret] != c && string[ret]; ++ret );
	}

	return ret;
}

inline
u32
str_bytes_to_null_or_end( const char * string, u32 bytes_length )
{
	u32 ret = 0;

	if ( string )
	{
		for ( ; ret != bytes_length && string[ret]; ++ret );
	}

	return ret;
}

inline
u32
str_bytes_to_null( const char * string )
{
	u32 ret = 0;

	if ( string )
	{
		for ( ; ret != U32_MAX - 1 && string[ret]; ++ret );
	}

	return ret;
}

u32
inline
str_copy( u8 * dst, u32 dst_bytes_size, u8 * src )
{
	u32 ret = 0;
	for ( ret = 0;
	     !ret || ( ret && src[ret-1] );
	      ++ret )
	{
		if ( ret == dst_bytes_size )
		{
			ret = 0;
			break;
		}
		dst[ret] = src[ret];
	}

	return ret;
}

internal
void
reorder( u32 * ids, void * data, u32 count, u32 elem_size )
{
	u8 v[elem_size];
	u8 moved[LPR_BITNSLOTS( count )];
	memset( moved, 0x0, LPR_BITNSLOTS( count ) );
	for ( u32 i = 0; i < count; ++i )
	{
		if ( LPR_BITTEST( moved, i ) )
		{
			continue;
		}

		memcpy( v, (u8*)data + i * elem_size, elem_size );
		for ( u32 j = i; ; )
		{
			u32 next = ids[j];
			if ( next == i )
			{
				memcpy( (u8*)data + j * elem_size, v, elem_size );
				LPR_BITSET( moved, j );
				break;
			}
			memcpy( (u8*)data + j * elem_size, (u8*)data + next * elem_size, elem_size );
			LPR_BITSET( moved, j );
			j = next;
		}
	}
}

struct
Mem_Stack
{
	u8 * mem_buf;
	u64 first_free;
	u64 total_size;
	u8 *
	push( u64 size );
};

inline
u8 *
Mem_Stack::
push( u64 size )
{
	u8 * ret = NULL;

	if ( this->first_free + size <= this->total_size )
	{
		ret = this->mem_buf + this->first_free;
		this->first_free += size;
	}

	return ret;
}

struct
ptr_u64_LL
{
	ptr_u64_LL * next;
	void * ptr;
	u64 size;
};

struct
u32_u32_LL
{
	u32_u32_LL * next;
	u32 value;
	u32 key;
};

#undef MH_EMIT_BODY
#undef MH_BODY_ONLY
#undef MH_STATIC

#define MH_TYPE ptr_u64_LL
#include "mem_hotel.inc.h"

#define MH_TYPE u32_u32_LL
#include "mem_hotel.inc.h"

struct
Mem_Pool
{
	u8 * mem_buf;
	u64 size;
	u64 occupied;

	ptr_u64_LL * free_list;
	ptr_u64_LL_MH freelist_nodes_mh;

	void
	init( void * mem, u64 size );

	void *
	allocate( u64 size );

	void *
	allocate_clean( u64 size );

	void
	deallocate( void * ptr );
};

inline
void
Mem_Pool::
init( void * mem, u64 size )
{
	this->mem_buf = (u8*)mem;
	this->size = size;
	this->occupied = 0;
	this->free_list = ::allocate( &this->freelist_nodes_mh );
	this->free_list->next = NULL;
	this->free_list->size = size;
	this->free_list->ptr  = this->mem_buf;
}

inline
void *
Mem_Pool::
allocate_clean( u64 size )
{
	void * ret = allocate( size );

	if ( ret )
	{
		memset( ret, 0, size );
	}

	return ret;
}


#endif /* ifndef GAME_API_UTILITY_H */
