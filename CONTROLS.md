# Controls

## Debug

|Keys |Description                                       |
|----:|--------------------------------------------------|
|  F1 | open the debug window                            |

## Record

|Keys |Description                                       |
|----:|--------------------------------------------------|
|  F5 | play recorded events (eventually stop recording) |
|s-F5 | play recorded events (eventually stop recording) and restore game memory|
|a-F5 | restore recorded game memory                     |
|  F6 | stop playing/recording recorded events           |
|  F7 | start input recording                            |
|s-F7 | record game memory and start input recording     |
|a-F7 | record game memory only                          |
|  F8 | toggle loop input playing                        |
|s-F8 | toggle loop input and game memory playing        |
|a-F8 | toggle loop memory restore during input playing  |
