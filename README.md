# Hot code reloading test
## What?
An implementation of hot code reloading like the [Handmade Hero's one](https://hero.handmadedev.org/videos/win32-platform/day021.html).

## How?
The key part is the separation between platform and game layers.
The platform layer has the main, and constitutes the executable.
It loads the game functions from a dynamic library at start-up and whenever it detects the library has been modified.

The whole game memory resides in the platform layer and is passed by reference to the game functions. Thus data structure changes won't work. The system layer never writes or reads from the game memory. It only bulk copies and restores it for debug purposes. The system layer calls the game functions, passing all the needed data. This is the only relationship it has with the game layer.

The system layer passes input events to the game layer, and calls update and render functions. It is up to the game layer to implement the game loop iteration.

A system API is passed to the game functions in the form of a struct with function pointers, declared in the generic system header. In this way the game layer can be system agnostic.

## Debug Functionalities Controls
Controls are listed into [CONTROLS.md](./CONTROLS.md).
