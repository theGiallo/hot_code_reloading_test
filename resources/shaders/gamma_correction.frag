#version 330 core
layout(row_major) uniform;

uniform sampler2D linear_fb;

in  vec2 _uv;

out vec4 out_color;

void main( void )
{
	vec4 c = texture2D( linear_fb, _uv );
	out_color = vec4( pow( c.rgb, vec3( 1.0 / 2.2 ) ), c.a );
}