#version 330 core
layout(row_major) uniform;

     in vec2 uv;
flat in vec4 g_grid_color;
flat in vec4 g_grid_bg_color;

out vec4 out_color;

// NOTE(theGiallo): h in [0,1)
vec4 rgb_from_hsv( vec4 hsv )
{
	vec4 ret;
	ret.a = hsv.a;
	float c = hsv.y * hsv.z;
	float hh = hsv.x * 6.0;
	float x = c * ( 1.0 - abs( mod( hh, 2) - 1.0 ) );

	if ( hh < 1.0 || hh >= 6.0 )
	{
		ret.rgb = vec3(c,x,0);
	} else
	if ( hh < 2.0 )
	{
		ret.rgb = vec3(x,c,0);
	} else
	if ( hh < 3.0 )
	{
		ret.rgb = vec3(0,c,x);
	} else
	if ( hh < 4.0 )
	{
		ret.rgb = vec3(0,x,c);
	} else
	if ( hh < 5.0 )
	{
		ret.rgb = vec3(x,0,c);
	} else
	if ( hh < 6.0 )
	{
		ret.rgb = vec3(c,0,x);
	}

	float m = hsv.z - c;
	ret.rgb += m;

	return ret;
}

vec3
apply_atmospheric_fog( vec3 color, vec3 air_color, float depth_meters )
{
	float fog_amount = 1.0 - exp( depth_meters * 0.002 );
	vec3 ret = mix( color, air_color, fog_amount );

	return ret;
}
vec3
apply_atmospheric_refraction_fog( vec3 color,
                                  vec3 air_color, vec3 sun_color,
                                  float depth_meters,
                                  vec3 ray_dir, vec3 sun_dir )
{
	float fog_amount = 1.0 - exp( depth_meters * 0.002 );
	float sun_amount = max( dot( ray_dir, sun_dir ), 0.0 );

	vec3 fog_color = mix( sun_color, air_color, pow( sun_amount, 0.8 ) );

	vec3 ret = mix( color, fog_color, fog_amount );

	return ret;
}
vec3
apply_atmospheric_scattering_refraction( vec3 color,
                                         vec3 air_color, vec3 sun_color,
                                         float depth_meters,
                                         vec3 ray_dir, vec3 sun_dir )
{
	float sun_amount = max( dot( ray_dir, sun_dir ), 0.0 );

	float ext_amount = 1.0-exp( -depth_meters * 0.0028 );
	float ins_amount = 1.0-exp( -depth_meters * 0.002 );

	vec3 fog_color = mix( sun_color, air_color, pow( sun_amount, 8.0 ) );

	vec3 ret = color * ( 1.0 - ext_amount ) + fog_color * ins_amount;

	return ret;
}

vec3
diffuse( vec3 color, vec3 normal, vec3 light_dir, vec3 light_color )
{
	float light_perc = max( 0.0, dot( normal, -light_dir ) );

	return light_perc * light_color * color;
}

float
smootherstep_1( float x )
{
	float ret = x * x * x * ( x * ( x * 6 - 15 ) + 10 );
	return ret;
}
vec2
smootherstep( float f0, float f1, vec2 v )
{
	vec2 ret;
	ret.x = smootherstep_1( clamp( ( v.x - f0 ) / ( f1 - f0 ), 0, 1 ) );
	ret.y = smootherstep_1( clamp( ( v.y - f0 ) / ( f1 - f0 ), 0, 1 ) );
	return ret;
}

float
min_of_v2( vec2 v )
{
	return min(v.x, v.y);
}
float
max_of_v2( vec2 v )
{
	return max(v.x, v.y);
}

vec2 square( vec2 v )
{
	return vec2(v.x*v.x, v.y*v.y);
}

void
main()
{
	float lines_width = 0.02;
	float lines_smooth_width = 0.001;
	vec2 _i;
	float smooth_start = lines_width - lines_smooth_width;
	float smooth_end = lines_width;
#if 0
	vec2 grid_a_2 =
	   min( modf( abs( uv ), _i ),
	   1.0 - modf( abs( uv ), _i ) );
	float grid_a = max_of_v2( square( 1.0 - smoothstep( 0.0, 0.12, grid_a_2 ) ) );
#else
	vec2 grid_a_2 =
	   vec2(2.0,2.0) -
	   ( smoothstep( smooth_start, smooth_end, modf( abs( uv ), _i ) ) +
	     smoothstep( smooth_start, smooth_end, 1.0 - modf( abs( uv ), _i ) ) );
	float grid_a = clamp( length( grid_a_2 ) / 0.707106781, 0.0, 1.0 );
#endif
	if ( ( grid_a == 1.0 && g_grid_color.a == 0.0 ) ||
	     ( grid_a == 0.0 && g_grid_bg_color.a == 0.0 ) )
	{
		discard;
	}

#if 1
	out_color = mix( g_grid_bg_color, g_grid_color, grid_a );
#else
	out_color = mix( g_grid_bg_color, g_grid_color, step( 0.0, grid_a ) );
#endif
}
