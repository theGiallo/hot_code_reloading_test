#version 330 core
layout(row_major) uniform;

layout(points) in;

layout(triangle_strip, max_vertices = 4) out;

uniform mat4 V;
uniform mat4 P;
uniform mat4 VP;

     in  vec3  v_plane_normal[];
     in  vec3  v_plane_up[];
     in  vec4  v_grid_color[];
     in  vec4  v_grid_bg_color[];
     in  float v_grid_side[];
     in  float v_grid_cell_side[];

     out vec2  uv;
flat out vec4  g_grid_color;
flat out vec4  g_grid_bg_color;


float
squared_length( vec3 v )
{
	return dot(v,v);
}

void
main()
{
	vec3 X = cross( v_plane_up[0], v_plane_normal[0] );
	vec3 Y = cross( v_plane_normal[0], X );
	vec3 p = gl_in[0].gl_Position.xyz;
	float h_side = 0.5 * v_grid_side[0];
	float uv_h_side = h_side / v_grid_cell_side[0];

	#define BACK_FACE_CULLING 1
	#if BACK_FACE_CULLING
	vec4 view_pos4 = vec4( p, 1.0 ) * V;
	vec4 view_pn4 = vec4( p + v_plane_normal[0], 1.0 ) * V;
	vec3 view_pos = view_pos4.xyz / view_pos4.w;
	vec3 view_pn = view_pn4.xyz / view_pn4.w;
	if ( dot( view_pn - view_pos, -view_pos ) <= 0 )
	{
		return;
	}
	#endif

	g_grid_color     = vec4( v_grid_color[0].rgb,    step( 1.0, v_grid_color[0].a ) );
	g_grid_bg_color  = vec4( v_grid_bg_color[0].rgb, step( 1.0, v_grid_bg_color[0].a ) );

	uv = vec2( -uv_h_side, -uv_h_side );
	gl_Position = vec4( p - X * h_side - Y * h_side, 1.0 ) * VP;
	EmitVertex();

	g_grid_color     = vec4( v_grid_color[0].rgb,    step( 1.0, v_grid_color[0].a ) );
	g_grid_bg_color  = vec4( v_grid_bg_color[0].rgb, step( 1.0, v_grid_bg_color[0].a ) );

	uv = vec2( -uv_h_side, uv_h_side );
	gl_Position = vec4( p - X * h_side + Y * h_side, 1.0 ) * VP;
	EmitVertex();

	g_grid_color     = vec4( v_grid_color[0].rgb,    step( 1.0, v_grid_color[0].a ) );
	g_grid_bg_color  = vec4( v_grid_bg_color[0].rgb, step( 1.0, v_grid_bg_color[0].a ) );

	uv = vec2( uv_h_side, -uv_h_side );
	gl_Position = vec4( p + X * h_side - Y * h_side, 1.0 ) * VP;
	EmitVertex();

	g_grid_color     = vec4( v_grid_color[0].rgb,    step( 1.0, v_grid_color[0].a ) );
	g_grid_bg_color  = vec4( v_grid_bg_color[0].rgb, step( 1.0, v_grid_bg_color[0].a ) );

	uv = vec2( uv_h_side, uv_h_side );
	gl_Position = vec4( p + X * h_side + Y * h_side, 1.0 ) * VP;
	EmitVertex();

	EndPrimitive();
}
