#version 330 core
layout(row_major) uniform;

uniform mat4 VP;
uniform mat4 V;
uniform mat4 P;

in  vec3  vertex;
in  vec3  normal;
in  vec3  attr_vec3_0;
in  float attr_float_0;
in  float attr_float_1;
in  vec4  attr_vec4_0;
in  vec4  attr_vec4_1;

#define plane_point    vertex
#define plane_normal   normal
#define plane_up       attr_vec3_0
#define grid_cell_side attr_float_0
#define grid_side      attr_float_1
#define grid_color     attr_vec4_0
#define grid_bg_color  attr_vec4_1

     out vec3  v_plane_normal;
     out vec3  v_plane_up;
     out vec4  v_grid_color;
     out vec4  v_grid_bg_color;
     out float v_grid_side;
     out float v_grid_cell_side;

void
main()
{
	gl_Position = vec4( plane_point, 1.0 );
	//gl_Position /= gl_Position.w;

	v_plane_normal   = plane_normal;
	v_plane_up       = plane_up;
	v_grid_cell_side = grid_cell_side;
	v_grid_side      = grid_side;
	v_grid_color     = grid_color;
	v_grid_bg_color  = grid_bg_color;
	v_grid_bg_color.rgb *= grid_bg_color.a;
	v_grid_color.rgb    *= grid_color.a;
}
