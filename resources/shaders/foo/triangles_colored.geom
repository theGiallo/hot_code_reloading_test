#version 330 core
layout(row_major) uniform;

layout(triangles) in;

#define RENDER_NORMALS 0
#if RENDER_NORMALS
	layout(triangle_strip, max_vertices = 12) out;
#else
	layout(triangle_strip, max_vertices = 3) out;
#endif

uniform mat4 V;
uniform mat4 P;

in vec3 v_color[3];
in float v_z[3];
in float v_depth[3];
in float v_meters_depth[3];
in float v_vid[3];
flat in vec3 v_sun_pos_v[3];
flat in vec3 v_sun_dir_v[3];
in vec3 v_view_pos[3];
in vec3 v_world_pos[3];
in vec3 v_world_camera_rel_pos[3];

flat out vec3 face_normal_v;
out vec3 v_color_f;
out float z;
out float depth;
out float meters_depth;
out float vid;
flat out vec3 sun_pos_v;
flat out vec3 sun_dir_v;
out vec3 view_pos;
out vec3 world_pos;
out vec3 world_camera_rel_pos;


float
squared_length( vec3 v )
{
	return dot(v,v);
}

void
main()
{
	vec3 face_normal_v_ =
	normalize( cross( v_view_pos[2].xyz - v_view_pos[1].xyz,
	                  v_view_pos[0].xyz - v_view_pos[1].xyz ) );
#define BACK_FACE_CULLING 0
#if BACK_FACE_CULLING
	if ( dot( face_normal_v_, -v_view_pos[0] ) <= 0 )
	{
		return;
	}
#endif

#if RENDER_NORMALS
	float max_len =
	   sqrt( max( squared_length( v_view_pos[0] - v_view_pos[1] ),
	              max( squared_length( v_view_pos[1] - v_view_pos[2] ),
	                   squared_length( v_view_pos[0] - v_view_pos[2] ) ) ) );
	vec3 baricenter_v = ( v_view_pos[0] + v_view_pos[1] + v_view_pos[2] ) / 3.0;
	vec3 arrow_end_v = baricenter_v + face_normal_v_ * max_len * 0.1;

	vec3 v[3];
	vec4 p[3];
	for ( int i = 0; i!=3; ++i )
	{
		v[i] = baricenter_v + ( v_view_pos[i] - baricenter_v ) * 0.01;
		p[i] = vec4( v[i], 1.0 ) * P;
	}
	vec4 baricenter_p = vec4( baricenter_v, 1.0 ) * P;
	vec4 arrow_end_p = vec4( arrow_end_v, 1.0 ) * P;


	for ( int t = 0; t!=3; ++t )
	{
		vec4 tp[3];
		tp[0] = p[t];
		tp[1] = baricenter_p;
		tp[2] = arrow_end_p;
		vec3 tv[3];
		tv[0] = v[t];
		tv[1] = baricenter_v;
		tv[2] = arrow_end_v;

		vec3 arrow_face_normal_v =
		normalize( cross( tv[2] - tv[1],
		                  tv[0] - tv[1] ) );

		if ( dot( arrow_face_normal_v, -v_view_pos[0] ) <= 0 )
		{
			arrow_face_normal_v = -arrow_face_normal_v;
		}

		for ( int i = 0; i!=3; ++i )
		{
			v_color_f = v_color[t];
			vid = v_vid[t];
			sun_pos_v = v_sun_pos_v[t];
			sun_dir_v = v_sun_dir_v[t];
			view_pos  = v[i];
			/*world_camera_rel_pos = v_world_camera_rel_pos[i];*/

			face_normal_v = arrow_face_normal_v;
			gl_Position = tp[i];
			depth = gl_Position.z / gl_Position.w;
			meters_depth = -v[i].z;
			world_pos = ( vec4(v[i],1.0) * inverse(V) ).xyz;
			z = world_pos.z;

			EmitVertex();
		}

		EndPrimitive();
	}
#endif

	for ( int i = 0; i!=3; ++i )
	{
		v_color_f = v_color[i];
		z = v_z[i];
		depth = v_depth[i];
		meters_depth = v_meters_depth[i];
		vid = v_vid[i];
		sun_pos_v = v_sun_pos_v[i];
		sun_dir_v = v_sun_dir_v[i];
		view_pos = v_view_pos[i];
		world_pos = v_world_pos[i];
		world_camera_rel_pos = v_world_camera_rel_pos[i];

		face_normal_v = face_normal_v_;
		gl_Position = gl_in[i].gl_Position;

		EmitVertex();
	}
	EndPrimitive();
}
