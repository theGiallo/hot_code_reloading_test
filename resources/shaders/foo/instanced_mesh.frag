#version 330 core
layout(row_major) uniform;

flat in vec4  g_instance_color;
     in vec3  v_color_f;
     in float z;
     in float depth;
     in float meters_depth;
     in float vid;
flat in vec3  sun_pos_v;
flat in vec3  sun_dir_v;
flat in vec3  face_normal_v;
     in vec3  view_pos;
     in vec3  world_pos;
     in vec3  world_camera_rel_pos;

out vec4 out_color;

// NOTE(theGiallo): h in [0,1)
vec4 rgb_from_hsv( vec4 hsv )
{
	vec4 ret;
	ret.a = hsv.a;
	float c = hsv.y * hsv.z;
	float hh = hsv.x * 6.0;
	float x = c * ( 1.0 - abs( mod( hh, 2) - 1.0 ) );

	if ( hh < 1.0 || hh >= 6.0 )
	{
		ret.rgb = vec3(c,x,0);
	} else
	if ( hh < 2.0 )
	{
		ret.rgb = vec3(x,c,0);
	} else
	if ( hh < 3.0 )
	{
		ret.rgb = vec3(0,c,x);
	} else
	if ( hh < 4.0 )
	{
		ret.rgb = vec3(0,x,c);
	} else
	if ( hh < 5.0 )
	{
		ret.rgb = vec3(x,0,c);
	} else
	if ( hh < 6.0 )
	{
		ret.rgb = vec3(c,0,x);
	}

	float m = hsv.z - c;
	ret.rgb += m;

	return ret;
}

vec3
apply_atmospheric_fog( vec3 color, vec3 air_color, float depth_meters )
{
	float fog_amount = 1.0 - exp( depth_meters * 0.002 );
	vec3 ret = mix( color, air_color, fog_amount );

	return ret;
}
vec3
apply_atmospheric_refraction_fog( vec3 color,
                                  vec3 air_color, vec3 sun_color,
                                  float depth_meters,
                                  vec3 ray_dir, vec3 sun_dir )
{
	float fog_amount = 1.0 - exp( depth_meters * 0.002 );
	float sun_amount = max( dot( ray_dir, sun_dir ), 0.0 );

	vec3 fog_color = mix( sun_color, air_color, pow( sun_amount, 0.8 ) );

	vec3 ret = mix( color, fog_color, fog_amount );

	return ret;
}
vec3
apply_atmospheric_scattering_refraction( vec3 color,
                                         vec3 air_color, vec3 sun_color,
                                         float depth_meters,
                                         vec3 ray_dir, vec3 sun_dir )
{
	float sun_amount = max( dot( ray_dir, sun_dir ), 0.0 );

	float ext_amount = 1.0-exp( -depth_meters * 0.0028 );
	float ins_amount = 1.0-exp( -depth_meters * 0.002 );

	vec3 fog_color = mix( sun_color, air_color, pow( sun_amount, 8.0 ) );

	vec3 ret = color * ( 1.0 - ext_amount ) + fog_color * ins_amount;

	return ret;
}

vec3
diffuse( vec3 color, vec3 normal, vec3 light_dir, vec3 light_color )
{
	float light_perc = max( 0.0, dot( normal, -light_dir ) );

	return light_perc * light_color * color;
}

void
main()
{
#if 1
	if ( g_instance_color.r != 0.0012312 )
	{
		vec3 p = world_pos;
		out_color = vec4(face_normal_v*0.5 + 0.5,1.0) * g_instance_color.a;
		return;
	}
#endif

#define DEPTH 0
#if DEPTH
	float d
	#if 1
		= meters_depth * 0.001;
	#else
		= pow(depth,100);
	#endif
	if ( d < 0 )
		out_color = vec4(0,0,0,1);
	else
	if ( d > 1 )
		out_color = vec4(1,1,1,1);
	else
		out_color = rgb_from_hsv( vec4(d,0.7,1,1) );
	return;
#endif


	float min = 0.0;
	float max = 1000.0;
	float v;
	v = ( z - min ) / (max-min);

	if ( meters_depth < 0 )
	{
		out_color = vec4(0,0,0,1);
		return;
	}

#if 0
	if ( v < 0 )
	{
		out_color = rgb_from_hsv( vec4(0,0.1,1,1) );
	} else
	if ( v > 1 )
	{
		out_color = rgb_from_hsv( vec4(0.5,1,1,1) );
	} else
	{
		out_color = rgb_from_hsv( vec4(v,0.7,1,1) );
	}
#endif

	/*out_color = rgb_from_hsv( vec4(0.01,0.0,0.1,1) );*/
	out_color.a = g_instance_color.a;
	out_color.rgb = v_color_f;

#if 0
		vec3 ray_dir = normalize( view_pos );
		vec3 diffuse_rgb = diffuse( out_color.rgb, face_normal_v, sun_dir_v, vec3( 0.5,0.6,0.7 ) );
		out_color.rgb = out_color.rgb * 0.1 + 13.0 * diffuse_rgb;
	#if 0
		out_color.rgb =
		   apply_atmospheric_fog(
		      out_color.rgb, vec3(0.5,0.6,0.7), meters_depth );
	#elif 0
		out_color.rgb =
		   apply_atmospheric_refraction_fog(
		      out_color.rgb, vec3(0.5,0.6,0.7), vec3(1.0,0.9,0.7), meters_depth,
		      ray_dir, sun_dir_v );
	#else
		out_color.rgb =
		   apply_atmospheric_scattering_refraction(
		      out_color.rgb, vec3(0.5,0.6,0.7), vec3(1.0,0.9,0.7), meters_depth,
		      ray_dir, sun_dir_v );
	#endif
#endif
}
