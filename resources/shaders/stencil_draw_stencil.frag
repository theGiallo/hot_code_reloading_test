#version 330 core
layout(row_major) uniform;
#define STENCIL_ENABLED 1

#if STENCIL_ENABLED
flat in uvec4 f_stencil_mask;
#endif
flat in uvec4 f_texture_mask;
in vec2 f_uv;

out uvec4 out_value;

uniform uint draw_mode;
uniform sampler2D texture;
uniform usampler2D stencil;
uniform usampler2D target_stencil;

void main( void )
{
#if STENCIL_ENABLED
	if ( f_stencil_mask != uvec4(0xFFFFFFFF) )
	{
		uvec4 stencil_texel = texelFetch( stencil, ivec2(gl_FragCoord.xy), 0 );
		if ( ( stencil_texel.r & f_stencil_mask.r ) != f_stencil_mask.r ||
		     ( stencil_texel.g & f_stencil_mask.g ) != f_stencil_mask.g ||
		     ( stencil_texel.b & f_stencil_mask.b ) != f_stencil_mask.b ||
		     ( stencil_texel.a & f_stencil_mask.r ) != f_stencil_mask.a )
		{
			discard;
		}
	}
#endif

	uint mode = draw_mode;

	if ( mode > 0x7F000000u )
	{
		uint mode_tx = mode & 0x0FFFFF00u;
		vec4 tex = texture2D( texture, f_uv );
		if ( ( mode_tx == 0x0F000000u && tex.a == 0 ) ||
		     ( mode_tx == 0x0F000100u && tex.a != 0 ) ||
		     ( mode_tx == 0x0F000200u && ( tex.r + tex.g + tex.b ) / ( 3.0 * max( tex.a, 1.0 ) ) < 0.5 ) ||
		     ( mode_tx == 0x0F000300u && ( tex.r ) / max( tex.a, 1.0 ) < 0.5 ) ||
		     ( mode_tx == 0x0F000400u && ( tex.g ) / max( tex.a, 1.0 ) < 0.5 ) ||
		     ( mode_tx == 0x0F000500u && ( tex.b ) / max( tex.a, 1.0 ) < 0.5 ) ||
		     ( mode_tx == 0x0F000600u && ( tex.r + tex.g + tex.b ) / ( 3.0 * max( tex.a, 1.0 ) ) > 0.5 ) ||
		     ( mode_tx == 0x0F000700u && ( tex.r ) / max( tex.a, 1.0 ) > 0.5 ) ||
		     ( mode_tx == 0x0F000800u && ( tex.g ) / max( tex.a, 1.0 ) > 0.5 ) ||
		     ( mode_tx == 0x0F000900u && ( tex.b ) / max( tex.a, 1.0 ) > 0.5 ) )
		{
			mode = 0x7FFFFFFFu;
		}
	}

	uint mode_l = mode & 0x700000FFu;
	uvec4 target_stencil_texel = texelFetch( target_stencil, ivec2(gl_FragCoord.xy), 0 );
	if ( mode_l == 0x70000000u )
	// NOTE(theGiallo): set zero
	{
		out_value = target_stencil_texel & ( ~ f_texture_mask );
	} else
	if ( mode_l == 0x70000001u )
	// NOTE(theGiallo): set one
	{
		out_value = target_stencil_texel | f_texture_mask;
	} else
	if ( mode_l == 0x70000002u )
	// NOTE(theGiallo): xor with 0
	{
		out_value = ( target_stencil_texel & ( ~ f_texture_mask ) ) |
		            ( ( target_stencil_texel ^ ( ~ f_texture_mask ) ) & f_texture_mask );
	} else
	if ( mode_l == 0x70000003u )
	// NOTE(theGiallo): xor with 1
	{
		out_value = ( target_stencil_texel & ( ~ f_texture_mask ) ) |
		            ( ( target_stencil_texel ^ f_texture_mask ) & f_texture_mask );
	} else
	if ( mode_l == 0x70000004u )
	// NOTE(theGiallo): invert
	{
		out_value = ( target_stencil_texel & ( ~ f_texture_mask ) ) |
		            ( ( ~ target_stencil_texel ) & f_texture_mask );
	} else
	{
		out_value = target_stencil_texel;
	}
}
