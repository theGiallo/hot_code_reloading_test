#version 330 core
layout(row_major) uniform;

in  vec2 vertex;
in  uint birth;
in  uint invisible;

out vec4 _color;

uniform uint color;
uniform uint time;
uniform uint max_age;
uniform mat3 VP;


void main( void )
{
	gl_Position = vec4( (vec3(vertex.xy, 1.0) * VP).xy, 0.0, 1.0 );

	if ( invisible != 0u )
	{
		_color = vec4( 0.0 );
	} else
	{
		_color.r = float( ( color >> 0  ) & uint(0xff) ) / 255.0f;
		_color.g = float( ( color >> 8  ) & uint(0xff) ) / 255.0f;
		_color.b = float( ( color >> 16 ) & uint(0xff) ) / 255.0f;
		_color.a = float( ( color >> 24 ) & uint(0xff) ) / 255.0f;
		_color.a *= 1.0 - float( time - birth ) / float( max_age );

		_color.rgb = pow( _color.rgb, vec3(2.2,2.2,2.2) ) * _color.a; // NOTE: premultiply alpha
	}

}
